package net.bor.discordguard;

// @author ArtBorax
import java.io.IOException;
import java.util.concurrent.TimeoutException;
import net.bor.discordguard.core.Manager;

public class DiscordGuard {

    public static void main(String[] args) {
        Manager manager = new Manager();
        try {
            manager.start();
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (TimeoutException ex) {
            ex.printStackTrace();
        }
    }

}
