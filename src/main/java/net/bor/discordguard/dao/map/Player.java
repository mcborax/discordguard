package net.bor.discordguard.dao.map;

// @author ArtBorax
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "players")
public class Player {

    @Id
    @Column(name = "player_id")
    private int playerId;

    @Column(name = "discord_user_id")
    private Long discordUserID;

    @Column(name = "discord_subscribe", nullable = false, columnDefinition = "boolean default false")
    private boolean discordSubscribe;

    @Column(name = "username", length = 30)
    private String username;

    @Column(name = "uuid", length = 60)
    private String uuid;

    @Column(name = "karma")
    private Double karma;

    @Column(name = "karma_speed")
    private Double karmaSpeed;

    @Column(name = "create_report", nullable = false, columnDefinition = "Integer default 0")
    private int createReport;

    @Column(name = "cons_report", nullable = false, columnDefinition = "Integer default 0")
    private int consReport;

    @Column(name = "accus_report", nullable = false, columnDefinition = "Integer default 0")
    private int accusReport;

    public int getPlayerId() {
        return playerId;
    }

    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }

    public Long getDiscordUserID() {
        return discordUserID;
    }

    public void setDiscordUserID(long discordUserID) {
        this.discordUserID = discordUserID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Double getKarma() {
        return karma;
    }

    public void setKarma(Double karma) {
        this.karma = karma;
    }

    public Double getKarmaSpeed() {
        return karmaSpeed;
    }

    public void setKarmaSpeed(Double karmaSpeed) {
        this.karmaSpeed = karmaSpeed;
    }

    public boolean isDiscordSubscribe() {
        return discordSubscribe;
    }

    public void setDiscordSubscribe(boolean discordSubscribe) {
        this.discordSubscribe = discordSubscribe;
    }

    public int getCreateReport() {
        return createReport;
    }

    public void setCreateReport(int createReport) {
        this.createReport = createReport;
    }

    public int getConsReport() {
        return consReport;
    }

    public void setConsReport(int consReport) {
        this.consReport = consReport;
    }

    public int getAccusReport() {
        return accusReport;
    }

    public void setAccusReport(int accusReport) {
        this.accusReport = accusReport;
    }

}
