package net.bor.discordguard.dao.map;

// @author ArtBorax
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "reports_type")
public class ReportType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "report_id")
    private Set<Report> report;

    @Column(name = "deprecated", nullable = false, columnDefinition = "boolean default false")
    private boolean deprecated;

    @Column(name = "short_desription", length = 100)
    private String shortDesription;

    @Column(name = "desription", length = 65535, columnDefinition = "Text")
    private String desription;

    public int getId() {
        return id;
    }

    public Set<Report> getReport() {
        return report;
    }

    public boolean isDeprecated() {
        return deprecated;
    }

    public void setDeprecated(boolean deprecated) {
        this.deprecated = deprecated;
    }

    public String getShortDesription() {
        return shortDesription;
    }

    public void setShortDesription(String shortDesription) {
        this.shortDesription = shortDesription;
    }

    public String getDesription() {
        return desription;
    }

    public void setDesription(String desription) {
        this.desription = desription;
    }

    

}
