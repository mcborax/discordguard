package net.bor.discordguard.dao.map;

// @author ArtBorax
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "servers")
public class Server {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "server_id")
    private int id;

    @Column(name = "name", length = 30)
    private String name;

//    @Type(type = "text")
    @Column(name = "fullname", length = 65535, columnDefinition = "Text")
    private String fullName;

    @Column(name = "time_open")
    private Date timeOpen;

    @Column(name = "time_close")
    private Date timeClose;

    @Column(name = "discord_chanel_id")
    private Long discordChanelID;

    @Column(name = "discord_webhook")
    private String discordWebhook;

    @Column(name = "version", length = 30)
    private String version;
    
    @Column(name = "avatar", length = 255)
    private String avatar;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Date getTimeOpen() {
        return timeOpen;
    }

    public void setTimeOpen(Date timeOpen) {
        this.timeOpen = timeOpen;
    }

    public Date getTimeClose() {
        return timeClose;
    }

    public void setTimeClose(Date timeClose) {
        this.timeClose = timeClose;
    }

    public Long getDiscordChanelID() {
        return discordChanelID;
    }

    public void setDiscordChanelID(Long discordChanelID) {
        this.discordChanelID = discordChanelID;
    }

    public String getDiscordWebhook() {
        return discordWebhook;
    }

    public void setDiscordWebhook(String discordWebhook) {
        this.discordWebhook = discordWebhook;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getAvatar() {
        return avatar;
    }

}
