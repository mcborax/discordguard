package net.bor.discordguard.dao.map;

// @author ArtBorax
import java.util.Date;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "reports")
public class Report {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "report_id")
    private int id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "creator_player_id")
    private Player creator;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "accused_player_id")
    private Player accused;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "server_id")
    private Server server;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "report_id")
    private Set<ReportResult> results;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "report_id")
    private Set<ReportMsg> msgs;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "type")
    private ReportType type;

    @Column(name = "cost_karma")
    private Integer costKarma;

    @Column(name = "creator_karma")
    private Double cratorKarma;

    @Column(name = "accused_karma")
    private Double accusedKarma;

    @Column(name = "time")
    private Date time;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Player getCreator() {
        return creator;
    }

    public void setCreator(Player creator) {
        this.creator = creator;
    }

    public Player getAccused() {
        return accused;
    }

    public void setAccused(Player accused) {
        this.accused = accused;
    }

    public Server getServer() {
        return server;
    }

    public void setServer(Server server) {
        this.server = server;
    }

    public Set<ReportResult> getResults() {
        return results;
    }

    public void setResults(Set<ReportResult> results) {
        this.results = results;
    }

    public Set<ReportMsg> getMsgs() {
        return msgs;
    }

    public void setMsgs(Set<ReportMsg> msgs) {
        this.msgs = msgs;
    }

    public ReportType getType() {
        return type;
    }

    public void setType(ReportType type) {
        this.type = type;
    }

    public Integer getCostKarma() {
        return costKarma;
    }

    public void setCostKarma(int costKarma) {
        this.costKarma = costKarma;
    }

    public double getCratorKarma() {
        return cratorKarma;
    }

    public void setCratorKarma(double cratorKarma) {
        this.cratorKarma = cratorKarma;
    }

    public double getAccusedKarma() {
        return accusedKarma;
    }

    public void setAccusedKarma(double accusedKarma) {
        this.accusedKarma = accusedKarma;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public ReportResult getLastResult() {
        if (results == null || results.isEmpty()) {
            return null;
        }
        ReportResult result = null;
        for (ReportResult rr : results) {
            if (result == null) {
                result = rr;
                continue;
            }
            if (rr.getId() > result.getId()) {
                result = rr;
            }
        }
        return result;
    }

}
