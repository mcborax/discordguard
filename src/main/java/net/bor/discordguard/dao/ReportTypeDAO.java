package net.bor.discordguard.dao;

// @author ArtBorax
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import net.bor.discordguard.dao.map.ReportType;

public class ReportTypeDAO extends AbstractDAO {

    public ReportTypeDAO(EntityManager entityManager) {
        super(entityManager);
    }

    public ReportType find(int id) {
        return entityManager.find(ReportType.class, id);
    }

    public List<ReportType> findAll() {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<ReportType> query = builder.createQuery(ReportType.class);
        query.from(ReportType.class);
        TypedQuery<ReportType> q = entityManager.createQuery(query);
        return q.getResultList();
    }

    public List<ReportType> findActive() {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<ReportType> query = builder.createQuery(ReportType.class);
        Root<ReportType> root = query.from(ReportType.class);
        query.select(root).where(builder.equal(root.get("deprecated"), false));
        query.orderBy(builder.asc(root.get("id")));
        TypedQuery<ReportType> q = entityManager.createQuery(query);
        return q.getResultList();
    }
}
