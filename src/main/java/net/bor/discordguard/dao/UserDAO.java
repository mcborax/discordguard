package net.bor.discordguard.dao;

// @author ArtBorax
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import net.bor.discordguard.dao.map.*;

public class UserDAO extends AbstractDAO {

    public UserDAO(EntityManager entityManager) {
        super(entityManager);
    }

    public Player find(int id) {
        return entityManager.find(Player.class, id);
    }

    public User getUser(String username, String uuid) {
        if (username == null) {
            return null;
        }
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> query = builder.createQuery(User.class);
        Root<User> root = query.from(User.class);

        if (username != null && uuid != null && !uuid.equals("00000000-0000-0000-0000-000000000000")) {
            query.select(root).where(builder.equal(root.get("username"), username), builder.equal(root.get("uuid"), uuid));
        } else if (username != null && (uuid == null || uuid.equals("00000000-0000-0000-0000-000000000000"))) {
            query.select(root).where(builder.equal(root.get("username"), username));
        } else if (username == null && uuid != null && !uuid.equals("00000000-0000-0000-0000-000000000000")) {
            query.select(root).where(builder.equal(root.get("uuid"), uuid));
        } else {
            return null;
        }
        TypedQuery<User> q = entityManager.createQuery(query);
        List<User> list = q.getResultList();
        if (list.size() == 1) {
            return list.get(0);
        }
        if (list.size() > 1) {
            System.out.println("ОШИБКА В БАЗЕ САЙТА!!! Найдено " + list.size() + " вместо одной, username=[" + username + "] uuid=[" + uuid + "]");
        }
        return null;
    }
}
