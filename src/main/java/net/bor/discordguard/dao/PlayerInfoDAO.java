package net.bor.discordguard.dao;

// @author ArtBorax
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import net.bor.discordguard.dao.map.*;

public class PlayerInfoDAO extends AbstractDAO {

    public PlayerInfoDAO(EntityManager entityManager) {
        super(entityManager);
    }

    public PlayerInfo find(Server server, Player player) {
        return entityManager.find(PlayerInfo.class, new PlayerInfoPK(server, player));
    }

    public PlayerInfo persist(Server server, Player player) {
        beginTransaction();
        PlayerInfo playerInfo = new PlayerInfo(new PlayerInfoPK(server, player));
        entityManager.persist(playerInfo);
        if (commitTransaction()) {
            return playerInfo;
        }
        return find(server, player);
    }

    public boolean update(PlayerInfo playerInfo) {
        beginTransaction();
        entityManager.merge(playerInfo);
        return commitTransaction();
    }

    public List<PlayerInfo> finds(Player player, Date time) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<PlayerInfo> query = builder.createQuery(PlayerInfo.class);
        Root<PlayerInfo> root = query.from(PlayerInfo.class);
        query.select(root).where(builder.equal(root.get("player"), player), builder.greaterThanOrEqualTo(root.get("timeQuit"), time));
        return entityManager.createQuery(query).getResultList();
    }

    public List<PlayerInfo> finds(Player player) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<PlayerInfo> query = builder.createQuery(PlayerInfo.class);
        Root<PlayerInfo> root = query.from(PlayerInfo.class);
        query.select(root).where(builder.equal(root.get("player"), player));
        return entityManager.createQuery(query).getResultList();
    }
}
