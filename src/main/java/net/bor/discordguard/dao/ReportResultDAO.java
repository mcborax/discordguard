package net.bor.discordguard.dao;

// @author ArtBorax
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.EntityManager;
import net.bor.discordguard.dao.map.*;
import net.bor.discordguard.karma.IKarma;

public class ReportResultDAO extends AbstractDAO {
    
    private IKarma karma;
    
    public ReportResultDAO(EntityManager entityManager) {
        super(entityManager);
    }
    
    public ReportResultDAO(EntityManager entityManager, IKarma karma) {
        super(entityManager);
        this.karma = karma;
    }
    
    public ReportResult find(int id) {
        return entityManager.find(ReportResult.class, id);
    }
    
    private void calcReport(Report report, Player creator, Player accused, int newLevel, int lastLevel) {
        if (newLevel > 0 && lastLevel == 0) {
            //создателю репорта выдать награду
            creator.setKarmaSpeed(creator.getKarmaSpeed() + karma.getRewardReport());
        } else if (newLevel > 0 && lastLevel == -1) {
            //создателю репорта компенсировать штраф
            //создателю репорта выдать награду
            creator.setKarmaSpeed(creator.getKarmaSpeed() + karma.getRewardReport() - karma.getPenaltyOfLevel(karma.getPenaltyLevelReport(), report.getCratorKarma()));
        } else if (newLevel == -1 && lastLevel == 0) {
            //создателю репорта выдать штраф
            creator.setKarmaSpeed(creator.getKarmaSpeed() + karma.getPenaltyOfLevel(karma.getPenaltyLevelReport(), report.getCratorKarma()));
        } else if (newLevel == -1 && lastLevel > 0) {
            //создателю репорта выдать штраф
            //создателю репорта забрать награду
            creator.setKarmaSpeed(creator.getKarmaSpeed() - karma.getRewardReport() + karma.getPenaltyOfLevel(karma.getPenaltyLevelReport(), report.getCratorKarma()));
        } else if (newLevel == 0 && lastLevel == -1) {
            //создателю репорта компенсировать штраф
            creator.setKarmaSpeed(creator.getKarmaSpeed() - karma.getPenaltyOfLevel(karma.getPenaltyLevelReport(), report.getCratorKarma()));
        } else if (newLevel == 0 && lastLevel > 0) {
            //создателю репорта забрать награду
            creator.setKarmaSpeed(creator.getKarmaSpeed() - karma.getRewardReport());
        }
        
        if (newLevel > 0 && lastLevel < 1) {
            //виновнику выдать штраф 
            accused.setKarmaSpeed(accused.getKarmaSpeed() + karma.getPenaltyOfLevel(newLevel, report.getAccusedKarma()));
        } else if (newLevel < 1 && lastLevel > 0) {
            //виновнику компенсировать штраф 
            accused.setKarmaSpeed(accused.getKarmaSpeed() - karma.getPenaltyOfLevel(lastLevel, report.getAccusedKarma()));
        } else if (newLevel > 0 && lastLevel > 0) {
            //виновнику компенсировать штраф 
            //виновнику выдать штраф 
            accused.setKarmaSpeed(accused.getKarmaSpeed() - karma.getPenaltyOfLevel(lastLevel, report.getAccusedKarma()) + karma.getPenaltyOfLevel(newLevel, report.getAccusedKarma()));
        }
    }
    
    public Set<Player> persistAndUpdate(Report report, Server server, Player player, String msg, Date time, int newLevel) {
        Set<Player> players = new HashSet<>();
        beginTransaction();
        
        Player accused = report.getAccused();
        Player creator = report.getCreator();
        players.add(accused);
        players.add(creator);
        
        ReportResult lastResult = report.getLastResult();
        if (lastResult != null) {
            int lastLevel = lastResult.getLevel();
            if (lastLevel == newLevel) {
                commitTransaction();
                return null;
            }
            
            calcReport(report, creator, accused, newLevel, lastLevel);
            
            for (ReportResult reportResult : report.getResults()) {
                int curLevel = reportResult.getLevel();
                Player resultPlayer = reportResult.getPlayer();
                if (newLevel > 0 && curLevel > 0 && lastLevel == 0) {
                    //создателю ресульта выдать награду
                    resultPlayer.setKarmaSpeed(resultPlayer.getKarmaSpeed() + karma.getRewardReport());
                } else if (newLevel > 0 && curLevel > 0 && lastLevel == -1) {
                    //создателю ресульта выдать награду
                    //создателю ресульта компенсировать штраф
                    resultPlayer.setKarmaSpeed(resultPlayer.getKarmaSpeed() + karma.getRewardReport() - karma.getPenaltyOfLevel(karma.getPenaltyLevelReport(), reportResult.getCratorKarma()));
                } else if (newLevel > 0 && curLevel < 1 && lastLevel < 1) {
                    //создателю ресульта забрать награду
                    //создателю ресульта выдать штраф
                    resultPlayer.setKarmaSpeed(resultPlayer.getKarmaSpeed() - karma.getRewardReport() + karma.getPenaltyOfLevel(karma.getPenaltyLevelReport(), reportResult.getCratorKarma()));
                } else if (newLevel == 0 && curLevel > 0 && lastLevel > 0) {
                    //создателю ресульта забрать награду
                    resultPlayer.setKarmaSpeed(resultPlayer.getKarmaSpeed() - karma.getRewardReport());
                } else if (newLevel < 1 && curLevel < 1 && lastLevel > 0) {
                    //создателю ресульта компенсировать штраф
                    //создателю ресульта выдать награду
                    resultPlayer.setKarmaSpeed(resultPlayer.getKarmaSpeed() + karma.getRewardReport() - karma.getPenaltyOfLevel(karma.getPenaltyLevelReport(), reportResult.getCratorKarma()));
                } else if (newLevel == -1 && curLevel > 0 && lastLevel > 0) {
                    //создателю ресульта забрать награду
                    //создателю ресульта выдать штраф
                    resultPlayer.setKarmaSpeed(resultPlayer.getKarmaSpeed() - karma.getRewardReport() + karma.getPenaltyOfLevel(karma.getPenaltyLevelReport(), reportResult.getCratorKarma()));
                }
                players.add(resultPlayer);
            }
        } else {
            //первый рассмотр
            calcReport(report, creator, accused, newLevel, 0);
        }
        
        ReportResult reportResult = new ReportResult();
        reportResult.setReport(report);
        reportResult.setServer(server);
        reportResult.setPlayer(player);
        reportResult.setTime(time);
        reportResult.setMsg(msg);
        reportResult.setLevel(newLevel);
        reportResult.setCratorKarma(player.getKarma());
        report.setCostKarma(karma.getShiftReport(player.getKarma()));
        entityManager.merge(report);
        entityManager.persist(reportResult);

        //создающему ресульта выдать награду
        player.setKarmaSpeed(player.getKarmaSpeed() + karma.getRewardReport());
        player.setConsReport(player.getConsReport() + 1);
        players.add(player);
        
        for (Player playerChange : players) {
            //проверить !!!!!!!!!!!!!!!!!!!! не будут ли сюда попадать разные ентити одного плеера
            entityManager.merge(playerChange);
        }
        
        if (commitTransaction()) {
            return players;
        }
        return null;
    }
    
    public ReportResult persist(Report report, Server server, Player player, String msg, Date time, int level) {
        beginTransaction();
        ReportResult reportResult = new ReportResult();
        reportResult.setReport(report);
        reportResult.setServer(server);
        reportResult.setPlayer(player);
        reportResult.setTime(time);
        reportResult.setMsg(msg);
        reportResult.setLevel(level);
        reportResult.setCratorKarma(player.getKarma());
        entityManager.persist(reportResult);
        if (commitTransaction()) {
            return reportResult;
        }
        return null;
    }
    
}
