package net.bor.discordguard.dao;

// @author ArtBorax
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import net.bor.discordguard.dao.map.*;

public class ReportDAO extends AbstractDAO {

    public ReportDAO(EntityManager entityManager) {
        super(entityManager);
    }

    public Report find(int id) {
        return entityManager.find(Report.class, id);
    }

    public Report persist(Server server, Player creator, Player accused, ReportType type, int costKarma, Date time) {
        beginTransaction();
        Report report = new Report();
        report.setServer(server);
        report.setCreator(creator);
        report.setAccused(accused);
        report.setType(type);
        report.setCostKarma(costKarma);
        report.setCratorKarma(creator.getKarma());
        report.setAccusedKarma(accused.getKarma());
        report.setTime(time);
        creator.setCreateReport(creator.getCreateReport() + 1);
        entityManager.merge(creator);
        accused.setAccusReport(accused.getAccusReport() + 1);
        entityManager.merge(accused);
        entityManager.persist(report);
        if (commitTransaction()) {
            return report;
        }
        return null;
    }

    public boolean update(Report report) {
        beginTransaction();
        entityManager.merge(report);
        return commitTransaction();
    }

    public List<Report> finds(Player player, int offset, int limit) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Report> query = builder.createQuery(Report.class);
        Root<Report> root = query.from(Report.class);

        query.select(root).where(builder.equal(root.get("accused"), player));
        query.orderBy(builder.desc(root.get("id")));

        TypedQuery<Report> q = entityManager.createQuery(query).setFirstResult(offset).setMaxResults(limit);
        return q.getResultList();
    }

    public List<Report> finds(double costKarma, int offset, int limit) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Report> query = builder.createQuery(Report.class);
        Root<Report> root = query.from(Report.class);

        Date date = new Date(System.currentTimeMillis() - (48 * 60 * 60 * 1000));
        query.select(root).where(builder.lt(root.get("costKarma"), costKarma), builder.greaterThanOrEqualTo(root.get("time"), date));
        query.orderBy(builder.desc(root.get("id")));

        TypedQuery<Report> q = entityManager.createQuery(query).setFirstResult(offset).setMaxResults(limit);
        return q.getResultList();
    }

    public List<Report> findsCreator(Player player, Date date) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Report> query = builder.createQuery(Report.class);
        Root<Report> root = query.from(Report.class);
        query.select(root).where(builder.equal(root.get("creator"), player), builder.greaterThanOrEqualTo(root.get("time"), date));
        TypedQuery<Report> q = entityManager.createQuery(query);
        return q.getResultList();
    }

    public List<Report> findsAccused(Player player, ReportType type, Date date) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Report> query = builder.createQuery(Report.class);
        Root<Report> root = query.from(Report.class);
        query.select(root).where(builder.equal(root.get("accused"), player), builder.equal(root.get("type"), type), builder.greaterThanOrEqualTo(root.get("time"), date));
        TypedQuery<Report> q = entityManager.createQuery(query);
        return q.getResultList();
    }
}
