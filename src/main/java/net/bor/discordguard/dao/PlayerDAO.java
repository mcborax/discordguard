package net.bor.discordguard.dao;

// @author ArtBorax
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import net.bor.discordguard.dao.map.*;

public class PlayerDAO extends AbstractDAO {

    public PlayerDAO(EntityManager entityManager) {
        super(entityManager);
    }

    public Player find(int id) {
        return entityManager.find(Player.class, id);
    }

    public Player persist(User user, Double karmaNewPlayer) {
        Player player = find(user.getId());
        if (player != null) {
            return player;
        }
        beginTransaction();
        player = new Player();
        player.setKarma(karmaNewPlayer);
        player.setKarmaSpeed(0D);
        player.setPlayerId(user.getId());
        player.setUsername(user.getUsername());
        player.setUuid(user.getUuid().toLowerCase());
        entityManager.persist(player);
        if (commitTransaction()) {
            return player;
        }
        return find(user.getId());
    }

    public boolean update(Player player) {
        beginTransaction();
        entityManager.merge(player);
        return commitTransaction();
    }

    public Player find(String username) {
        if (username == null) {
            return null;
        }
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Player> query = builder.createQuery(Player.class);
        Root<Player> root = query.from(Player.class);
        query.select(root).where(builder.equal(root.get("username"), username));
        TypedQuery<Player> q = entityManager.createQuery(query);
        List<Player> list = q.getResultList();
        if (list.size() == 1) {
            return list.get(0);
        }
        if (list.size() > 1) {
            System.out.println("ОШИБКА В БАЗЕ САЙТА!!! Найдено " + list.size() + " вместо одной, username=[" + username + "]");
        }
        return null;
    }

    public List<Player> finds() {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Player> query = builder.createQuery(Player.class);
        Root<Player> root = query.from(Player.class);
        query.select(root).where(builder.le(root.get("karma"), 100000), builder.ge(root.get("karma"), 0), builder.ge(root.get("karmaSpeed"), 0));
        query.orderBy(builder.desc(root.get("karma")));
        TypedQuery<Player> q = entityManager.createQuery(query).setMaxResults(18);
        return q.getResultList();
    }

    public List<String> findAllUsername() {
        TypedQuery<String> query = entityManager.createQuery("SELECT username FROM Player", String.class);
        return query.getResultList();
    }

    public Player findDiscord(long discordUserID) {
        if (discordUserID < 10000) {
            return null;
        }
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Player> query = builder.createQuery(Player.class);
        Root<Player> root = query.from(Player.class);
        query.select(root).where(builder.equal(root.get("discordUserID"), discordUserID));
        TypedQuery<Player> q = entityManager.createQuery(query);
        List<Player> list = q.getResultList();
        if (list.size() == 1) {
            return list.get(0);
        }
        if (list.size() > 1) {
            System.out.println("ОШИБКА В БАЗЕ САЙТА!!! Найдено " + list.size() + " вместо одной, discordUserID=[" + discordUserID + "]");
        }
        return null;
    }
}
