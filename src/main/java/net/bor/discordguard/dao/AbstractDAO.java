package net.bor.discordguard.dao;

// @author ArtBorax
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

public abstract class AbstractDAO {

    protected final EntityManager entityManager;
    protected final EntityTransaction entityTransaction;

    public AbstractDAO(EntityManager entityManager) {
        this.entityManager = entityManager;
        this.entityTransaction = this.entityManager.getTransaction();
    }

    protected void beginTransaction() {
        try {
            entityTransaction.begin();
        } catch (Exception e) {
            rollbackTransaction();
        }
    }

    protected boolean commitTransaction() {
        try {
            entityTransaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            rollbackTransaction();
            return false;
        } finally {
            entityManager.close();
        }
        return true;
    }

    protected void rollbackTransaction() {
        try {
            if (entityTransaction.isActive()) {
                entityTransaction.rollback();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
