package net.bor.discordguard.dao;

// @author ArtBorax
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import net.bor.discordguard.dao.map.*;

public class ChatDAO extends AbstractDAO {

    public ChatDAO(EntityManager entityManager) {
        super(entityManager);
    }

    public Chat find(int id) {
        return entityManager.find(Chat.class, id);
    }

    public void addServerChat(Server server, Player player, String msg, Date time, int cens, int type) {
        beginTransaction();
        Chat chat = new Chat();
        chat.setServer(server);
        chat.setPlayer(player);
        chat.setTime(time);
        chat.setMsg(msg);
        //chat.setMsg((msg.getCmd() == null || msg.getCmd().isEmpty()) ? msg.getMsg() : "/" + ((msg.getMsg() == null || msg.getMsg().isEmpty()) ? msg.getCmd() : msg.getCmd() + " " + msg.getMsg()));
        chat.setCensLevel(cens);
        chat.setType(type);
        entityManager.persist(chat);
        if (!commitTransaction()) {
            //место для дедлупа!!!!!!
            addServerChat(server, player, msg, time, cens, type);
        }
    }

    public List<Chat> finds(Player player, int offset, int limit, int minType) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Chat> query = builder.createQuery(Chat.class);
        Root<Chat> root = query.from(Chat.class);
        query.select(root).where(builder.equal(root.get("player"), player), builder.or(builder.le(root.get("type"), minType), builder.gt(root.get("censLevel"), 0)));
        query.orderBy(builder.desc(root.get("id")));
        TypedQuery<Chat> q = entityManager.createQuery(query).setFirstResult(offset).setMaxResults(limit);
        return q.getResultList();
    }

    public List<Chat> finds(int offset, int limit, int minType) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Chat> query = builder.createQuery(Chat.class);
        Root<Chat> root = query.from(Chat.class);
        query.select(root).where(builder.or(builder.le(root.get("type"), minType), builder.equal(root.get("type"), 3), builder.gt(root.get("censLevel"), 0)));
        query.orderBy(builder.desc(root.get("id")));
        TypedQuery<Chat> q = entityManager.createQuery(query).setFirstResult(offset).setMaxResults(limit);
        return q.getResultList();
    }

}
