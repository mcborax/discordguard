package net.bor.discordguard.dao;

// @author ArtBorax
import java.util.Date;
import javax.persistence.EntityManager;
import net.bor.discordguard.dao.map.*;

public class ReportMsgDAO extends AbstractDAO {

    public ReportMsgDAO(EntityManager entityManager) {
        super(entityManager);
    }

    public ReportMsg find(int id) {
        return entityManager.find(ReportMsg.class, id);
    }

    public ReportMsg persist(Server server, Player player, Report report, String msg, Date time) {
        beginTransaction();
        ReportMsg reportMsg = new ReportMsg();
        reportMsg.setMsg(msg);
        reportMsg.setPlayer(player);
        reportMsg.setReport(report);
        reportMsg.setServer(server);
        reportMsg.setTime(time);
        entityManager.persist(reportMsg);
        if (commitTransaction()) {
            return reportMsg;
        }
        return null;
    }

}
