package net.bor.discordguard.dao;

// @author ArtBorax
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import net.bor.discordguard.dao.map.Server;

public class ServerDAO extends AbstractDAO {

    public ServerDAO(EntityManager entityManager) {
        super(entityManager);
    }

    public Server find(int id) {
        return entityManager.find(Server.class, id);
    }

    public List<Server> findAll() {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Server> query = builder.createQuery(Server.class);
        query.from(Server.class);
        TypedQuery<Server> q = entityManager.createQuery(query);
        return q.getResultList();
    }
}
