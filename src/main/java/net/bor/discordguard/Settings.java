package net.bor.discordguard;

// @author ArtBorax
public class Settings {

    public static final String CONFIG_DIR = "config";
    public static final String CONFIG_FILENAME = "config.json";
    public static final String LANG_FOLDER = "lang";
    public static final String CONFIG_KARMA_FILENAME = "karma.json";
    public static final String CENS_LEVEL_1_FILENAME = "censlevel1.json";
    public static final String CENS_LEVEL_2_FILENAME = "censlevel2.json";
    public static final String CENS_LEVEL_3_FILENAME = "censlevel3.json";
    public static final String CENS_EXCEP_FILENAME = "censexcep.json";
    public static final String CENS_CMDS_FILENAME = "cmds.json";
    public static final String DICTIONARY_FILENAME = "dictionary.json";
    public static final String LOG_FILENAME = "server.log";
    public static final int SIZE_POOL = 15;
    public static final int SIZE_POOL_WEBHOOK = 20;

    public static final int CHAT_PAGE_SIZE = 18;

    public static final String LANG_UNKNOWN_CODE = "Я не знаю такого кода, попробуйте еще раз.";
    public static final String LANG_INVALID_CODE = "Ты уверен, что это твой код? Код состоит из 4 цифр.";
    public static final String LANG_DISCORD_ACCOUNT_LINKED = "Твоя учетная запись Discord связана с %name%";
    public static final long PING_TIMEOUT = 30000;
}
