package net.bor.discordguard.cens;

// @author ArtBorax
import java.util.Set;

public interface ICens {

    public void reload();

    public ResultCens getCens(String msg, boolean moderation);

    public Set<String> getCmds();

    public boolean isCensCmd(String cmd);
    
    public Set<String> getDictionary();
}
