package net.bor.discordguard.cens;

// @author ArtBorax
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;
import net.bor.discordguard.Settings;
import net.bor.discordguard.config.IConfig;
import net.bor.discordguard.console.IConsole;

public class Cens implements ICens {

    private Set<String> censLevel1;
    private Set<String> censLevel2;
    private Set<String> censLevel3;
    private Set<String> censExcep;
    private Set<String> dictionary;
    private Set<String> cmds;

    private final IConsole console;
    private final IConfig config;

    private static final Pattern pParams = Pattern.compile("\\d+");

    public Cens(IConsole console, IConfig config) {
        this.console = console;
        this.config = config;
        reload();
    }

    private Set<String> load(String filename) {
        try {
            Set result = config.load(filename, Set.class);
            if (result == null) {
                result = new HashSet<>();
                result.add("(|[нзz]а|пр[оаие]|[оа]б|с|в[iы]|у)бзд[ёе](|.+)");
                result.add("(|[нзz]а|пр[оаие]|[оа]б|с|в[iы]|у)бзд[нлеиую][шхноутлц](|.+)");
                config.save(result, filename);
            }
            return result;
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public void reload() {
        censLevel1 = load(Settings.CENS_LEVEL_1_FILENAME);
        censLevel2 = load(Settings.CENS_LEVEL_2_FILENAME);
        censLevel3 = load(Settings.CENS_LEVEL_3_FILENAME);
        censExcep = load(Settings.CENS_EXCEP_FILENAME);
        dictionary = load(Settings.DICTIONARY_FILENAME);
        cmds = load(Settings.CENS_CMDS_FILENAME);
    }

    @Override
    public Set<String> getDictionary() {
        return dictionary;
    }

    private Set<String> getCensWords(Set<String> setMsg, Set<String> cens, int level) {
        HashSet<String> result = new HashSet<>();
        setMsg.stream().forEach((word) -> {
            cens.stream().filter((rgs) -> (Pattern.matches(rgs, word))).forEach((rg) -> {
                if (!censExcep.contains(word)) {
                    console.outBLUE("Level:" + level + " [" + word + "]->[" + rg + "]");
                    result.add(word);
                }
            });
        });
        return result;
    }

    private void getModerationWords(Set<String> setMsg, ResultCens rc) {
        //реализовать многопоточный стрим
        HashSet<String> result = new HashSet<>();
        setMsg.stream().filter((String word) -> pParams.matcher(word).matches() ? false : !dictionary.contains(word)).map((word) -> {
            if (word != null && !word.isEmpty()) {
                result.add(word);
            }
            return word;
        }).forEach((word) -> {
            if (word != null && !word.isEmpty()) {
                console.outBLUE("Moderation: [" + word + "]");
            }
        });
        rc.setModerationWords(result);
    }

    private ResultCens getCensLevel(Set<String> setMsg) {
        Set<String> level1 = getCensWords(setMsg, censLevel1, 1);
        Set<String> level2 = getCensWords(setMsg, censLevel2, 2);
        Set<String> level3 = getCensWords(setMsg, censLevel3, 3);
        int summ = level1.size() + (level2.size() * 5) + (level3.size() * 10);
        int level = 0;
        if (summ > 1 && summ <= 4) {
            level = 1;
        }
        if (summ > 4 && summ <= 9) {
            level = 2;
        }
        if (summ > 9 && summ <= 14) {
            level = 3;
        }
        if (summ > 14 && summ <= 24) {
            level = 4;
        }
        if (summ > 24) {
            level = 5;
        }
        return new ResultCens(level1, level2, level3, level);
    }

    @Override
    public ResultCens getCens(String msg, boolean moderation) {
        String[] arrmsg = msg.replaceAll("ё", "е").replaceAll("\\p{Punct}", "").trim().toLowerCase().split("\\s");
        Set<String> setMsg = new HashSet<>();
        Collections.addAll(setMsg, arrmsg);
        ResultCens result = getCensLevel(setMsg);
        if (moderation) {
            getModerationWords(setMsg, result);
        }
        return result;
    }

    @Override
    public Set<String> getCmds() {
        return cmds;
    }

    @Override
    public boolean isCensCmd(String cmd) {
        if (cmds == null || cmds.isEmpty()) {
            return false;
        }
        if (cmds.contains(cmd)) {
            return true;
        }
        return false;
    }

}
