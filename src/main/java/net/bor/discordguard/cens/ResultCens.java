package net.bor.discordguard.cens;

// @author ArtBorax
import java.util.Set;

public class ResultCens {

    private final Set<String> wordsLevel1;
    private final Set<String> wordsLevel2;
    private final Set<String> wordsLevel3;
    private final int level;
    private Set<String> wordsModeration;
    private int penalty = 0;

    public ResultCens(Set<String> worldsLevel1, Set<String> worldsLevel2, Set<String> worldsLevel3, int level) {
        this.wordsLevel1 = worldsLevel1;
        this.wordsLevel2 = worldsLevel2;
        this.wordsLevel3 = worldsLevel3;
        this.level = level;
    }

    public Set<String> getWordsLevel1() {
        return wordsLevel1;
    }

    public Set<String> getWordsLevel2() {
        return wordsLevel2;
    }

    public Set<String> getWordsLevel3() {
        return wordsLevel3;
    }

    public boolean isCensEmpty() {
        if (!wordsLevel1.isEmpty()) {
            return false;
        }
        if (!wordsLevel2.isEmpty()) {
            return false;
        }
        if (!wordsLevel3.isEmpty()) {
            return false;
        }
        return true;
    }

    public int getLevel() {
        return level;
    }

    public Set<String> getModerationWords() {
        return wordsModeration;
    }

    public void setModerationWords(Set<String> worldsModeration) {
        this.wordsModeration = worldsModeration;
    }

    public String getCensAllWords() {
        StringBuilder sb = new StringBuilder();
        wordsLevel1.stream().forEach((word) -> {
            sb.append(word).append(" ");
        });
        wordsLevel2.stream().forEach((word) -> {
            sb.append(word).append(" ");
        });
        wordsLevel3.stream().forEach((word) -> {
            sb.append(word).append(" ");
        });
        return sb.toString().trim();
    }

    public int getPenalty() {
        return penalty;
    }

    public void setPenalty(int penalty) {
        this.penalty = penalty;
    }

}
