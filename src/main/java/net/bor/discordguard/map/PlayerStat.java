package net.bor.discordguard.map;

// @author ArtBorax
public class PlayerStat {

    private int playerId = 0;
    private String username;
    private String uuid;
    private double karma;
    private double karmaSpeed;

    public PlayerStat(int playerId, String username, String uuid, Double karma, Double karmaSpeed) {
        this.playerId = playerId;
        this.username = username;
        this.uuid = uuid;
        this.karma = karma;
        this.karmaSpeed = karmaSpeed;
    }

    public int getPlayerId() {
        return playerId;
    }

    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }

    public double getKarma() {
        return karma;
    }

    public void setKarma(double karma) {
        this.karma = karma;
    }

    public double getKarmaSpeed() {
        return karmaSpeed;
    }

    public void setKarmaSpeed(double karmaSpeed) {
        this.karmaSpeed = karmaSpeed;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

}
