package net.bor.discordguard.map;

// @author ArtBorax
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ActionPlayer {

    private int playerId;
    private String username;
    private String uuid;
    private String msg;
    private int serverId;
    private Date datetime;
    private boolean confirmation;
    private String cmd;
    private int karma;
    private String type;
    private List<Msgs> msgs;

    public class Msgs {

        private String msg;
        private String cmd;

        public Msgs(String msg, String cmd) {
            this.msg = msg;
            this.cmd = cmd;
        }
    }

    public void addMsgs(String msg, String cmd) {
        if (msgs == null) {
            msgs = new ArrayList<>();
        }
        msgs.add(new Msgs(msg, cmd));
    }

    public void addMsgs(String msg) {
        if (msgs == null) {
            msgs = new ArrayList<>();
        }
        msgs.add(new Msgs(msg, null));
    }

    public List<Msgs> getMsgs() {
        return msgs;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getServerId() {
        return serverId;
    }

    public void setServerId(int serverId) {
        this.serverId = serverId;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

    public int getPlayerId() {
        return playerId;
    }

    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }

    public boolean isConfirmation() {
        return confirmation;
    }

    public void setConfirmation(boolean confirmation) {
        this.confirmation = confirmation;
    }

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public int getKarma() {
        return karma;
    }

    public void setKarma(int karma) {
        this.karma = karma;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
