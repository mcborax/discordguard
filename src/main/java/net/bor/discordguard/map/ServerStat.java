package net.bor.discordguard.map;

// @author ArtBorax
import java.util.Set;

public class ServerStat {

    private int serverId;
    private double tps;
    private Set<Integer> players;

    public ServerStat(int serverId, double tps, Set<Integer> players) {
        this.serverId = serverId;
        this.tps = tps;
        this.players = players;
    }

    public int getServerId() {
        return serverId;
    }

    public double getTps() {
        return tps;
    }

    public Set<Integer> getPlayers() {
        return players;
    }

}
