package net.bor.discordguard.map;

// @author ArtBorax
import java.util.ArrayList;
import java.util.Date;

public class ReportMessage {

    private int serverId;
    private int playerId;
    private int reportId;
    private String playerUUID;
    private String username;
    private String accusedUsername;
    private String accusedUUID;
    private String msg;
    private ArrayList<String> msgs;
    private Integer type;
    private int level;
    private Date time;
    private String typeMsg;

    public void setServerId(int serverId) {
        this.serverId = serverId;
    }

    public int getServerId() {
        return serverId;
    }

    public int getPlayerId() {
        return playerId;
    }

    public int getReportId() {
        return reportId;
    }

    public String getAccusedUsername() {
        return accusedUsername;
    }

    public String getAccusedUUID() {
        return accusedUUID;
    }

    public String getMsg() {
        return msg;
    }

    public ArrayList<String> getMsgs() {
        return msgs;
    }

    public void setMsgs(ArrayList<String> msgs) {
        this.msgs = msgs;
    }

    public Integer getType() {
        return type;
    }

    public int getLevel() {
        return level;
    }

    public Date getTime() {
        return time;
    }

    public String getTypeMsg() {
        return typeMsg;
    }

    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }

    public void setPlayerUUID(String playerUUID) {
        this.playerUUID = playerUUID;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setTypeMsg(String typeMsg) {
        this.typeMsg = typeMsg;
    }

    public void setAccusedUsername(String username) {
        this.accusedUsername = username;
    }

}
