package net.bor.discordguard.config;

// @author ArtBorax
import java.io.IOException;
import java.util.HashMap;

public interface IConfig {

    public void save(Object obj, String filename) throws IOException;

    public <T> T load(String filename, Class<T> clazz) throws IOException;

    public <T> T load(String folder, String filename, Class<T> clazz) throws IOException;

    public void save(Object obj, String folder, String filename) throws IOException;

    public void reload();

    public String getDBDiscordGuardHost();

    public String getDBDiscordGuardName();

    public String getDBDiscordGuardUsername();

    public String getDBDiscordGuardPass();

    public String getDBUsersHost();

    public String getDBUsersName();

    public String getDBUsersUsername();

    public String getDBUsersPass();

    public String getDBLauncherHost();

    public String getDBLauncherName();

    public String getDBLauncherUsername();

    public String getDBLauncherPass();

    public String getDiscordToken();

    public long getDiscordServerId();

    public HashMap<Integer, Long> getServersIdChanelId();

    public long getDiscordChanelIdLog();

    public long getDiscordChanelIdConsole();

    public String getDiscordHookAll();

    public HashMap<Long, Integer> getRoleIdPermissionLvl();

    public String getDiscordGameStatus();

    public String getDiscordCommandPrefix();

    public String getRabbitHost();

    public int getRabbitPort();

    public String getRabbitUser();

    public String getRabbitPass();

    public String getRabbitQueueNameC();

    public String getRabbitQueueNameS();

    public String getRabbitExchangeNameC();

    public String getRabbitExchangeNameS();

    public int getAdminPlayerId();

    public int getReportIdChat();

    public String getLangDefault();

    public String getDiscordAvatarURL();

}
