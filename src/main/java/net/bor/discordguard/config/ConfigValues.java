package net.bor.discordguard.config;

// @author ArtBorax
import java.util.ArrayList;
import java.util.HashMap;

public class ConfigValues {

    public String DBDiscordGuardHost = "127.0.0.1:3306";
    public String DBDiscordGuardName = "nameDB";
    public String DBDiscordGuardUsername = "username";
    public String DBDiscordGuardPass = "pass";
    public String DBUsersHost = "127.0.0.1:3306";
    public String DBUsersName = "nameDB";
    public String DBUsersUsername = "username";
    public String DBUsersPass = "pass";
    public String DBLauncherHost = "127.0.0.1:3306";
    public String DBLauncherName = "nameDB";
    public String DBLauncherUsername = "username";
    public String DBLauncherPass = "pass";
    public String DiscordToken = "token";
    public long DiscordServerId = 1000;
    public HashMap<Integer, Long> ServersIdChanelId = new HashMap<>();
    public long DiscordChanelIdLog = 1001;
    public long DiscordChanelIdConsole = 1002;
    public ArrayList<String> DiscordHookAll = new ArrayList<>();
    public String DiscordAvatarURL = "";
    public HashMap<Long, Integer> RoleIdPermissionLvl = new HashMap<>();
    public String DiscordGameStatus = "MCBorax.ru";
    public String DiscordCommandPrefix = ".";
    public String RabbitHost = "127.0.0.1";
    public int RabbitPort = 5672;
    public String RabbitUser = "user";
    public String RabbitPass = "pass";
    public String RabbitQueueNameC = "client";
    public String RabbitQueueNameS = "server";
    public String RabbitExchangeNameC = "";
    public String RabbitExchangeNameS = "";
    public int AdminPlayerId = 0;
    public int ReportIdChat = 1;
    public String LangDefault = "ru_RU";

    public ConfigValues() {
        DiscordHookAll.add("url");
        DiscordHookAll.add("url");
        
        ServersIdChanelId.put(1, 10001L);
        ServersIdChanelId.put(2, 10002L);
        ServersIdChanelId.put(3, 10003L);

        RoleIdPermissionLvl.put(101L, 1);
        RoleIdPermissionLvl.put(102L, 2);
        RoleIdPermissionLvl.put(103L, 3);
        RoleIdPermissionLvl.put(104L, 4);
    }
}
