package net.bor.discordguard.config;

// @author ArtBorax
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.HashMap;
import net.bor.discordguard.Settings;
import net.bor.discordguard.console.IConsole;

public class Config implements IConfig {

    private ConfigValues configValues;
    private final IConsole console;
    private int countHookAll = -1;

    public Config(IConsole console) {
        this.console = console;
        reload();
    }

    @Override
    public void save(Object obj, String filename) throws IOException {
        File folder = new File(Settings.CONFIG_DIR);
        if (!folder.exists()) {
            folder.mkdir();
            console.outGREEN("Create folder " + Settings.CONFIG_DIR);
        }
        try (Writer writer = new OutputStreamWriter(new FileOutputStream(Settings.CONFIG_DIR + File.separator + filename), "UTF-8")) {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            gson.toJson(obj, writer);
            console.outGREEN("Config save " + filename);
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void save(Object obj, String folder, String filename) throws IOException {
        File fl = new File(Settings.CONFIG_DIR);
        if (!fl.exists()) {
            fl.mkdir();
            console.outGREEN("Create folder " + Settings.CONFIG_DIR);
        }
        fl = new File(Settings.CONFIG_DIR + File.separator + folder);
        if (!fl.exists()) {
            fl.mkdir();
            console.outGREEN("Create folder " + Settings.CONFIG_DIR + "/" + folder);
        }
        try (Writer writer = new OutputStreamWriter(new FileOutputStream(Settings.CONFIG_DIR + File.separator + folder + File.separator + filename), "UTF-8")) {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            gson.toJson(obj, writer);
            console.outGREEN("Config save " + folder + "/" + filename);
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public <T> T load(String filename, Class<T> clazz) throws IOException {
        T obj = null;
        try (Reader reader = new InputStreamReader(new FileInputStream(Settings.CONFIG_DIR + File.separator + filename), "UTF-8")) {
            Gson gson = new GsonBuilder().create();
            obj = gson.fromJson(reader, clazz);
            console.out("Сonfig loaded " + filename);
        } catch (FileNotFoundException ex) {
            console.outYELLOW("File not found " + filename);
        }
        return obj;
    }

    @Override
    public <T> T load(String folder, String filename, Class<T> clazz) throws IOException {
        T obj = null;
        try (Reader reader = new InputStreamReader(new FileInputStream(Settings.CONFIG_DIR + File.separator + folder + File.separator + filename), "UTF-8")) {
            Gson gson = new GsonBuilder().create();
            obj = gson.fromJson(reader, clazz);
            console.out("Сonfig loaded " + folder + "/" + filename);
        } catch (FileNotFoundException ex) {
            console.outYELLOW("File not found " + folder + "/" + filename);
        }
        return obj;
    }

    @Override
    public void reload() {
        try {
            configValues = load(Settings.CONFIG_FILENAME, ConfigValues.class);
            if (configValues == null) {
                configValues = new ConfigValues();
                save(configValues, Settings.CONFIG_FILENAME);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public String getDBDiscordGuardHost() {
        return configValues.DBDiscordGuardHost;
    }

    @Override
    public String getDBDiscordGuardName() {
        return configValues.DBDiscordGuardName;
    }

    @Override
    public String getDBDiscordGuardUsername() {
        return configValues.DBDiscordGuardUsername;
    }

    @Override
    public String getDBDiscordGuardPass() {
        return configValues.DBDiscordGuardPass;
    }

    @Override
    public String getDBUsersHost() {
        return configValues.DBUsersHost;
    }

    @Override
    public String getDBUsersName() {
        return configValues.DBUsersName;
    }

    @Override
    public String getDBUsersUsername() {
        return configValues.DBUsersUsername;
    }

    @Override
    public String getDBUsersPass() {
        return configValues.DBUsersPass;
    }

    @Override
    public String getDBLauncherHost() {
        return configValues.DBLauncherHost;
    }

    @Override
    public String getDBLauncherName() {
        return configValues.DBLauncherName;
    }

    @Override
    public String getDBLauncherUsername() {
        return configValues.DBLauncherUsername;
    }

    @Override
    public String getDBLauncherPass() {
        return configValues.DBLauncherPass;
    }

    @Override
    public String getDiscordToken() {
        return configValues.DiscordToken;
    }

    @Override
    public long getDiscordServerId() {
        return configValues.DiscordServerId;
    }

    @Override
    public HashMap<Integer, Long> getServersIdChanelId() {
        return configValues.ServersIdChanelId;
    }

    @Override
    public long getDiscordChanelIdLog() {
        return configValues.DiscordChanelIdLog;
    }

    @Override
    public long getDiscordChanelIdConsole() {
        return configValues.DiscordChanelIdConsole;
    }

    @Override
    public String getDiscordHookAll() {
        if (configValues.DiscordHookAll.size() >= countHookAll) {
            countHookAll = -1;
        }
        countHookAll++;
        return configValues.DiscordHookAll.get(countHookAll);
    }

    @Override
    public HashMap<Long, Integer> getRoleIdPermissionLvl() {
        return configValues.RoleIdPermissionLvl;
    }

    @Override
    public String getDiscordGameStatus() {
        return configValues.DiscordGameStatus;
    }

    @Override
    public String getDiscordCommandPrefix() {
        return configValues.DiscordCommandPrefix;
    }

    @Override
    public String getRabbitHost() {
        return configValues.RabbitHost;
    }

    @Override
    public int getRabbitPort() {
        return configValues.RabbitPort;
    }

    @Override
    public String getRabbitUser() {
        return configValues.RabbitUser;
    }

    @Override
    public String getRabbitPass() {
        return configValues.RabbitPass;
    }

    @Override
    public String getRabbitQueueNameC() {
        return configValues.RabbitQueueNameC;
    }

    @Override
    public String getRabbitQueueNameS() {
        return configValues.RabbitQueueNameS;
    }

    @Override
    public String getRabbitExchangeNameC() {
        return configValues.RabbitExchangeNameC;
    }

    @Override
    public String getRabbitExchangeNameS() {
        return configValues.RabbitExchangeNameS;
    }

    @Override
    public int getAdminPlayerId() {
        return configValues.AdminPlayerId;
    }

    @Override
    public String getLangDefault() {
        return configValues.LangDefault;
    }

    @Override
    public int getReportIdChat() {
        return configValues.ReportIdChat;
    }

    @Override
    public String getDiscordAvatarURL() {
        return configValues.DiscordAvatarURL;
    }

}
