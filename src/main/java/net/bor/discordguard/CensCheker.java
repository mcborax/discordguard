package net.bor.discordguard;

// @author ArtBorax
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import net.bor.discordguard.cens.Cens;
import net.bor.discordguard.cens.ResultCens;
import net.bor.discordguard.config.Config;
import net.bor.discordguard.console.Console;

public class CensCheker {

    public static void main(String[] args) throws IOException {
        Console console = new Console();
        Config config = new Config(console);
        Set<String> dictionary = null;
                dictionary = new HashSet<>();
//        try {
//            dictionary = config.load(Settings.DICTIONARY_FILENAME, Set.class);
//            if (dictionary == null) {
//                dictionary = new HashSet<>();
//                config.save(dictionary, Settings.DICTIONARY_FILENAME);
//            }
//        } catch (IOException ex) {
//            ex.printStackTrace();
//        }
        if (dictionary == null) {
            return;
        }

        int size = dictionary.size();
        console.out("Изначальный размер словаря: " + size);

        HashSet<String> result1 = new HashSet<>();
        HashSet<String> result2 = new HashSet<>();
        HashSet<String> result3 = new HashSet<>();
        result1.add("гнид(|.+)");
        result3.add("хул[еяи]");
        result3.add("аху[еи](|.+)");
        result2.add("пнх");
        result2.add("пздц");
        result1.add("(|[нзz]а|пр[оаие]|[оа]б|с|в[iы]|у)бзд[ёе](|.+)");
        result1.add("(|[нзz]а|пр[оаие]|[оа]б|с|в[iы]|у)бзд[нлеиую][шхноутлц](|.+)");
        result2.add("(|[нзz]а|пр[оаие]|с|в[iы]|у)[мбп]ля+");
        result2.add("(|[нзz]а|пр[оаие]|с|в[iы]|у)[мбп]ля+[бдт](|.+)");  
        result2.add("вафел");
        result2.add("вафл[ёе]р");
        result3.add("взъеб(|.+)");
        result3.add("выбляд(|.+)");
        result1.add("выпер[дт](|.+)");
        result1.add("выс[ср]ат(|.+)");
        result2.add("г[оа]ндон(|.+)");
        result1.add("(|[нз]а|пр[оаие]|[оа]б|с|в[iы]|из)г[оа]в[не][иоюяшн](|.+)");
        result3.add("д[оа][ёе]б[ыи]в[оа]т(|.+)");
        result3.add("д[оа]лб[оа][ёе]б");
        result1.add("д[оа]лб[оа]я(|.+)");
        result1.add("(|[нзz]а|пр[оаие]|[оа]б|с|в[iы]|у)дрис[тн](|.+)");
        result1.add("(|[нзz]а|пр[оаие]|[оа]б|с|в[iы]|у)дроч(|.+)");
        result1.add("задрот(|.+)");
        result2.add("(|[нз3z]а|пр[оаие]|[оа]б|[оа]б[ъь]|наст[оа]|п[оа]д|п[оа]д[ъь]|п[оа]|р[оа][зс][ъь]|р[оа][зс]|в[iы]|)[яёеиi]б(|[аиеу][лн])");
        result2.add("(|.+)eba[tl](|.+)");
        result3.add("eblan(|.+)");
        result3.add("[иеe][b6б][aа@][lл][oaоа]");
        result3.add("[ёеи][6б]лан(|.+)");
        result2.add("[ёеи][6б]ки");
        result3.add("[ёеи][6б]л(|.+)");
        result3.add("[ёе][6б]н(|.+)");
        result2.add("[ёе][6б]ск(|.+)");
        result2.add("елда(|.+)");
        result1.add("жоп[уа]"); 
        result1.add("залуп(|.+)"); 
        result1.add("зас[еир][ар](|.+)"); 
        result3.add("зло[ёе]б(|.+)"); 
        result3.add("[ёеи][6б][ао]н[ао]м[ао]т"); 
        result2.add("ибонех[еу](|.+)"); 
        result1.add("конча"); 
        result1.add("курв[яа](|.+)"); 
        result1.add("л[оа][хш](|ар.+)"); 
        result2.add("лярв(|.+)"); 
        result2.add("(|[нз]а|пр[оеаи]|от)м[оа]нд[еиоуюа](|ть)");
        result2.add("мин(|ь)ет(|.+)"); 
        result2.add("м[оа]кр[оа]щ[ёе]лка"); 
        result2.add("мраз(|.+)"); 
        result2.add("(|[нз]а|пр[оеаи]|от)муд[аеино](|[клз])"); 
        result1.add("(|.+)п[иёе][р3zз]д(|.+)"); 
        result1.add("(|[нз]а|пр[оеаи]|обо|об|по|у)с(|[еи])[цр][ау][тн](|.+)"); 
        result3.add("(|.+)[^ч]ебуч(|.+)"); 
        result3.add("ебуч(|.+)");
        result2.add("[нз][aеи]х[еи]р(|.+)"); 
        result3.add("[нз][aеи][aеи][бп][аеё](|.+)"); 
        result3.add("(|на|за|про|при|пре|об|об[ъь]|от)(|с[ьъ])еб[еио]с(|.+)"); 
        result1.add("очкун"); 
        result1.add("падла"); 
        result1.add("паскуда"); 
        result3.add("п[ие]д[оаеи][рк](|[^ю].+)"); 
        result2.add("п[ие]дри(|.+)"); 
        result2.add("п[еи]р[еи][ёе]б(|.+)"); 
        result1.add("п[ёе]рнуть"); 
        result1.add("пися"); 
        result1.add("письк[аиое](|.+)"); 
        result1.add("писю(|.+)"); 
        result1.add("п[оа]скуд(|.+)"); 
        result3.add("п[оа]т[оа]ску(|.+)"); 
        result1.add("пр[еи]дур[оа]к"); 
        result1.add("своло(|[^к]+)"); 
        result2.add("с[еи]к[еи]ль"); 
        result1.add("с(|[еи])рун"); 
        result1.add("срак(|.+)"); 
        result1.add("ссыш(|.+)"); 
        result2.add("стерва"); 
        result1.add("с(|ц)ук[аи](|.+)"); 
        result1.add("suk[ai]"); 
        result1.add("с(|ц)[ыу]ч[икоьа][раойне](|.+)"); 
        result2.add("(|от|[нзz]а|по|пр[оаие]|р[оа][зс])трах[аои](|.+)"); 
        result2.add("ублюд(|.+)"); 
        result1.add("ушлеп(|.+)"); 
        result3.add("(|[нз3z][аaеи]|по|пр[оаие]|[оа]б|[aсву])[хx](|_)[уy](|_)[ёеiийяюe](|.+)"); 
        result3.add("[хx][уy]л."); 
        result1.add("(|[нзzп][аоеи]|пр[оаие]|[оа])хер(|[^о][^б]+|.)"); 
        result1.add("хамло"); 
        result1.add("хитр[оа]жопый"); 
        result1.add("целка"); 
        result1.add("чмо"); 
        result1.add("чмошник"); 
        result1.add("чмыр(|.+)"); 
        result2.add("ш[ао]лав(|.+)"); 
        result2.add("шлюх(|.+)"); 
        config.save(result1, Settings.CENS_LEVEL_1_FILENAME);
        config.save(result2, Settings.CENS_LEVEL_2_FILENAME);
        config.save(result3, Settings.CENS_LEVEL_3_FILENAME);
        HashSet<String> exc = new HashSet<>();
        exc.add("яб");
        exc.add("иб");
        config.save(exc, Settings.CENS_EXCEP_FILENAME);

        Cens cens = new Cens(console, config);
        cens.reload();
        for (String word : dictionary) {
            ResultCens r = cens.getCens(word, false);
            if (!r.getWordsLevel1().isEmpty()) {
                for (String ww : r.getWordsLevel1()) {
                    System.out.println(ww);
                }
                for (String ww : r.getWordsLevel2()) {
                    System.out.println(ww);
                }
                for (String ww : r.getWordsLevel3()) {
                    System.out.println(ww);
                }
                //break;
            }
        }

    }

}
