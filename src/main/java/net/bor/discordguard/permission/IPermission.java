package net.bor.discordguard.permission;

// @author ArtBorax
public interface IPermission {

    public boolean isAuth(long roleId);

    public boolean isSendMsgServers(long roleId);

    public boolean isGetDossier(long roleId);

    public boolean isGetCens(long roleId);

    public boolean isGetChatHistory(long roleId);

    public boolean isGetOtherChatHistory(long roleId);

    public boolean isUndo(long roleId);

    public boolean isOtherUndo(long roleId);

    public boolean isGetBan(long roleId);

    public boolean isSetBan(long roleId);

    public boolean isSetUnBan(long roleId);
}
