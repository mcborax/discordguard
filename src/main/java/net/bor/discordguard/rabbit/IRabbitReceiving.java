package net.bor.discordguard.rabbit;

// @author ArtBorax
public interface IRabbitReceiving {

    public boolean receive(String appId, String messageId, String msg);
}
