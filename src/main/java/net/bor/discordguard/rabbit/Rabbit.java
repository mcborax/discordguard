package net.bor.discordguard.rabbit;

// @author ArtBorax
import com.google.gson.Gson;
import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import java.io.IOException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeoutException;
import net.bor.discordguard.config.IConfig;
import net.bor.discordguard.console.IConsole;

public class Rabbit implements IRabbit {

    private final IConfig config;
    private final IConsole console;

    private Connection connection;
    private Channel channel;

    private final Gson gson = new Gson();

    private Thread thread;
    private final ArrayBlockingQueue<Msg> queue;

    private class Msg {

        int appId;
        String queueName;
        String exchangeName;
        MsgType type;
        Object msg;

        public Msg(int appId, String exchangeName, String queueName, MsgType type, Object msg) {
            this.appId = appId;
            this.queueName = queueName;
            this.exchangeName = exchangeName;
            this.type = type;
            this.msg = msg;
        }

    }

    public Rabbit(IConfig config, IConsole console) {
        this.config = config;
        this.console = console;
        queue = new ArrayBlockingQueue(1000000, true);
    }

    @Override
    public void init() throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(config.getRabbitHost());
        factory.setUsername(config.getRabbitUser());
        factory.setPassword(config.getRabbitPass());
        factory.setPort(config.getRabbitPort());
        connection = factory.newConnection();
        channel = connection.createChannel();
        console.out("RabbitMQ create connect.");
    }

    @Override
    public void initProperties(int i) {
        for (MsgType type : MsgType.values()) {
            type.setServerId(i);
        }
    }

    @Override
    public void queueDeclareReceive(String queueName, IRabbitReceiving receive) throws IOException {
        channel.queueDeclare(queueName, true, false, false, null);
        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, BasicProperties properties, byte[] body) throws IOException {
                if (receive.receive(properties.getAppId(), properties.getMessageId(), new String(body, "UTF-8"))) {
                    channel.basicAck(envelope.getDeliveryTag(), false);
                } else {
                    //временная мера
                    channel.basicAck(envelope.getDeliveryTag(), false);
                }
            }
        };
        channel.basicConsume(queueName, consumer);
        console.out("RabbitMQ channel [" + queueName + "] declared.");
    }

    @Override
    public void start() {
        if (thread == null || !thread.isAlive()) {
            thread = treading();
            thread.start();
            return;
        }
        console.out("Thread RabbitMQ already started.");
    }

    @Override
    public void putMsg(int appId, MsgType type, Object obgMsg) {
        Msg msg = new Msg(appId, config.getRabbitExchangeNameS(), config.getRabbitQueueNameS(), type, obgMsg);
        while (!queue.offer(msg)) {
            queue.remove();
            console.outRED("RabbitMQ queue OVERLOAD!");
        }
    }

    private Thread treading() {
        return new Thread() {
            @Override
            public void run() {
                console.out("Thread RabbitMQ start.");
                while (true) {
                    try {
                        Msg msg = queue.take();
                        while (!send(msg)) {
                            Thread.sleep(1000);
                        }
                    } catch (InterruptedException ex) {
                        break;
                    }
                }
                console.out("Thread RabbitMQ stop.");
            }
        };
    }

    private boolean send(Msg msg) {
        if (connection != null && connection.isOpen() && channel != null && channel.isOpen()) {
            try {
                channel.basicPublish(msg.exchangeName, msg.queueName, msg.type.getProps(msg.appId), gson.toJson(msg.msg).getBytes("UTF-8"));
                return true;
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public void shutdown() {
        if (thread != null && thread.isAlive()) {
            thread.interrupt();
            try {
                thread.join();
            } catch (InterruptedException ex) {
            }
        }
        try {
            channel.close();
        } catch (Exception ex) {
        }
        try {
            connection.close();
        } catch (Exception ex) {
        }
        console.out("RabbitMQ shutdown.");
    }

}
