package net.bor.discordguard.rabbit;

// @author ArtBorax
import java.io.IOException;
import java.util.concurrent.TimeoutException;

public interface IRabbit {

    public void init() throws IOException, TimeoutException;

    public void initProperties(int i);

    public void queueDeclareReceive(String queueName, IRabbitReceiving receive) throws IOException;

    public void start();

    public void putMsg(int appId, MsgType type, Object obgMsg);

    public void shutdown();
}
