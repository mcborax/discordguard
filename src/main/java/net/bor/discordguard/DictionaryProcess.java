package net.bor.discordguard;

// @author ArtBorax
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;
import net.bor.discordguard.config.Config;
import net.bor.discordguard.console.Console;

public class DictionaryProcess {

    private final static Pattern pParams = Pattern.compile("[А-Яа-я]+");

    public static void main(String[] args) {
        Console console = new Console();
        Config config = new Config(console);
        Set<String> dictionary = null;
        try {
            dictionary = config.load(Settings.DICTIONARY_FILENAME, Set.class);
            if (dictionary == null) {
                dictionary = new HashSet<>();
                config.save(dictionary, Settings.DICTIONARY_FILENAME);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        if (dictionary == null) {
            return;
        }

        int size = dictionary.size();
        console.out("Изначальный размер словаря: " + size);

        for (File file : new File(Settings.CONFIG_DIR + File.separator + "dicraw").listFiles()) {
            if (file.isFile()) {
                readFile(dictionary, file);
            }
        }
        console.out("Текущий размер словаря: " + dictionary.size() + ", увеличился на: " + (dictionary.size() - size));
        try {
            config.save(dictionary, Settings.DICTIONARY_FILENAME);
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

    private static void readFile(Set<String> dictionary, File file) {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
            String strLine;
            while ((strLine = br.readLine()) != null) {
                for (String word : strLine.replaceAll("\\p{Punct}", " ").trim().toLowerCase().split("\\s")) {
                    if (pParams.matcher(word.trim()).matches()) {
                        dictionary.add(word.trim());
                    }
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
