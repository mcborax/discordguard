package net.bor.discordguard.discordbot;

// @author ArtBorax
public interface IWebhookWorker {

    public void start();

    public void putMsg(WebhookMsg msg);

    public void shutdown();
}
