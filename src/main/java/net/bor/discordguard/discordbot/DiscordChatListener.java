package net.bor.discordguard.discordbot;

// @author ArtBorax
import net.bor.discordguard.core.IManager;
import net.bor.discordguard.core.discord.DiscordMsgEvent;
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public class DiscordChatListener extends ListenerAdapter {

    private final DiscordBot discordBot;
    private final int serverId;
    private final IManager manager;

    public DiscordChatListener(DiscordBot discordBot, int serverId, IManager manager) {
        this.discordBot = discordBot;
        this.serverId = serverId;
        this.manager = manager;
    }

    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        if (event.getAuthor() == null || event.getMember() == null || event.getAuthor().getId() == null || DiscordUtil.getJda() == null || DiscordUtil.getJda().getSelfUser() == null || DiscordUtil.getJda().getSelfUser().getId() == null || event.getAuthor().equals(DiscordUtil.getJda().getSelfUser())) {
            return;
        }
        if (discordBot.getChannel(serverId) == null || event.getChannel().getIdLong() != discordBot.getChannel(serverId).getIdLong()) {
            return;
        }
        manager.putQueue(new DiscordMsgEvent(discordBot, serverId, event.getMessage()));
    }
}
