package net.bor.discordguard.discordbot;

// @author ArtBorax
public interface IDiscordBot {

    public void start();

    public void shotdown();

    public void processLogMessage(String message, int time);

    public void processConsoleMessage(String message, int time);

    public void processChatMessage(int serverId, String message, int time);

    public void processPrivateMessage(Long discordUserId, String message);

}
