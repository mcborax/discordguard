package net.bor.discordguard.discordbot;

// @author ArtBorax
import java.util.List;
import java.util.function.Consumer;
import net.bor.discordguard.console.IConsole;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.ChannelType;
import net.dv8tion.jda.core.entities.Emote;
import net.dv8tion.jda.core.entities.Game;
import net.dv8tion.jda.core.entities.Game.GameType;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.PrivateChannel;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.exceptions.PermissionException;

public class DiscordUtil {

    private static IConsole console;
    private static DiscordBot discordBot;

    public static JDA getJda() {
        return discordBot.getJda();
    }

    public DiscordUtil(DiscordBot discordBot, IConsole console) {
        this.console = console;
        this.discordBot = discordBot;
    }

    public static void setGameStatus(String gameStatus) {
        if (getJda() == null) {
            console.out("Attempted to set game status using null JDA");
            return;
        }
        if (gameStatus.isEmpty()) {
            return;
        }
        getJda().getPresence().setGame(Game.of(GameType.DEFAULT, gameStatus));
    }

    public static TextChannel getTextChannelById(Long channelId) {
        try {
            return getJda().getTextChannelById(channelId);
        } catch (Exception ignored) {
        }
        return null;
    }

    public static void sendPrivateMessage(Long discordUserId, String message) {
        if (getJda() == null || message == null || discordUserId == null || discordUserId < 1000) {
            return;
        }
        try {
            sendPrivateMessage(getJda().getUserById(discordUserId).openPrivateChannel().complete(), message);
        } catch (Exception ignored) {
        }
    }

    private static void sendPrivateMessage(PrivateChannel channel, String message) {
        if (channel == null) {
            return;
        }
        message = message.trim();
        if (message.isEmpty()) {
            return;
        }
        String overflow = null;
        if (message.length() > 2000) {
            overflow = message.substring(2000);
            message = message.substring(0, 2000);
        }
        try {
            channel.sendMessage(message).nonce("").complete();
        } catch (Exception ex) {
        } finally {
            if (overflow == null) {
                try {
                    channel.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        if (overflow != null) {
            sendPrivateMessage(channel, overflow);
        }
    }

    /**
     * Send the given String message to the given TextChannel that will expire in x milliseconds
     *
     * @param channel the TextChannel to send the message to
     * @param message the message to send to the TextChannel
     * @param expiration milliseconds until expiration of message. if this is 0, the message will not expire
     */
    public static void sendMessage(TextChannel channel, String message, int expiration) {
        if (channel == null || getJda() == null || message == null) {
            return;
        }
        message = message.trim();
        if (message.isEmpty()) {
            return;
        }
        String overflow = null;
        if (message.length() > 2000) {
            overflow = message.substring(2000);
            message = message.substring(0, 2000);
        }
        queueMessage(channel, message, m -> {
            if (expiration > 0) {
                try {
                    Thread.sleep(expiration * 1000);
                } catch (InterruptedException e) {
                }
                deleteMessage(m);
            }
        });
        if (overflow != null) {
            sendMessage(channel, overflow, expiration);
        }
    }

    /**
     * Delete the given message, given the bot has permission to
     *
     * @param message The message to delete
     */
    public static void deleteMessage(Message message) {
        if (message.isFromType(ChannelType.PRIVATE)) {
            return;
        }
        try {
            message.delete().queue();
        } catch (PermissionException e) {
            if (e.getPermission() != Permission.UNKNOWN) {
                console.outRED("Could not delete message in channel " + message.getTextChannel() + " because the bot does not have the \"" + e.getPermission().getName() + "\" permission");
            } else {
                console.outRED("Could not delete message in channel " + message.getTextChannel() + " because \"" + e.getMessage() + "\"");
            }
        }
    }

    /**
     * Send the given message to the given channel, optionally doing something with the message via the given consumer
     *
     * @param channel The channel to send the message to
     * @param message The message to send to the channel
     * @param consumer The consumer to handle the message
     */
    public static void queueMessage(TextChannel channel, String message, Consumer<Message> consumer) {
        message = translateEmotes(message, channel.getGuild());
        queueMessage(channel, new MessageBuilder().append(message).build(), consumer);
    }

    /**
     * Send the given message to the given channel, optionally doing something with the message via the given consumer
     *
     * @param channel The channel to send the message to
     * @param message The message to send to the channel
     * @param consumer The consumer to handle the message
     */
    public static void queueMessage(TextChannel channel, Message message, Consumer<Message> consumer) {
        if (channel == null) {
            console.out("Tried sending a message to a null channel");
            return;
        }

        try {
            channel.sendMessage(message).queue(sentMessage -> {
                if (consumer != null) {
                    consumer.accept(sentMessage);
                }
            });
        } catch (PermissionException e) {
            if (e.getPermission() != Permission.UNKNOWN) {
                console.outRED("Could not send message in channel " + channel + " because the bot does not have the \"" + e.getPermission().getName() + "\" permission");
            } else {
                console.outRED("Could not send message in channel " + channel + " because \"" + e.getMessage() + "\"");
            }
        } catch (IllegalStateException ignored) {
        }
    }

    public static String translateEmotes(String messageToTranslate, Guild guild) {
        return translateEmotes(messageToTranslate, guild.getEmotes());
    }

    public static String translateEmotes(String messageToTranslate, List<Emote> emotes) {
        for (Emote emote : emotes) {
            messageToTranslate = messageToTranslate.replace(":" + emote.getName() + ":", emote.getAsMention());
        }
        return messageToTranslate;
    }
}
