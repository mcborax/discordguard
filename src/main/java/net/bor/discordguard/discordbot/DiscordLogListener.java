package net.bor.discordguard.discordbot;

// @author ArtBorax
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public class DiscordLogListener extends ListenerAdapter {

    private final DiscordBot discordBot;

    public DiscordLogListener(DiscordBot discordBot) {
        this.discordBot = discordBot;
    }

    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        if (event.getAuthor() == null || event.getMember() == null || event.getAuthor().getId() == null || DiscordUtil.getJda() == null || DiscordUtil.getJda().getSelfUser() == null || DiscordUtil.getJda().getSelfUser().getId() == null || event.getAuthor().equals(DiscordUtil.getJda().getSelfUser())) {
            return;
        }
        if (discordBot.getLogChannel() == null || event.getChannel().getIdLong() != discordBot.getLogChannel().getIdLong()) {
            return;
        }
        event.getMessage().delete().complete();
    }
}
