package net.bor.discordguard.discordbot;

// @author ArtBorax
public class WebhookMsg {

    private String username;
    private String msg;
    private String avatar;
    private String webHook;

    public WebhookMsg(String username, String msg, String avatar, String webHook) {
        this.username = username;
        this.msg = msg;
        this.avatar = avatar;
        this.webHook = webHook;
    }

    public WebhookMsg(String username, String msg, String webHook) {
        this.username = username;
        this.msg = msg;
        this.webHook = webHook;
    }

    public String getWebHook() {
        return webHook;
    }

    public String getMsg() {
        return msg;
    }

    public String getUsername() {
        return username;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
    
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public void setWebHook(String webHook) {
        this.webHook = webHook;
    }

}
