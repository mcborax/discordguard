package net.bor.discordguard.discordbot;

import net.bor.discordguard.core.IManager;
import net.bor.discordguard.core.discord.DiscordPrivateMsgEvent;
import net.dv8tion.jda.core.events.message.priv.PrivateMessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

// @author ArtBorax
public class DiscordPrivateChat extends ListenerAdapter {

    private final DiscordBot discordBot;
    private final IManager manager;

    public DiscordPrivateChat(DiscordBot discordBot, IManager manager) {
        this.discordBot = discordBot;
        this.manager = manager;
    }

    @Override
    public void onPrivateMessageReceived(PrivateMessageReceivedEvent event) {
        if (event.getAuthor().getIdLong() == event.getJDA().getSelfUser().getIdLong()) {
            return;
        }
        manager.putQueue(new DiscordPrivateMsgEvent(discordBot, event.getMessage()));
    }

}
