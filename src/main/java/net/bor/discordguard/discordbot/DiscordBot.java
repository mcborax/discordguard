package net.bor.discordguard.discordbot;

// @author ArtBorax
import java.util.HashMap;
import javax.security.auth.login.LoginException;
import net.bor.discordguard.config.IConfig;
import net.bor.discordguard.console.IConsole;
import net.bor.discordguard.core.IManager;
import net.bor.discordguard.lang.Lang;
import net.bor.discordguard.lang.LangType;
import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.TextChannel;

public class DiscordBot implements IDiscordBot {

    private static boolean isReady = false;

    private JDA jda;
    private TextChannel consoleChannel;
    private TextChannel logChannel;
    private HashMap<Integer, TextChannel> channels = new HashMap<>();

    private final IConfig config;
    private final IConsole console;
    private final IManager manager;

    public DiscordBot(IConfig config, IConsole console, IManager manager) {
        this.config = config;
        this.console = console;
        this.manager = manager;
        new DiscordUtil(this, console);
    }

    @Override
    public void start() {
        shotdown();
        // login to discord
        try {
            JDABuilder jdaBuild = new JDABuilder(AccountType.BOT)
                    .setAudioEnabled(false)
                    .setAutoReconnect(true)
                    .setBulkDeleteSplittingEnabled(false)
                    .setToken(config.getDiscordToken())
                    .addEventListener(new DiscordConsoleListener(this))
                    .addEventListener(new DiscordLogListener(this))
                    .addEventListener(new DiscordPrivateChat(this, manager));

            for (int serverId : config.getServersIdChanelId().keySet()) {
                jdaBuild.addEventListener(new DiscordChatListener(this, serverId, manager));
            }

            jda = jdaBuild.buildAsync();
        } catch (LoginException e) {
            console.outRED("LoginException: " + e.getMessage());
            return;
        } catch (Exception e) {
            console.outRED(Lang.getDefLangMsg(LangType.DISCORD_SYSTEM_ERROR_UNKNOWN));
            e.printStackTrace();
            return;
        }

        while (jda.getStatus() != JDA.Status.CONNECTED) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
            }
        }

        // game status
        if (!config.getDiscordGameStatus().isEmpty()) {
            DiscordUtil.setGameStatus(config.getDiscordGameStatus());
        }

        // распечатать, что видно боту
        for (Guild server : jda.getGuilds()) {
            console.out("server: " + server);
            for (TextChannel channel : server.getTextChannels()) {
                console.out("      - " + channel);
            }
        }

        if (jda.getGuilds().isEmpty()) {
            console.outRED(Lang.getDefLangMsg(LangType.DISCORD_SYSTEM_ERROR_EMPTY_CHANEL));
            return;
        }

        // set console channel
        if (config.getDiscordChanelIdConsole() > 100000000000000000l) {
            consoleChannel = DiscordUtil.getTextChannelById(config.getDiscordChanelIdConsole());
        }

//         set cens channel
        if (config.getDiscordChanelIdLog() > 100000000000000000l) {
            logChannel = DiscordUtil.getTextChannelById(config.getDiscordChanelIdLog());
        }

        channels.clear();
        for (int serverId : config.getServersIdChanelId().keySet()) {
            if (config.getServersIdChanelId().get(serverId) > 100000000000000000l) {
                channels.put(serverId, DiscordUtil.getTextChannelById(config.getServersIdChanelId().get(serverId)));
            }
        }

        // set ready status
        if (jda.getStatus() == JDA.Status.CONNECTED) {
            isReady = true;
        }
    }

    public JDA getJda() {
        return jda;
    }

    public TextChannel getConsoleChannel() {
        return consoleChannel;
    }

    public TextChannel getLogChannel() {
        return logChannel;
    }

    public HashMap<Integer, TextChannel> getChannels() {
        return channels;
    }

    public TextChannel getChannel(int serverId) {
        return channels.get(serverId);
    }

    public boolean isReady() {
        return isReady;
    }

    @Override
    public void processLogMessage(String message, int time) {
        DiscordUtil.sendMessage(getLogChannel(), message, time);
    }

    @Override
    public void processConsoleMessage(String message, int time) {
        DiscordUtil.sendMessage(getConsoleChannel(), message, time);
    }

    @Override
    public void processChatMessage(int serverId, String message, int time) {
        DiscordUtil.sendMessage(channels.get(serverId), message, time);
    }

    @Override
    public void processPrivateMessage(Long discordUserId, String message) {
        DiscordUtil.sendPrivateMessage(discordUserId, message);
    }

    @Override
    public void shotdown() {
        if (jda != null) {
            // удалить все листнеры
            jda.getRegisteredListeners().forEach(o -> jda.removeEventListener(o));
            try {
                jda.shutdown();
                jda = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
