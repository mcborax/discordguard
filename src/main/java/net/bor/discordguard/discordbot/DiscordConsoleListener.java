package net.bor.discordguard.discordbot;

// @author ArtBorax
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public class DiscordConsoleListener extends ListenerAdapter {

    private final DiscordBot discordBot;

    public DiscordConsoleListener(DiscordBot discordBot) {
        this.discordBot = discordBot;
    }

    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        if (event.getAuthor() == null || event.getMember() == null || event.getAuthor().getId() == null || DiscordUtil.getJda() == null || DiscordUtil.getJda().getSelfUser() == null || DiscordUtil.getJda().getSelfUser().getId() == null || event.getAuthor().equals(DiscordUtil.getJda().getSelfUser())) {
            return;
        }
        if (discordBot.getConsoleChannel() == null || event.getChannel().getIdLong() != discordBot.getConsoleChannel().getIdLong()) {
            return;
        }
        event.getMessage().delete().complete();
    }
}
