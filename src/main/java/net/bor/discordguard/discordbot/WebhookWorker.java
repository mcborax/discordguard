package net.bor.discordguard.discordbot;

// @author ArtBorax
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.request.body.MultipartBody;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import net.bor.discordguard.Settings;
import net.bor.discordguard.console.IConsole;

public class WebhookWorker implements IWebhookWorker {

    private final ArrayBlockingQueue<WebhookMsg> queue = new ArrayBlockingQueue(20000, true);
    private final IConsole console;
    private Thread thread;
    private ExecutorService taskExecutor;

    public WebhookWorker(IConsole console) {
        this.console = console;
    }

    @Override
    public void start() {
        if (thread == null || !thread.isAlive()) {
            thread = treading();
            if (taskExecutor == null) {
                taskExecutor = Executors.newFixedThreadPool(Settings.SIZE_POOL_WEBHOOK);
            }
            thread.start();
            return;
        }
        console.out("Thread WebHook already started.");
    }

    @Override
    public void shutdown() {
        if (thread != null && thread.isAlive()) {
            thread.interrupt();
            try {
                thread.join();
            } catch (InterruptedException ex) {
            }
            console.out("Thread WebHook shutdown.");
        }
        if (taskExecutor != null) {
            console.out("All tasks WebHook stops...");
            taskExecutor.shutdown();
            try {
                taskExecutor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
                console.out("All tasks WebHook stopped!");
            } catch (InterruptedException e) {
            }
            taskExecutor = null;
        }
    }

    @Override
    public void putMsg(WebhookMsg msg) {
        while (!queue.offer(msg)) {
            queue.remove();
            console.outRED("Queue WebHook OVERLOAD!");
        }
    }

    private Thread treading() {
        return new Thread() {
            @Override
            public void run() {
                console.out("Thread WebHook start.");
                while (true) {
                    try {
                        WebhookMsg msg = queue.take();
                        taskExecutor.execute(() -> {
                            int countSend = 1;
                            while (!send(msg)) {
                                if (countSend > 50) {
                                    console.outRED("Сообщение не отправлено [" + msg.getMsg() + "] после 50 попыток!");
                                    break;
                                }
                                try {
                                    if (countSend > 10) {
                                        Thread.sleep((long) (Math.random() * 10000));
                                    } else {
                                        Thread.sleep((long) (Math.random() * 1000 * countSend));
                                    }
                                } catch (InterruptedException ex) {
                                    break;
                                }
                                countSend++;
                            }
                        });
                    } catch (InterruptedException ex) {
                        break;
                    }
                }
                console.out("Thread WebHook stop.");
            }
        };
    }

    private boolean send(WebhookMsg msg) {
        if (msg.getWebHook() == null || msg.getWebHook().isEmpty() || msg.getMsg() == null || msg.getMsg().isEmpty()) {
            return true;
        }
        try {
            MultipartBody body = Unirest.post(msg.getWebHook()).field("content", msg.getMsg());

            if (msg.getUsername() != null && !msg.getUsername().isEmpty()) {
                body.field("username", msg.getUsername());
            }
            if (msg.getAvatar() != null && !msg.getAvatar().isEmpty()) {
                body.field("avatar_url", msg.getAvatar());
            }
            int stat = body.asString().getStatus();
            if (stat == 204) {
                return true;
            }
        } catch (Exception e) {
            console.outRED("Failed to deliver webhook message to Discord: " + e.getMessage());
            e.printStackTrace();
        }
        return false;
    }

}
