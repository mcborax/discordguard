package net.bor.discordguard.core;

// @author ArtBorax

public interface IManager {

    public void putQueue(Object msg);
    
    public void shutdown();
}
