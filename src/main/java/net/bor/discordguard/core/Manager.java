package net.bor.discordguard.core;

// @author ArtBorax
import com.google.gson.Gson;
import java.io.IOException;
import java.sql.SQLException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import net.bor.discordguard.Settings;
import net.bor.discordguard.cens.Cens;
import net.bor.discordguard.cens.ICens;
import net.bor.discordguard.config.Config;
import net.bor.discordguard.config.IConfig;
import net.bor.discordguard.console.Console;
import net.bor.discordguard.console.IConsole;
import net.bor.discordguard.core.command.CommandAddReport;
import net.bor.discordguard.core.command.CommandConsiderReport;
import net.bor.discordguard.core.command.CommandCreateReport;
import net.bor.discordguard.core.command.CommandLink;
import net.bor.discordguard.core.command.CommandLogChat;
import net.bor.discordguard.core.command.CommandMainMenu;
import net.bor.discordguard.core.command.CommandPlayerStat;
import net.bor.discordguard.core.command.CommandReportInfo;
import net.bor.discordguard.core.command.CommandReports;
import net.bor.discordguard.core.command.CommandTop;
import net.bor.discordguard.core.command.Utils;
import net.bor.discordguard.core.discord.DiscordMsgEvent;
import net.bor.discordguard.core.discord.DiscordPrivateMsgEvent;
import net.bor.discordguard.core.discord.DiscordUtils;
import net.bor.discordguard.dao.map.Player;
import net.bor.discordguard.dao.map.Server;
import net.bor.discordguard.db.DataBase;
import net.bor.discordguard.db.IDataBase;
import net.bor.discordguard.discordbot.DiscordBot;
import net.bor.discordguard.discordbot.IDiscordBot;
import net.bor.discordguard.discordbot.IWebhookWorker;
import net.bor.discordguard.discordbot.WebhookWorker;
import net.bor.discordguard.karma.IKarma;
import net.bor.discordguard.karma.Karma;
import net.bor.discordguard.lang.ILangs;
import net.bor.discordguard.lang.Lang;
import net.bor.discordguard.map.ActionPlayer;
import net.bor.discordguard.map.ReportMessage;
import net.bor.discordguard.map.ServerStat;
import net.bor.discordguard.rabbit.IRabbit;
import net.bor.discordguard.rabbit.MsgType;
import net.bor.discordguard.rabbit.Rabbit;

public class Manager implements IManager {

    private final ArrayBlockingQueue<Object> queue = new ArrayBlockingQueue(1000, true);

    public final IConsole console;
    public final IConfig config;
    public final ILangs lang;
    public final IWebhookWorker webhookWorker;
    public final IDataBase db;
    public final IRabbit rabbit;
    public final ICens cens;
    public final IKarma karma;
    public final IDiscordBot discordBot;
    private ExecutorService taskExecutor;
    private Thread bossThread;

    public static Manager inst = null;

    public Manager() {
        inst = this;
        console = new Console();
        config = new Config(console);
        lang = new Lang(config);
        cens = new Cens(console, config);
        karma = new Karma(console, config);
        db = new DataBase(config, console);
        webhookWorker = new WebhookWorker(console);
        rabbit = new Rabbit(config, console);
        discordBot = new DiscordBot(config, console, this);
    }

    public void start() throws IOException, TimeoutException {
        lang.reload();
        addNickNameToDictionary();
        stopThreads();
        initThreads();
        startThreads();
        webhookWorker.start();
        rabbit.init();
        try {
            db.getServers().stream().forEach((server) -> {
                rabbit.initProperties(server.getId());
            });
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        rabbitListeners();
        rabbit.start();
        discordBot.start();
//        test();
    }

    private void addNickNameToDictionary() {
        console.outYELLOW("Size dictionary: " + cens.getDictionary().size());
        try {
            for (String nick : db.getAllUsernamePlayers()) {
                cens.getDictionary().add(nick.toLowerCase());
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        console.outYELLOW("Size dictionary+username: " + cens.getDictionary().size());
    }

    private void initThreads() {
        bossThread = getThreadBoss();
    }

    private void startThreads() {
        if (taskExecutor == null) {
            taskExecutor = Executors.newFixedThreadPool(Settings.SIZE_POOL);
        }
        if (bossThread != null && !bossThread.isAlive()) {
            bossThread.start();
        }
    }

    private void stopThreads() {
        if (bossThread != null && bossThread.isAlive()) {
            bossThread.interrupt();
            try {
                bossThread.join();
            } catch (InterruptedException ex) {
            }
        }
        if (taskExecutor != null) {
            console.out("All tasks stops...");
            taskExecutor.shutdown();
            try {
                taskExecutor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
                console.out("All tasks stopped!");
            } catch (InterruptedException e) {
            }
            taskExecutor = null;
        }
    }

    public void test() {
        System.out.println("!!!!!! start add !!!!!");
        ActionPlayer msg = new ActionPlayer();
        msg.setPlayerId(config.getAdminPlayerId());
        msg.setServerId(12);
        for (int i = 0; i < 500; i++) {
            msg.setMsg(Integer.toString(i));
            msg.setType(MsgType.GET_LOG_CHAT.toString());
            queue.add(msg);
        }
        while (!queue.isEmpty()) {
            System.out.println(queue.size());
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
            }
        }
        System.out.println("!!!!!! complete add !!!!!");
    }

    private Thread getThreadBoss() {
        Thread t = new Thread() {
            @Override
            public void run() {
                console.out("Thread Boss Manager start.");
                while (true) {
                    try {
                        Object obj = queue.take();
                        if (obj == null) {
                            continue;
                        }
                        taskExecutor.execute(() -> {
                            if (obj instanceof ActionPlayer) {
                                ActionPlayer msg = (ActionPlayer) obj;
                                switch (MsgType.getType(msg.getType())) {
                                    case PLAYER_JOIN:
                                        ManagerUtils.joinPlayer(msg);
                                        break;
                                    case ACTION_PLAYER:
                                        ManagerUtils.actionPlayer(msg.getServerId(), msg.getPlayerId(), msg.getKarma());
                                        break;
                                    case CHAT_MSG:
                                        processChat(msg);
                                        break;
                                    case PLAYER_QUIT:
                                        ManagerUtils.quitPlayer(msg);
                                        break;
                                    case GET_REPORTS:
                                        CommandReports.execute(msg);
                                        break;
                                    case GET_LOG_CHAT:
                                        CommandLogChat.execute(msg);
                                        break;
                                    case GET_MENU_MAIN:
                                        CommandMainMenu.execute(msg.getServerId(), msg.getPlayerId());
                                        break;
                                    case GET_SHOW_KARMA:
                                        Utils.sendChatKarma(msg.getServerId(), msg.getPlayerId());
                                        break;
                                    case GET_TOP_KARMA:
                                        CommandTop.execute(msg.getServerId(), msg.getPlayerId());
                                        break;
                                    case GET_PLAYER_STAT:
                                        CommandPlayerStat.execute(msg);
                                        break;
                                    case DISCORD_LINK:
                                        CommandLink.execute(msg);
                                        break;
                                }
                            } else if (obj instanceof ReportMessage) {
                                ReportMessage rm = (ReportMessage) obj;
                                switch (MsgType.getType(rm.getTypeMsg())) {
                                    case REPORT_CREATE:
                                    case REPORT_CREATE_ADD:
                                    case REPORT_CREATE_COMPL:
                                        CommandCreateReport.execute(rm);
                                        break;
                                    case REPORT_ADD:
                                    case REPORT_ADD_COMPL:
                                        CommandAddReport.execute(rm);
                                        break;
                                    case REPORT_CONS:
                                        CommandConsiderReport.execute(rm);
                                        break;
                                    case REPORT_INFO:
                                        CommandReportInfo.execute(rm);
                                        break;
                                }
                            } else if (obj instanceof ServerStat) {
                                ManagerUtils.serverPing((ServerStat) obj);
                            } else if (obj instanceof DiscordMsgEvent) {
                                DiscordUtils.processDiscordMsg((DiscordMsgEvent) obj);
                            } else if (obj instanceof DiscordPrivateMsgEvent) {
                                DiscordUtils.processDiscordMsg((DiscordPrivateMsgEvent) obj);
                            }
                        });
                    } catch (InterruptedException ex) {
                        break;
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
                console.out("Thread Boss Manager stop.");
            }
        };
        t.setName("Manager Boss");
        return t;
    }

    private void processChat(ActionPlayer msg) {
        try {
            Server server = db.getServer(msg.getServerId());

            if (server == null || server.getDiscordWebhook() == null || server.getDiscordWebhook().isEmpty()) {
                return;
            }
            Player player = ManagerUtils.getPlayer(server, msg);
            if (player != null) {
                ManagerUtils.processChat(server, player, msg);
            } else {
                console.outRED("!!!В базе не найден игрок!!! [" + msg.getServerId() + "][" + msg.getPlayerId() + "][" + msg.getUsername() + "][" + msg.getUuid() + "]");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    private void rabbitListeners() throws IOException {
        rabbit.queueDeclareReceive(config.getRabbitQueueNameC(), (String appId, String messageId, String rawMsg) -> {
            if (appId == null || appId.isEmpty()) {
                return false;
            }
            int serverId = 0;
            try {
                serverId = Integer.parseInt(appId);
            } catch (Exception ex) {
                return false;
            }
            if (serverId == 0) {
                return false;
            }
            switch (MsgType.getType(messageId)) {
                case CHAT_MSG:
                case ACTION_PLAYER:
                case PLAYER_JOIN:
                case PLAYER_QUIT:
                case GET_REPORTS:
                case GET_MENU_MAIN:
                case GET_SHOW_KARMA:
                case GET_LOG_CHAT:
                case GET_TOP_KARMA:
                case GET_PLAYER_STAT:
                case DISCORD_LINK:
                    ActionPlayer msg = new Gson().fromJson(rawMsg, ActionPlayer.class);
                    msg.setServerId(serverId);
                    putQueue(msg);
                    break;
                case REPORT_ADD:
                case REPORT_ADD_COMPL:
                case REPORT_CREATE:
                case REPORT_CREATE_ADD:
                case REPORT_CREATE_COMPL:
                case REPORT_CONS:
                case REPORT_INFO:
                    ReportMessage rm = new Gson().fromJson(rawMsg, ReportMessage.class);
                    rm.setServerId(serverId);
                    putQueue(rm);
                    break;
                case PING:
                    putQueue(new Gson().fromJson(rawMsg, ServerStat.class));
                    break;
                case UNKNOW:
                    return false;
            }
            return true;
        });
    }

    @Override
    public void putQueue(Object msg) {
        try {
            queue.put(msg);
        } catch (InterruptedException ex) {
        }
    }

    @Override
    public void shutdown() {
        Thread t = new Thread() {
            @Override
            public void run() {
                rabbit.shutdown();
                stopThreads();
                discordBot.shotdown();
                webhookWorker.shutdown();
                db.shutdown();
                System.out.println("!!!STOP!!!");
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex) {
                }
            }
        };
        t.setName("Stop thread");
        t.start();
    }

}
