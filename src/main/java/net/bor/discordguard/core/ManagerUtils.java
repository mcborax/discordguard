package net.bor.discordguard.core;

// @author ArtBorax
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;
import net.bor.discordguard.cens.ResultCens;
import net.bor.discordguard.dao.map.Player;
import net.bor.discordguard.dao.map.PlayerInfo;
import net.bor.discordguard.dao.map.Report;
import net.bor.discordguard.dao.map.ReportResult;
import net.bor.discordguard.dao.map.ReportType;
import net.bor.discordguard.dao.map.Server;
import net.bor.discordguard.dao.map.User;
import net.bor.discordguard.discordbot.WebhookMsg;
import net.bor.discordguard.lang.Lang;
import net.bor.discordguard.lang.LangType;
import net.bor.discordguard.map.ActionPlayer;
import net.bor.discordguard.map.PlayerStat;
import net.bor.discordguard.map.Properties;
import net.bor.discordguard.map.ServerStat;
import net.bor.discordguard.rabbit.MsgType;

public class ManagerUtils {

    private final static ConcurrentHashMap<Integer, FloodMsg> floodMsgs = new ConcurrentHashMap<>();

    private static final Pattern pHack = Pattern.compile("cmd:");

    public static void actionPlayer(int serverId, int playerId, int action) {
        try {
            Player player = Manager.inst.db.getPlayer(playerId);
            if (player == null) {
                return;
            }
            double resultKarma = Manager.inst.karma.getProcessKarma(player.getKarma(), player.getKarmaSpeed()) + Manager.inst.karma.getActionPlayer(action, player.getKarma());
            double resultKarmaSpeed = Manager.inst.karma.getProcessKarmaSpeed(player.getKarma(), player.getKarmaSpeed());
            if (resultKarma != player.getKarma() || resultKarmaSpeed != player.getKarmaSpeed()) {
                player.setKarma(resultKarma);
                player.setKarmaSpeed(resultKarmaSpeed);
                Manager.inst.db.updatePlayer(player);
                sendChangeKarma(serverId, player);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static void serverPing(ServerStat serverStat) {
        HashMap<Integer, HashMap<Integer, Long>> lastOnlinePlayers = OnlineServer.updateOnline(serverStat.getServerId(), serverStat.getPlayers());
        for (int serverId : lastOnlinePlayers.keySet()) {
            for (int playerId : lastOnlinePlayers.get(serverId).keySet()) {
                if (lastOnlinePlayers.get(serverId).get(playerId) > 0) {
                    try {
                        Player player = Manager.inst.db.getPlayer(playerId);
                        if (player != null) {
                            PlayerInfo playerInfo = Manager.inst.db.getPlayerInfo(serverId, player);
                            if (playerInfo != null) {
                                playerInfo.setTimeOnline(playerInfo.getTimeOnline() + lastOnlinePlayers.get(serverId).get(playerId));
                                Manager.inst.db.updatePlayerInfo(playerInfo);
                            }
                        }
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        }
    }

    public static void serverOfflineNow(int serverId) {
        Manager.inst.console.outRED("Server id:".concat(Integer.toString(serverId)).concat(" offline!"));
        try {
            Server server = Manager.inst.db.getServer(serverId);
            if (server != null) {
                Manager.inst.webhookWorker.putMsg(new WebhookMsg(server.getName(), Lang.getDefLangMsg(LangType.DISCORD_MSG_SYSTEM_SERVER_OFF), server.getAvatar(), server.getDiscordWebhook()));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static void serverOnlineNow(int serverId) {
        Manager.inst.console.outGREEN("Server id:".concat(Integer.toString(serverId)).concat(" online!"));
        sendProperties(serverId);
        try {
            Server server = Manager.inst.db.getServer(serverId);
            if (server != null) {
                Manager.inst.webhookWorker.putMsg(new WebhookMsg(server.getName(), Lang.getDefLangMsg(LangType.DISCORD_MSG_SYSTEM_SERVER_ON), server.getAvatar(), server.getDiscordWebhook()));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static void sendProperties(int serverId) {
        Manager.inst.rabbit.putMsg(serverId, MsgType.SYS_PROPERTIES, new Properties(Manager.inst.cens.getCmds()));
    }

    public static void joinPlayer(ActionPlayer msg) {
        Player player = getPlayer(msg);
        if (player != null) {
            if (msg.getUuid() != null && !msg.getUuid().equals("00000000-0000-0000-0000-000000000000") && player.getUuid() != null && player.getUuid().equals("00000000-0000-0000-0000-000000000000")) {
                try {
                    player.setUuid(msg.getUuid().toLowerCase());
                    Manager.inst.db.updatePlayer(player);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                    player.setUuid("00000000-0000-0000-0000-000000000000");
                }
            }
            OnlineServer.updateOnlinePlayer(msg.getServerId(), player.getPlayerId());
            try {
                PlayerInfo playerInfo = Manager.inst.db.getPlayerInfo(msg.getServerId(), player);
                if (playerInfo == null) {
                    playerInfo = Manager.inst.db.createPlayerInfo(msg.getServerId(), player);
                }
                if (playerInfo == null) {
                    return;
                }
                playerInfo.setTimeJoin(msg.getDatetime());
                playerInfo.setJoinCount(playerInfo.getJoinCount() + 1);
                Manager.inst.db.updatePlayerInfo(playerInfo);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            sendChangeKarma(msg.getServerId(), player);

            if (Manager.inst.karma.idModerationByDictionary(player.getKarma(), player.getKarmaSpeed())) {
                sendCommandModerationChangeOn(msg.getServerId(), player);
            }
        }
    }

    public static void quitPlayer(ActionPlayer msg) {
        Player player = getPlayer(msg);
        if (player != null) {
            long lastUpdate = OnlineServer.removeOnlinePlayer(msg.getServerId(), msg.getPlayerId());
            try {
                PlayerInfo playerInfo = Manager.inst.db.getPlayerInfo(msg.getServerId(), player);
                if (playerInfo == null) {
                    return;
                }
                playerInfo.setTimeQuit(msg.getDatetime());
                if (lastUpdate > 0) {
                    playerInfo.setTimeOnline(playerInfo.getTimeOnline() + lastUpdate);
                }
                Manager.inst.db.updatePlayerInfo(playerInfo);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    public static void sendChatKarma(Player player, ActionPlayer ap) {
        int max = (int) Math.max(Math.max(Math.abs(player.getKarma()), Math.abs(player.getKarmaSpeed())), Math.abs(player.getKarma() + player.getKarmaSpeed()));
        int size = (((max / 100) + 1) * 100);
        double krm = player.getKarma() + player.getKarmaSpeed();
        StringBuilder sb = new StringBuilder();
        for (int i = 0 - size; i <= size; i = i + (size / 19)) {
            if (i < (size / 19) / 2 && i > 0 - (size / 19) / 2 && krm >= 0) {
                sb.append("§20");
            } else if (i < (size / 19) / 2 && i > 0 - (size / 19) / 2 && krm < 0) {
                sb.append("§40");
            } else if (i < 0 && player.getKarma() <= i) {
                sb.append("§4|");
            } else if (i < 0 && krm <= i) {
                sb.append("§c|");
            } else if (i > 0 && player.getKarma() >= i) {
                sb.append("§2|");
            } else if (i > 0 && krm >= i) {
                sb.append("§a|");
            } else {
                sb.append("§8|");
            }
        }
        ap.addMsgs(sb.toString());
        if (krm >= 0) {
            ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_KARMA_PLAYER_POS, Integer.toString((int) (krm))));
        } else {
            ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_KARMA_PLAYER_NEG, Integer.toString((int) (krm))));
        }
    }

    public static void sendChangeKarma(Player player) {
        OnlineServer.getServersOnlinePlayer(player.getPlayerId()).stream().forEach((serverId) -> {
            sendChangeKarma(serverId, player);
        });
    }

    public static void sendChangeKarma(int serverId, Player player) {
        Manager.inst.rabbit.putMsg(serverId, MsgType.KARMA_CHANGE, new PlayerStat(player.getPlayerId(), player.getUsername(), player.getUuid(), player.getKarma(), player.getKarmaSpeed()));
    }

    public static void processChat(Server server, Player player, ActionPlayer msg) {
        if (msg.isConfirmation() && Manager.inst.karma.isMute(player.getKarma(), player.getKarmaSpeed())) {
            if (msg.getCmd() != null && !msg.getCmd().isEmpty()) {
                if (Manager.inst.cens.isCensCmd(msg.getCmd())) {
                    sendMessagePlayer(server, player, Lang.getDefLangMsg(LangType.GAME_MSG_MUTE));
                    return;
                }
            } else {
                sendMessagePlayer(server, player, Lang.getDefLangMsg(LangType.GAME_MSG_MUTE));
                return;
            }
        }

        try {
            if (msg.getCmd() != null && !msg.getCmd().isEmpty()) {
                if (Manager.inst.cens.isCensCmd(msg.getCmd())) {
                    processChatMsg(server, player, msg, true);
                } else {
                    addDBAndWebhook(server, player, msg, 0);
                }
            } else {
                processChatMsg(server, player, msg, false);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static String checkMsgDiscordAndAddDB(int serverId, Player player, String msg, Date date) throws SQLException {
        ResultCens resultCens = Manager.inst.cens.getCens(msg, false);
        if (Manager.inst.karma.getPenaltyOfLevel(resultCens.getLevel(), player.getKarma()) < 0 && !resultCens.isCensEmpty()) {
            return Lang.getDefLangMsg(LangType.DISCORD_MSG_SEND_CENS);
        } else {
            int hashMsg = msg.hashCode() + serverId + player.getPlayerId();
            long curtime = date.getTime();
            FloodMsg newmsg = new FloodMsg(curtime);
            FloodMsg result = floodMsgs.put(hashMsg, newmsg);
            if (result != null && curtime - result.getTime() < 120000) {
                newmsg.setCount(result.getCount() + 1);
                if (result.getCount() >= 1 && result.getCount() <= 2) {
                    Manager.inst.discordBot.processPrivateMessage(player.getDiscordUserID(), Lang.getDefLangMsg(LangType.DISCORD_MSG_FLOOD_WARN));
                } else if (result.getCount() > 2) {
                    return Lang.getDefLangMsg(LangType.DISCORD_MSG_SEND_FLOOD);
                }
            }
            if (floodMsgs.size() > 3000) {
                long curTime = System.currentTimeMillis() - 120000;
                floodMsgs.values().removeIf(item -> {
                    return item.getTime() < curTime;
                });
            }
        }
        if (serverId != 0) {
            Server server = Manager.inst.db.getServer(serverId);
            if (server == null) {
                return Lang.getDefLangMsg(LangType.DISCORD_MSG_ERROR_SERVER);
            }
            Manager.inst.db.addServerChat(server, player, msg, date, 0, 3);
        } else {
            boolean flag = true;
            for (Integer srv : OnlineServer.getServersOnline()) {
                Server server = Manager.inst.db.getServer(srv);
                if (server != null) {
                    Manager.inst.db.addServerChat(server, player, msg, date, 0, 3);
                    flag = false;
                }
            }
            if (flag) {
                return Lang.getDefLangMsg(LangType.DISCORD_MSG_ERROR_SERVER);
            }
        }
        return null;
    }

    private static void addDBAndWebhook(Server server, Player player, ActionPlayer msg, int level) throws SQLException {
        int type = 0;
        String stmsg = null;
        if (msg.getCmd() == null || msg.getCmd().isEmpty()) {
            type = 0;
            stmsg = msg.getMsg();
        } else if (msg.getMsg() == null || msg.getMsg().isEmpty()) {
            stmsg = msg.getCmd();
            if (Manager.inst.cens.isCensCmd(msg.getCmd())) {
                type = 1;
            } else {
                type = 2;
            }
        } else {
            stmsg = msg.getCmd().concat(" ").concat(msg.getMsg());
            if (Manager.inst.cens.isCensCmd(msg.getCmd())) {
                type = 1;
            } else {
                type = 2;
            }
        }
        if (stmsg == null) {
            return;
        }
        if (type < 2) {
            String resultMsg = shieldingMarkdown((msg.getCmd() == null || msg.getCmd().isEmpty())
                    ? msg.getMsg() : "/".concat(((msg.getMsg() == null || msg.getMsg().isEmpty())
                            ? msg.getCmd() : msg.getCmd().concat(" ").concat(msg.getMsg()))));
            Manager.inst.webhookWorker.putMsg(new WebhookMsg(server.getName().concat(" ").concat(msg.getUsername()), resultMsg, Manager.inst.config.getDiscordAvatarURL().concat(msg.getUsername()), Manager.inst.config.getDiscordHookAll()));
            Manager.inst.webhookWorker.putMsg(new WebhookMsg(msg.getUsername(), resultMsg, Manager.inst.config.getDiscordAvatarURL().concat(msg.getUsername()), server.getDiscordWebhook()));
        }
        Manager.inst.db.addServerChat(server, player, stmsg, msg.getDatetime(), level, type);
    }

    private static ResultCens checkAndCreateReport(Server server, Player player, ActionPlayer msg) throws SQLException {
        ResultCens resultCens = Manager.inst.cens.getCens(msg.getMsg(), Manager.inst.karma.idModerationByDictionary(player.getKarma(), player.getKarmaSpeed()));
        int penalty = Manager.inst.karma.getPenaltyOfLevel(resultCens.getLevel(), player.getKarma());
        resultCens.setPenalty(penalty);
        if (penalty < 0 && !msg.isConfirmation()) {
            //1 создать репорт, 2 отправить его игроку, 3 всем подписанным игрокам, 4 в дискорд подписчикам и в дискорд канал
            ReportType reportType = Manager.inst.db.getReportType(Manager.inst.config.getReportIdChat());
            Player creator = Manager.inst.db.getPlayer(Manager.inst.config.getAdminPlayerId());
            if (reportType != null && creator != null) {
                Date time = new Date(System.currentTimeMillis());
                Report report = createReport(server, creator, player, reportType, time);
                if (report != null) {
                    ReportResult reportResult = createReportAndCompl(report, server, creator, Lang.getDefLangMsg(LangType.DESCRIPT_WORD_CENS, resultCens.getCensAllWords()), time, resultCens.getLevel());
                    if (reportResult != null) {
                        player.setKarmaSpeed(player.getKarmaSpeed() + penalty);
                        if (Manager.inst.db.updatePlayer(player)) {
                            //отправить новую карму на сервер
                            sendChangeKarma(server.getId(), player);
                            ActionPlayer ap = new ActionPlayer();
                            ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_REPORT_CHANGE_CREATE, report.getCreator().getUsername(), Integer.toString(report.getId()), reportType.getShortDesription(), report.getServer().getName(), report.getAccused().getUsername()));
                            ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_REPORTINFO, Integer.toString(report.getId())), Lang.getDefLangMsg(LangType.GAME_MSG_REPORTINFO_CMD, Integer.toString(report.getId())));
                            sendMessagePlayer(null, null, ap);
                            //**{0}** создал репорт: __#{1}__, на: **{2}**, сервер: _{3}_, тип: _{4}_, степень: **{5}**, описание: {6}.
                            Manager.inst.discordBot.processLogMessage(Lang.getDefLangMsg(LangType.DISCORD_MSG_REPORT_NEWCONSAUTO,
                                    shieldingMarkdown(creator.getUsername()),
                                    Integer.toString(report.getId()),
                                    shieldingMarkdown(player.getUsername()),
                                    server.getName(),
                                    reportType.getShortDesription(),
                                    Integer.toString(resultCens.getLevel()),
                                    Lang.getDefLangMsg(LangType.DESCRIPT_WORD_CENS, shieldingMarkdown(resultCens.getCensAllWords()))), 0);

                        } else {
                            Manager.inst.console.outRED("Не получилось обновить плеера при создании репорта!");
                        }
                    }
                }
            }
        }
        return resultCens;
    }

    private static boolean isFloodMsg(Server server, Player player, ActionPlayer msg) throws SQLException {
        String compMsg = null;
        if (msg.getCmd() != null && !msg.getCmd().isEmpty()) {
            if (Manager.inst.cens.isCensCmd(msg.getCmd())) {
                compMsg = "/".concat(msg.getCmd());
            }
        }
        if (msg.getMsg() != null && !msg.getMsg().isEmpty()) {
            if (compMsg == null) {
                compMsg = msg.getMsg();
            } else {
                compMsg = compMsg.concat(" ").concat(msg.getMsg());
            }
        }
        if (compMsg != null) {
            int hashMsg = compMsg.hashCode() + server.getId() + player.getPlayerId();
            long curtime = msg.getDatetime().getTime();
            FloodMsg newmsg = new FloodMsg(curtime);
            FloodMsg result = floodMsgs.put(hashMsg, newmsg);
            if (result != null && curtime - result.getTime() < 120000) {
                newmsg.setCount(result.getCount() + 1);
                if (result.getCount() >= 2 && msg.isConfirmation()) {
                    sendMessagePlayer(server, player, Lang.getDefLangMsg(LangType.GAME_MSG_FLOOD_WARN));
                    return true;
                }
                if (result.getCount() >= 2 && result.getCount() <= 3) {
                    sendMessagePlayer(server, player, Lang.getDefLangMsg(LangType.GAME_MSG_FLOOD_WARN));
                } else if (result.getCount() > 3) {
                    ReportType reportType = Manager.inst.db.getReportType(Manager.inst.config.getReportIdChat());
                    Player creator = Manager.inst.db.getPlayer(Manager.inst.config.getAdminPlayerId());
                    if (reportType != null && creator != null) {
                        Date time = new Date(System.currentTimeMillis());
                        Report report = createReport(server, creator, player, reportType, time);
                        if (report != null) {
                            ReportResult reportResult = createReportAndCompl(report, server, creator, Lang.getDefLangMsg(LangType.DESCRIPT_FLOOD, compMsg), time, 1);
                            if (reportResult != null) {
                                player.setKarmaSpeed(player.getKarmaSpeed() + Manager.inst.karma.getPenaltyOfLevel(1, player.getKarma()));
                                if (Manager.inst.db.updatePlayer(player)) {
                                    //отправить новую карму на сервер
                                    sendChangeKarma(server.getId(), player);
                                    ActionPlayer ap = new ActionPlayer();
                                    ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_REPORT_CHANGE_CREATE, report.getCreator().getUsername(), Integer.toString(report.getId()), reportType.getShortDesription(), report.getServer().getName(), report.getAccused().getUsername()));
                                    ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_REPORTINFO, Integer.toString(report.getId())), Lang.getDefLangMsg(LangType.GAME_MSG_REPORTINFO_CMD, Integer.toString(report.getId())));
                                    sendMessagePlayer(null, null, ap);

                                    //**{0}** создал репорт: __#{1}__, на: **{2}**, сервер: _{3}_, тип: _{4}_, степень: **{5}**, описание: {6}.
                                    Manager.inst.discordBot.processLogMessage(Lang.getDefLangMsg(LangType.DISCORD_MSG_REPORT_NEWCONSAUTO,
                                            shieldingMarkdown(creator.getUsername()),
                                            Integer.toString(report.getId()),
                                            shieldingMarkdown(player.getUsername()),
                                            server.getName(),
                                            reportType.getShortDesription(),
                                            "1",
                                            Lang.getDefLangMsg(LangType.DESCRIPT_FLOOD, shieldingMarkdown(compMsg))), 0);
                                } else {
                                    Manager.inst.console.outRED("Не получилось обновить плеера при создании репорта!");
                                }
                            }
                        }
                    }
                }
            }
            if (floodMsgs.size() > 3000) {
                long curTime = System.currentTimeMillis() - 120000;
                floodMsgs.values().removeIf(item -> {
                    return item.getTime() < curTime;
                });
            }
        }
        return false;
    }

    private static void processChatMsg(Server server, Player player, ActionPlayer msg, boolean cmd) throws SQLException {
        if (isFloodMsg(server, player, msg)) {
            return;
        }
        ResultCens resultCens = checkAndCreateReport(server, player, msg);
        if (resultCens.getModerationWords() == null) {
            //значит достаточно кармы и модерация неактивна
            if (msg.isConfirmation()) {
                //это сообщение ждет подтверждения на отправку на сервере
                sendCommandModerationChange(server, player, false);
                if (!cmd) {
                    sendMessageConfirmed(server, player, msg.getMsg(), null);
                } else if (msg.getMsg() != null && !msg.getMsg().isEmpty()) {
                    sendMessageConfirmed(server, player, null, msg.getCmd().concat(" ").concat(msg.getMsg()));
                } else {
                    sendMessageConfirmed(server, player, null, msg.getCmd());
                }
            }
            addDBAndWebhook(server, player, msg, resultCens.getLevel());
        } else if (resultCens.getModerationWords().isEmpty() && resultCens.getPenalty() >= 0 && resultCens.isCensEmpty()) {
            //сообщение прошло модерацию
            if (!msg.isConfirmation()) {
                sendCommandModerationChange(server, player, true);
            } else if (!cmd) {
                sendMessageConfirmed(server, player, msg.getMsg(), null);
            } else if (msg.getMsg() != null && !msg.getMsg().isEmpty()) {
                sendMessageConfirmed(server, player, null, msg.getCmd().concat(" ").concat(msg.getMsg()));
            } else {
                sendMessageConfirmed(server, player, null, msg.getCmd());
            }
            addDBAndWebhook(server, player, msg, resultCens.getLevel());
        } else if (!msg.isConfirmation()) {
            sendCommandModerationChange(server, player, true);
            addDBAndWebhook(server, player, msg, resultCens.getLevel());
        } else {
            sendMessagePlayer(server, player, Lang.getDefLangMsg(LangType.GAME_MSG_MODER_CANCEL));
            //возможно отправить список слов которые не прошли модерацию
        }
    }

    public static Player getPlayer(ActionPlayer msg) {
        return getPlayer(msg.getServerId(), msg);
    }

    public static Player getPlayer(Server server, ActionPlayer msg) {
        return getPlayer(server.getId(), msg);
    }

    public static Player getPlayer(int serverId, ActionPlayer msg) {
        Player player = null;
        try {
            if (msg.getPlayerId() == 0) {
                User user = Manager.inst.db.getUser(msg.getUsername(), msg.getUuid());
                if (user != null) {
                    player = Manager.inst.db.getOrCreatePlayer(user, Manager.inst.karma.getNewPlayer());
                    if (player != null) {
                        Manager.inst.rabbit.putMsg(serverId, MsgType.STAT_PLAYER, new PlayerStat(player.getPlayerId(), player.getUsername(), player.getUuid(), player.getKarma(), player.getKarmaSpeed()));
                    }
                }
            } else {
                player = Manager.inst.db.getPlayer(msg.getPlayerId());
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return player;
    }

    public static void sendCommandModerationChangeOn(int serverId, Player player) {
        ActionPlayer chat = new ActionPlayer();
        chat.setConfirmation(true);
        chat.setPlayerId(player.getPlayerId());
        chat.setUsername(player.getUsername());
        chat.setUuid(player.getUuid());
        Manager.inst.rabbit.putMsg(serverId, MsgType.CMD_MODERATION, chat);
    }

    public static void sendCommandModerationChange(Server server, Player player, boolean confirmation) {
        ActionPlayer chat = new ActionPlayer();
        chat.setConfirmation(confirmation);
        chat.setPlayerId(player.getPlayerId());
        chat.setUsername(player.getUsername());
        chat.setUuid(player.getUuid());
        Manager.inst.rabbit.putMsg(server.getId(), MsgType.CMD_MODERATION, chat);
        if (confirmation) {
            sendMessagePlayer(server, player, Lang.getDefLangMsg(LangType.GAME_MSG_MODER_ON));
        } else {
            sendMessagePlayer(server, player, Lang.getDefLangMsg(LangType.GAME_MSG_MODER_OFF));
        }
    }

    public static void sendMessageConfirmed(Server server, Player player, String msg, String cmd) {
        ActionPlayer chat = new ActionPlayer();
        chat.setMsg(msg);
        chat.setCmd(cmd);
        chat.setPlayerId(player.getPlayerId());
        chat.setUsername(player.getUsername());
        chat.setUuid(player.getUuid());
        Manager.inst.rabbit.putMsg(server.getId(), MsgType.CHAT_MSG_CONF, chat);
    }

    public static void sendMessagePlayer(Server server, Player player, ActionPlayer ap) {
        if (ap.getMsgs() == null || ap.getMsgs().isEmpty()) {
            return;
        }
        if (player != null) {
            ap.setPlayerId(player.getPlayerId());
            ap.setUsername(player.getUsername());
            ap.setUuid(player.getUuid());
        }
        if (server == null) {
            OnlineServer.getServersOnline().stream().forEach((serverId) -> {
                Manager.inst.rabbit.putMsg(serverId, MsgType.CHAT_MSG_PLAYER, ap);
            });
        } else {
            Manager.inst.rabbit.putMsg(server.getId(), MsgType.CHAT_MSG_PLAYER, ap);
        }
    }

    public static void sendMessagePlayer(Server server, Player player, String msg, String cmd) {
        ActionPlayer chat = new ActionPlayer();
        chat.setMsg(msg);
        chat.setCmd(cmd);
        if (player != null) {
            chat.setPlayerId(player.getPlayerId());
            chat.setUsername(player.getUsername());
            chat.setUuid(player.getUuid());
        }
        if (server == null) {
            OnlineServer.getServersOnline().stream().forEach((serverId) -> {
                Manager.inst.rabbit.putMsg(serverId, MsgType.CHAT_MSG_PLAYER, chat);
            });
        } else {
            Manager.inst.rabbit.putMsg(server.getId(), MsgType.CHAT_MSG_PLAYER, chat);
        }
    }

    public static void sendMessagePlayer(Server server, Player player, String msg) {
        sendMessagePlayer(server, player, msg, null);
    }

    public static Report createReport(Server server, Player creator, Player accused, ReportType type, Date time) {
        Report report = null;
        try {
            report = Manager.inst.db.createReport(server, creator, accused, type, Manager.inst.karma.getShiftReport(creator.getKarma()), time);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return report;
    }

    public static ReportResult createReportAndCompl(Report report, Server server, Player creator, String msg, Date time, int level) {
        ReportResult reportResult = null;
        try {
            reportResult = Manager.inst.db.createReportResult(report, server, creator, msg, time, level);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return reportResult;
    }

    public static String getPassedTime(Date datetime) {
        int min = (int) (((System.currentTimeMillis() - datetime.getTime()) / 1000) / 60);
        if (min <= 0) {
            return Integer.toString(min).concat(" ").concat(Lang.getDefLangMsg(LangType.WORD_TIME_NOW));
        }
        if (min >= 1440) {
            return Integer.toString((int) (min / 1440)).concat(" ").concat(Lang.getDefLangMsg(getLangType(min / 1440, LangType.WORD_TIME_DAY_1, LangType.WORD_TIME_DAY_2, LangType.WORD_TIME_DAY_3)));
        } else if (min >= 60) {
            return Integer.toString((int) (min / 60)).concat(" ").concat(Lang.getDefLangMsg(getLangType(min / 60, LangType.WORD_TIME_HOUR_1, LangType.WORD_TIME_HOUR_2, LangType.WORD_TIME_HOUR_3)));
        }
        return Integer.toString(min).concat(" ").concat(Lang.getDefLangMsg(getLangType(min, LangType.WORD_TIME_MIN_1, LangType.WORD_TIME_MIN_2, LangType.WORD_TIME_MIN_3)));
    }

    public static LangType getLangType(int num, LangType one, LangType two, LangType three) {
        num = num % 100;
        if (num > 19) {
            num = num % 10;
        }
        switch (num) {
            case 1: {
                return one;
            }
            case 2:
            case 3:
            case 4: {
                return two;
            }
        }
        return three;
    }

    public static String checkIn(String msg) {
        if (pHack.matcher(msg).find()) {
            Manager.inst.console.outRED("Олени пытаются просунуть инекцию: [".concat(msg).concat("]"));
            return msg.replaceAll("cmd:", "hack");
        }
        return msg;
    }

    public static String shieldingMarkdown(String msg) {
        //\(\_\*\{\[\#\+\-\.\!
        return msg.replace("_", "\\_").replace("*", "\\*").replace("{", "\\{").replace("[", "\\[").replace("(", "\\(").replace("#", "\\#").replace("+", "\\+").replace("-", "\\-").replace(".", "\\.").replace("!", "\\!");
    }

    public static String shieldingMinecraft(String msg) {
        return msg.replace("§", "");
    }

    public static boolean isCorrectMsg(String msg) {
        return !msg.matches(".*?[^0-9a-zA-Za-яА-Яё!`~@#№\\\\$%^&*(){}:;\"'/,. ?<>_+-=].*?");
    }

}
