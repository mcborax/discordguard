package net.bor.discordguard.core.discord;

// @author ArtBorax
import java.sql.SQLException;
import net.bor.discordguard.core.Manager;
import net.bor.discordguard.core.OnlineServer;
import net.bor.discordguard.dao.map.Player;
import net.bor.discordguard.lang.Lang;
import net.bor.discordguard.lang.LangType;

public class CommandList {

    public static void exrcute(DiscordMsgEvent dme) {
        StringBuilder sb = new StringBuilder();
        int count = 0;
        if (dme.getServerId() != 0) {
            for (int playerId : OnlineServer.getServerOnlinePlayers(dme.getServerId())) {
                try {
                    Player player = Manager.inst.db.getPlayer(playerId);
                    if (player != null) {
                        sb.append(", ").append(player.getUsername());
                        count++;
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        } else {
            for (int serverId : OnlineServer.getServersOnline()) {
                for (int playerId : OnlineServer.getServerOnlinePlayers(serverId)) {
                    try {
                        Player player = Manager.inst.db.getPlayer(playerId);
                        if (player != null) {
                            sb.append(", ").append(player.getUsername());
                            count++;
                        }
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        }
        if (sb.length() > 2) {
            dme.getDiscordBot().processChatMessage(dme.getServerId(), Lang.getDefLangMsg(LangType.DISCORD_MSG_COMMAND_LIST, Integer.toString(count), sb.toString().substring(2)), 30);
        } else {
            dme.getDiscordBot().processChatMessage(dme.getServerId(), Lang.getDefLangMsg(LangType.DISCORD_MSG_COMMAND_LIST, Integer.toString(count), ""), 10);
        }
    }

}
