package net.bor.discordguard.core.discord;

// @author ArtBorax
import java.util.Date;
import net.bor.discordguard.discordbot.DiscordBot;
import net.dv8tion.jda.core.entities.Message;

public class DiscordMsgEvent {

    private final int serverId;
    private final Message message;
    private final DiscordBot bot;
    private final Date time;

    public DiscordMsgEvent(DiscordBot bot, int serverId, Message message) {
        this.serverId = serverId;
        this.message = message;
        this.bot = bot;
        time=new Date(System.currentTimeMillis());
    }

    public DiscordBot getDiscordBot() {
        return bot;
    }

    public int getServerId() {
        return serverId;
    }

    public Message getMessage() {
        return message;
    }

    public Date getTime() {
        return time;
    }

}
