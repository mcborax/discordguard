package net.bor.discordguard.core.discord;

// @author ArtBorax
import java.sql.SQLException;
import net.bor.discordguard.core.Manager;
import net.bor.discordguard.dao.map.Player;
import net.bor.discordguard.lang.Lang;
import net.bor.discordguard.lang.LangType;

public class CommandSubscribe {

    public static String exrcute(Player player, boolean type) {
        if (type) {
            if (player.isDiscordSubscribe()) {
                return Lang.getDefLangMsg(LangType.DISCORD_MSG_COMMAND_SUBSCRIBE_ALREADY);
            } else {
                try {
                    player.setDiscordSubscribe(type);
                    Manager.inst.db.updatePlayer(player);
                    return Lang.getDefLangMsg(LangType.DISCORD_MSG_COMMAND_SUBSCRIBE);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                    return Lang.getDefLangMsg(LangType.DISCORD_MSG_ERROR_UNKNOWN);
                }
            }
        } else if (player.isDiscordSubscribe()) {
            try {
                player.setDiscordSubscribe(type);
                Manager.inst.db.updatePlayer(player);
                return Lang.getDefLangMsg(LangType.DISCORD_MSG_COMMAND_UNSUBSCRIBE);
            } catch (SQLException ex) {
                ex.printStackTrace();
                return Lang.getDefLangMsg(LangType.DISCORD_MSG_ERROR_UNKNOWN);
            }
        } else {
            return Lang.getDefLangMsg(LangType.DISCORD_MSG_COMMAND_UNSUBSCRIBE_ALREADY);
        }
    }

}
