package net.bor.discordguard.core.discord;

// @author ArtBorax
import net.bor.discordguard.core.Manager;
import net.bor.discordguard.dao.map.Player;
import net.bor.discordguard.lang.Lang;
import net.bor.discordguard.lang.LangType;

public class CommandShutdown {

    public static String exrcute(Player player) {
        if (player.getPlayerId() != Manager.inst.config.getAdminPlayerId()) {
            return Lang.getDefLangMsg(LangType.DISCORD_MSG_COMMAND_ACCESS_DENIED);
        } else {
            Manager.inst.shutdown();
            return Lang.getDefLangMsg(LangType.DISCORD_MSG_COMMAND_OK);
        }
    }

}
