package net.bor.discordguard.core.discord;

// @author ArtBorax
import net.bor.discordguard.lang.Lang;
import net.bor.discordguard.lang.LangType;

public class CommandHelpChat {

    public static void exrcute(DiscordMsgEvent dme) {
        dme.getDiscordBot().processChatMessage(dme.getServerId(), Lang.getDefLangMsg(LangType.DISCORD_MSG_COMMAND_HELP_LIST), 10);
        dme.getDiscordBot().processChatMessage(dme.getServerId(), Lang.getDefLangMsg(LangType.DISCORD_MSG_COMMAND_HELP_SHUTDOWN), 10);
        dme.getDiscordBot().processChatMessage(dme.getServerId(), Lang.getDefLangMsg(LangType.DISCORD_MSG_COMMAND_HELP_SUBSCRIBE), 10);
        dme.getDiscordBot().processChatMessage(dme.getServerId(), Lang.getDefLangMsg(LangType.DISCORD_MSG_COMMAND_HELP_UNSUBSCRIBE), 10);
    }

}
