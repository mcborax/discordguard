package net.bor.discordguard.core.discord;

// @author ArtBorax
import net.bor.discordguard.core.Manager;
import net.bor.discordguard.lang.Lang;
import net.bor.discordguard.lang.LangType;

public class CommandPrivateHelpChat {

    public static void exrcute(long discordUserId) {
        Manager.inst.discordBot.processPrivateMessage(discordUserId, Lang.getDefLangMsg(LangType.DISCORD_MSG_COMMAND_HELP_SHUTDOWN));
        Manager.inst.discordBot.processPrivateMessage(discordUserId, Lang.getDefLangMsg(LangType.DISCORD_MSG_COMMAND_HELP_SUBSCRIBE));
        Manager.inst.discordBot.processPrivateMessage(discordUserId, Lang.getDefLangMsg(LangType.DISCORD_MSG_COMMAND_HELP_UNSUBSCRIBE));
    }

}
