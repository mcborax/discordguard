package net.bor.discordguard.core.discord;

// @author ArtBorax
import java.sql.SQLException;
import net.bor.discordguard.core.Manager;
import net.bor.discordguard.core.ManagerUtils;
import net.bor.discordguard.core.OnlineServer;
import net.bor.discordguard.core.command.Utils;
import net.bor.discordguard.dao.map.Player;
import net.bor.discordguard.lang.Lang;
import net.bor.discordguard.lang.LangType;
import net.bor.discordguard.map.ActionPlayer;
import net.bor.discordguard.rabbit.MsgType;
import net.dv8tion.jda.core.entities.PrivateChannel;

public class DiscordUtils {

    public static void processDiscordMsg(DiscordMsgEvent dme) {
        if (!dme.getMessage().getContentStripped().isEmpty() && ManagerUtils.isCorrectMsg(dme.getMessage().getContentStripped())) {
            if (OnlineServer.isOnlineServer(dme.getServerId()) || dme.getServerId() == 0) {
                Player player = getPlayer(dme.getMessage().getAuthor().getIdLong());
                if (player != null) {
                    if (dme.getMessage().getContentStripped().startsWith(Manager.inst.config.getDiscordCommandPrefix())) {
                        dme.getMessage().delete().complete();
                        processDiscordCmd(player, dme, dme.getMessage().getContentStripped().substring(1).split(" "));
                    } else if ((dme.getServerId() > 0 && Manager.inst.karma.isDiscordSendMsg(player.getKarma(), player.getKarmaSpeed()))
                            || (dme.getServerId() == 0 && Manager.inst.karma.isDiscordSendMsgBroadcast(player.getKarma(), player.getKarmaSpeed()))) {
                        String msg = ManagerUtils.shieldingMinecraft(dme.getMessage().getContentStripped());
                        if (msg.length() > 181) {
                            dme.getMessage().delete().complete();
                            dme.getDiscordBot().processChatMessage(dme.getServerId(), Lang.getDefLangMsg(LangType.DISCORD_MSG_VERYLONG), 10);
                        } else if (!msg.isEmpty()) {
                            //проверить на ценз и флуд и записать в базу
                            try {
                                String resultMsg = ManagerUtils.checkMsgDiscordAndAddDB(dme.getServerId(), player, msg, dme.getTime());
                                if (resultMsg == null) {
                                    sendMsg(dme.getServerId(), player.getUsername(), msg);
                                } else {
                                    dme.getMessage().delete().complete();
                                    dme.getDiscordBot().processChatMessage(dme.getServerId(), resultMsg, 20);
                                }
                            } catch (SQLException ex) {
                                ex.printStackTrace();
                                dme.getDiscordBot().processChatMessage(dme.getServerId(), Lang.getDefLangMsg(LangType.DISCORD_MSG_ERROR_SERVER), 20);
                            }
                        } else {
                            dme.getMessage().delete().complete();
                        }
                    } else {
                        dme.getMessage().delete().complete();
                        Manager.inst.discordBot.processPrivateMessage(dme.getMessage().getAuthor().getIdLong(), Lang.getDefLangMsg(LangType.DISCORD_MSG_LOW_KARMA));
                    }
                } else {
                    dme.getMessage().delete().complete();
                    PrivateChannel channel = null;
                    try {
                        channel = dme.getMessage().getAuthor().openPrivateChannel().complete();
                        channel.sendMessage(Lang.getDefLangMsg(LangType.DISCORD_MSG_NEED_LINKED)).nonce("").complete();
                        channel.sendMessage(Lang.getDefLangMsg(LangType.DISCORD_MSG_NEED_LINKED_CODE, Utils.getCodeLink(dme.getMessage().getAuthor().getIdLong()))).nonce("").complete();
                        channel.sendMessage(Lang.getDefLangMsg(LangType.DISCORD_MSG_NEED_LINKED_HELP)).nonce("").complete();
                    } catch (Exception ex) {
                        dme.getDiscordBot().processChatMessage(dme.getServerId(), Lang.getDefLangMsg(LangType.DISCORD_MSG_ERROR_RESPONSE), 60);
                    } finally {
                        try {
                            if (channel != null) {
                                channel.close();
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                }
                return;
            } else {
                dme.getDiscordBot().processChatMessage(dme.getServerId(), Lang.getDefLangMsg(LangType.DISCORD_MSG_SYSTEM_SERVER_OFF), 10);
            }
        } else {
            dme.getDiscordBot().processChatMessage(dme.getServerId(), Lang.getDefLangMsg(LangType.DISCORD_MSG_UNCORRECT), 10);
        }
        dme.getMessage().delete().complete();

    }

    private static void sendMsg(int serverId, String username, String msg) {
        if (serverId != 0) {
            ActionPlayer ap = new ActionPlayer();
            ap.setMsg(Lang.getDefLangMsg(LangType.DISCORD_MSG_GAME, username, msg));
            Manager.inst.rabbit.putMsg(serverId, MsgType.CHAT_MSG_BROADCAST, ap);
        } else {
            OnlineServer.getServersOnline().stream().filter((server) -> (server != 0)).forEach((server) -> {
                sendMsg(server, username, msg);
            });
        }
    }

    private static Player getPlayer(long discordUserID) {
        try {
            return Manager.inst.db.getPlayerDiscordUserID(discordUserID);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    private static void processDiscordCmd(Player player, DiscordMsgEvent dme, String[] args) {
        if (args[0].equalsIgnoreCase("help")) {
            CommandHelpChat.exrcute(dme);
        } else if (args[0].equalsIgnoreCase("list") || args[0].equalsIgnoreCase("ls") || args[0].equalsIgnoreCase("who")) {
            CommandList.exrcute(dme);
        } else if (args[0].equalsIgnoreCase("shutdown")) {
            dme.getDiscordBot().processChatMessage(dme.getServerId(), CommandShutdown.exrcute(player), 2);
        } else if (args[0].equalsIgnoreCase("subscribe")) {
            dme.getDiscordBot().processChatMessage(dme.getServerId(), CommandSubscribe.exrcute(player, true), 10);
        } else if (args[0].equalsIgnoreCase("unsubscribe")) {
            dme.getDiscordBot().processChatMessage(dme.getServerId(), CommandSubscribe.exrcute(player, false), 10);
        } else {
            dme.getDiscordBot().processChatMessage(dme.getServerId(), Lang.getDefLangMsg(LangType.DISCORD_MSG_COMMAND_NOTFOUND), 10);
        }
    }

    public static void processDiscordMsg(DiscordPrivateMsgEvent dpme) {
        long discordUserId = dpme.getMessage().getAuthor().getIdLong();
        Player player = getPlayer(discordUserId);
        if (player == null) {
            PrivateChannel channel = dpme.getMessage().getPrivateChannel();
            try {
                channel.sendMessage(Lang.getDefLangMsg(LangType.DISCORD_MSG_NEED_LINKED)).nonce("").complete();
                channel.sendMessage(Lang.getDefLangMsg(LangType.DISCORD_MSG_NEED_LINKED_CODE, Utils.getCodeLink(discordUserId))).nonce("").complete();
                channel.sendMessage(Lang.getDefLangMsg(LangType.DISCORD_MSG_NEED_LINKED_HELP)).nonce("").complete();
            } catch (Exception ex) {
            } finally {
                try {
                    if (channel != null) {
                        channel.close();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        } else if (dpme.getMessage().getContentStripped().startsWith(Manager.inst.config.getDiscordCommandPrefix())) {
            processDiscordCmd(player, dpme, dpme.getMessage().getContentStripped().substring(1).split(" "));
        }
        System.out.println(dpme.getMessage().getAuthor().getName());
        System.out.println(dpme.getMessage().getContentStripped());
    }

    private static void processDiscordCmd(Player player, DiscordPrivateMsgEvent dpme, String[] args) {
        if (args[0].equalsIgnoreCase("help")) {
            CommandPrivateHelpChat.exrcute(dpme.getMessage().getAuthor().getIdLong());
        } else if (args[0].equalsIgnoreCase("shutdown")) {
            dpme.getDiscordBot().processPrivateMessage(dpme.getMessage().getAuthor().getIdLong(), CommandShutdown.exrcute(player));
        } else if (args[0].equalsIgnoreCase("subscribe")) {
            dpme.getDiscordBot().processPrivateMessage(dpme.getMessage().getAuthor().getIdLong(), CommandSubscribe.exrcute(player, true));
        } else if (args[0].equalsIgnoreCase("unsubscribe")) {
            dpme.getDiscordBot().processPrivateMessage(dpme.getMessage().getAuthor().getIdLong(), CommandSubscribe.exrcute(player, false));
        } else {
            dpme.getDiscordBot().processPrivateMessage(dpme.getMessage().getAuthor().getIdLong(), Lang.getDefLangMsg(LangType.DISCORD_MSG_COMMAND_NOTFOUND));
        }
    }
}
