package net.bor.discordguard.core.discord;

// @author ArtBorax
import java.util.Date;
import net.bor.discordguard.discordbot.DiscordBot;
import net.dv8tion.jda.core.entities.Message;

public class DiscordPrivateMsgEvent {

    private final Message message;
    private final DiscordBot bot;
    private final Date time;

    public DiscordPrivateMsgEvent(DiscordBot bot, Message message) {
        this.message = message;
        this.bot = bot;
        time=new Date(System.currentTimeMillis());
    }

    public DiscordBot getDiscordBot() {
        return bot;
    }

    public Message getMessage() {
        return message;
    }

    public Date getTime() {
        return time;
    }

}
