package net.bor.discordguard.core.command;

// @author ArtBorax
import java.sql.SQLException;
import java.util.Date;
import net.bor.discordguard.core.Manager;
import net.bor.discordguard.core.ManagerUtils;
import net.bor.discordguard.dao.map.Player;
import net.bor.discordguard.dao.map.Report;
import net.bor.discordguard.dao.map.Server;
import net.bor.discordguard.lang.Lang;
import net.bor.discordguard.lang.LangType;
import net.bor.discordguard.map.ActionPlayer;
import net.bor.discordguard.map.ReportMessage;
import net.bor.discordguard.rabbit.MsgType;

public class CommandAddReport {

    public static void execute(ReportMessage rm) {
        Server server = null;
        Player player = null;
        Report report = null;
        try {
            server = Manager.inst.db.getServer(rm.getServerId());
            player = Manager.inst.db.getPlayer(rm.getPlayerId());
            report = Manager.inst.db.getReport(rm.getReportId());
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        if (server == null || player == null) {
            return;
        }
        if (report == null) {
            ManagerUtils.sendMessagePlayer(server, player, Lang.getDefLangMsg(LangType.GAME_MSG_REPORT_NOTFOUND, Integer.toString(rm.getReportId())));
            return;
        }
        if (report.getTime().before(new Date(System.currentTimeMillis() - (24 * 60 * 60 * 1000)))) {
            ManagerUtils.sendMessagePlayer(server, player, Lang.getDefLangMsg(LangType.GAME_MSG_REPORTMSGS_ADD_EXPIRATION_END));
            return;
        }

        //проверить поля описания
        if (rm.getMsgs() == null || rm.getMsgs().isEmpty()) {
            Utils.sendMenuDescriptionReport(server, player, rm, -1, MsgType.REPORT_ADD);
            return;
        }
        StringBuilder sbMsgs = new StringBuilder();
        int length = 0;
        for (String msg : rm.getMsgs()) {
            length = length + msg.trim().length();
            sbMsgs.append(msg);
            sbMsgs.append(" ");
        }
        if (length < 10 || MsgType.getType(rm.getTypeMsg()) == MsgType.REPORT_ADD) {
            Utils.sendMenuDescriptionReport(server, player, rm, length, MsgType.REPORT_ADD);
            return;
        }
        //окончательная проверка всех данных пройдена, создаетм репорт
        if (MsgType.getType(rm.getTypeMsg()) != MsgType.REPORT_ADD_COMPL || rm.getTime() == null) {
            ManagerUtils.sendMessagePlayer(server, player, Lang.getDefLangMsg(LangType.GAME_MSG_ERROR_UNKNOWN));
            return;
        }
        ActionPlayer ap = new ActionPlayer();
        try {
            Manager.inst.db.createReportMsg(server, player, report, sbMsgs.toString().trim(), rm.getTime());
            //отправить информацию о модификации репорта
            if (report.getAccused().getPlayerId() == player.getPlayerId()) {
                ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_REPORT_CHANGE_APPEAL, Integer.toString(report.getId()), player.getUsername()));
                //**{0}** апеллирует репорт __{1}__ , описание: {2}.
                Manager.inst.discordBot.processLogMessage(Lang.getDefLangMsg(LangType.DISCORD_MSG_REPORT_APPEAL,
                        ManagerUtils.shieldingMarkdown(player.getUsername()),
                        Integer.toString(report.getId()),
                        ManagerUtils.shieldingMarkdown(sbMsgs.toString().trim())), 0);

            } else {
                ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_REPORT_CHANGE_ADD, Integer.toString(report.getId()), player.getUsername()));
                //**{0}** добавил в репорт __{1}__ , описание: {2}.
                Manager.inst.discordBot.processLogMessage(Lang.getDefLangMsg(LangType.DISCORD_MSG_REPORT_ADD, 
                        ManagerUtils.shieldingMarkdown(player.getUsername()), 
                        Integer.toString(report.getId()), 
                        ManagerUtils.shieldingMarkdown(sbMsgs.toString().trim())), 0);

            }
            ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_REPORTINFO, Integer.toString(report.getId())), Lang.getDefLangMsg(LangType.GAME_MSG_REPORTINFO_CMD, Integer.toString(report.getId())));
            ManagerUtils.sendMessagePlayer(null, null, ap);
            return;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        ManagerUtils.sendMessagePlayer(server, player, Lang.getDefLangMsg(LangType.GAME_MSG_ERROR_UNKNOWN));
    }

}
