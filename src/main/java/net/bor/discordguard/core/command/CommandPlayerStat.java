package net.bor.discordguard.core.command;

// @author ArtBorax
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import net.bor.discordguard.core.Manager;
import net.bor.discordguard.core.ManagerUtils;
import net.bor.discordguard.core.OnlineServer;
import net.bor.discordguard.dao.map.Player;
import net.bor.discordguard.dao.map.PlayerInfo;
import net.bor.discordguard.dao.map.Server;
import net.bor.discordguard.lang.Lang;
import net.bor.discordguard.lang.LangType;
import net.bor.discordguard.map.ActionPlayer;

public class CommandPlayerStat {

    private static SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static void execute(ActionPlayer msg) {
        Server server = null;
        Player player = null;
        Player target = null;
        try {
            player = Manager.inst.db.getPlayer(msg.getPlayerId());
            server = Manager.inst.db.getServer(msg.getServerId());
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        if (player == null || server == null) {
            return;
        }
        if (msg.getUsername() != null && !msg.getUsername().isEmpty()) {
            try {
                target = Manager.inst.db.getPlayer(msg.getUsername());
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            if (target == null) {
                ManagerUtils.sendMessagePlayer(server, player, Lang.getDefLangMsg(LangType.GAME_MSG_NOTFOUND_PLAYER, msg.getUsername()));
                return;
            }
        }
        if (target == null) {
            target = player;
        }

        try {
            ActionPlayer ap = new ActionPlayer();
            ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_HEAD_PLAYERSTAT, target.getUsername()));
            ap.addMsgs(
                    Lang.getDefLangMsg(LangType.GAME_MSG_MENU_BODY_PLAYERSTAT_KARMA, Integer.toString((int) (target.getKarma() + target.getKarmaSpeed())), Integer.toString(target.getCreateReport()), Integer.toString(target.getConsReport()), Integer.toString(target.getAccusReport())),
                    Lang.getDefLangMsg(LangType.GAME_MSG_MENU_BODY_PLAYERSTAT_KARMA_CMD, Integer.toString((int) (target.getKarma() + target.getKarmaSpeed())), Integer.toString(target.getCreateReport()), Integer.toString(target.getConsReport()), Integer.toString(target.getAccusReport())));
            for (PlayerInfo pi : Manager.inst.db.getPlayerInfos(target)) {
                int min = (int) (pi.getTimeOnline() / 60000);
                int hour = min / 60;
                int day = hour / 24;
                min = min % 60;
                hour = hour % 60;
                if (OnlineServer.isOnlinePlayer(pi.getServer().getId(), target.getPlayerId())) {
                    ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_BODY_PLAYERSTAT_ONLINE, pi.getServer().getName(),
                            Integer.toString(day), Lang.getDefLangMsg(ManagerUtils.getLangType(day, LangType.WORD_TIME_DAY, LangType.WORD_TIME_DAY_2, LangType.WORD_TIME_DAY_3)),
                            Integer.toString(hour), Lang.getDefLangMsg(ManagerUtils.getLangType(hour, LangType.WORD_TIME_HOUR, LangType.WORD_TIME_HOUR_2, LangType.WORD_TIME_HOUR_3)),
                            Integer.toString(min), Lang.getDefLangMsg(ManagerUtils.getLangType(min, LangType.WORD_TIME_MIN, LangType.WORD_TIME_MIN_2, LangType.WORD_TIME_MIN_3))));
                    continue;
                }
                if (pi.getTimeQuit() == null || pi.getTimeQuit().getTime() < 1000000000) {
                    continue;
                }
                ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_BODY_PLAYERSTAT_OFFLINE, pi.getServer().getName(),
                        Integer.toString(day), Lang.getDefLangMsg(ManagerUtils.getLangType(day, LangType.WORD_TIME_DAY, LangType.WORD_TIME_DAY_2, LangType.WORD_TIME_DAY_3)),
                        Integer.toString(hour), Lang.getDefLangMsg(ManagerUtils.getLangType(hour, LangType.WORD_TIME_HOUR, LangType.WORD_TIME_HOUR_2, LangType.WORD_TIME_HOUR_3)),
                        Integer.toString(min), Lang.getDefLangMsg(ManagerUtils.getLangType(min, LangType.WORD_TIME_MIN, LangType.WORD_TIME_MIN_2, LangType.WORD_TIME_MIN_3)),
                        date.format(pi.getTimeQuit())
                ));
            }
            ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_PLAYER_LOGCHAT, target.getUsername()), Lang.getDefLangMsg(LangType.GAME_MSG_PLAYER_LOGCHAT_CMD, target.getUsername()));
            if (target.getAccusReport() > 0) {
                ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_REPORS_PLAYER, target.getUsername()), Lang.getDefLangMsg(LangType.GAME_MSG_MENU_REPORS_PLAYER_CMD, target.getUsername()));
            }
            ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_SPLITTER));
            ManagerUtils.sendMessagePlayer(server, player, ap);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

}
