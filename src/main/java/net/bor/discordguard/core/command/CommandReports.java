package net.bor.discordguard.core.command;

// @author ArtBorax
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import net.bor.discordguard.Settings;
import net.bor.discordguard.core.Manager;
import net.bor.discordguard.core.ManagerUtils;
import net.bor.discordguard.dao.map.Player;
import net.bor.discordguard.dao.map.Report;
import net.bor.discordguard.dao.map.ReportResult;
import net.bor.discordguard.dao.map.Server;
import net.bor.discordguard.lang.Lang;
import net.bor.discordguard.lang.LangType;
import net.bor.discordguard.map.ActionPlayer;

public class CommandReports {

    private static SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    private static SimpleDateFormat time = new SimpleDateFormat("HH:mm:ss");

    public static void execute(ActionPlayer msg) {
        Player target = null;
        Player player = null;
        try {
            player = Manager.inst.db.getPlayer(msg.getPlayerId());
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        if (player == null) {
            return;
        }
        Server server = null;
        try {
            server = Manager.inst.db.getServer(msg.getServerId());
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        if (server == null) {
            return;
        }

        int page = 1;
        if (msg.getMsg() != null && !msg.getMsg().isEmpty()) {
            try {
                page = Integer.parseInt(msg.getMsg());
            } catch (Exception ex) {
            }
        }
        if (page < 1) {
            page = 1;
        }
        try {
            ActionPlayer ap = new ActionPlayer();
            if (msg.getUsername() != null && !msg.getUsername().isEmpty()) {
                try {
                    target = Manager.inst.db.getPlayer(msg.getUsername());
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                if (target == null) {
                    ManagerUtils.sendMessagePlayer(server, player, Lang.getDefLangMsg(LangType.GAME_MSG_NOTFOUND_PLAYER, msg.getUsername()));
                    return;
                }

                List<Report> reports = Manager.inst.db.getReports(target, (page - 1) * Settings.CHAT_PAGE_SIZE, Settings.CHAT_PAGE_SIZE + 1);
                ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_HEAD_REPORTS, target.getUsername()));
                if (reports != null && reports.size() > 0) {
                    for (int i = reports.size() - 1; i >= 0; i--) {
                        Report report = reports.get(i);
                        if (i >= Settings.CHAT_PAGE_SIZE) {
                            continue;
                        }
                        ReportResult lastResult = report.getLastResult();
                        String txt = null;
                        if (lastResult == null) {
                            // #1, не решен, 2018-03-16 23:08:47, Нарушение в чате
                            txt = Lang.getDefLangMsg(LangType.GAME_MSG_MENU_BODY_REPORTS_OPEN, Integer.toString(report.getId()), date.format(report.getTime()), report.getType().getShortDesription());
                        } else if (lastResult.getLevel() > 0) {
                            // #1, оштрафован, 2018-03-16 23:08:47, Нарушение в чате
                            txt = Lang.getDefLangMsg(LangType.GAME_MSG_MENU_BODY_REPORTS_NEG, Integer.toString(report.getId()), date.format(report.getTime()), report.getType().getShortDesription());
                        } else {
                            // #1, отклонен, 2018-03-16 23:08:47, нет доказательств
                            txt = Lang.getDefLangMsg(LangType.GAME_MSG_MENU_BODY_REPORTS_POS, Integer.toString(report.getId()), date.format(report.getTime()), lastResult.getMsg());
                        }
                        //(§f<<<назад)[(cmd:report list {0})] §e| (§fвперед>>>)[(cmd:report list {1})]
                        StringBuilder sb = new StringBuilder();
                        sb.append("(").append(ManagerUtils.checkIn(txt)).append(")[(hvr:");
                        //сервер: x1, создал: ArtBorax, минимальная карма для рассмотра: 10
                        sb.append(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_BODY_REPORTS_HVR, report.getServer().getName(), report.getCreator().getUsername(), Integer.toString(report.getCostKarma())));
                        if (lastResult != null) {
                            for (ReportResult result : report.getResults()) {
                                sb.append("\n");
                                //2018-03-16 23:08, уровень нарушения 3, рассмотрел: ArtBorax, Нецензурная лексика: мат.
                                sb.append(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_BODY_REPORTS_HVR_RESULT, date.format(result.getTime()), result.getPlayer().getUsername(), Integer.toString(result.getLevel()), ManagerUtils.checkIn(result.getMsg())));
                            }
                        }
                        sb.append(")(cmd:report info ").append(report.getId()).append(")]");
                        ap.addMsgs(txt, sb.toString());

                    }
                    if (page > 1 && reports.size() > Settings.CHAT_PAGE_SIZE) {
                        ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_FOOT_REPORTS, Integer.toString(page + 1), target.getUsername().concat(" ")), Lang.getDefLangMsg(LangType.GAME_MSG_MENU_FOOT_REPORTS_CMD, Integer.toString(page - 1), Integer.toString(page + 1), target.getUsername().concat(" ")));
                    } else if (page > 1 && reports.size() <= Settings.CHAT_PAGE_SIZE) {
                        ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_SPLITTER), Lang.getDefLangMsg(LangType.GAME_MSG_MENU_FOOT_REPORTS_FORWARD_CMD, Integer.toString(page - 1), target.getUsername().concat(" ")));
                    } else if (page == 1 && reports.size() <= Settings.CHAT_PAGE_SIZE) {
                        ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_SPLITTER));
                    } else if (page == 1 && reports.size() > Settings.CHAT_PAGE_SIZE) {
                        ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_FOOT_REPORTS, Integer.toString(page + 1), target.getUsername().concat(" ")), Lang.getDefLangMsg(LangType.GAME_MSG_MENU_FOOT_REPORTS_BACK_CMD, Integer.toString(page + 1), target.getUsername().concat(" ")));
                    }
                } else {
                    ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_EMPTY));
                    ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_SPLITTER));
                }

            } else {
                List<Report> reports = Manager.inst.db.getReports(player.getKarma(), (page - 1) * Settings.CHAT_PAGE_SIZE, Settings.CHAT_PAGE_SIZE + 1);
                ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_HEAD_REPORTSCONS));
                if (reports != null && reports.size() > 0) {
                    for (int i = reports.size() - 1; i >= 0; i--) {
                        Report report = reports.get(i);
                        if (i >= Settings.CHAT_PAGE_SIZE) {
                            continue;
                        }
                        ReportResult lastResult = report.getLastResult();
                        String txt = null;
                        if (lastResult == null) {
                            // #1, test, не решен, 2 минуты, Нарушение в чате
                            txt = Lang.getDefLangMsg(LangType.GAME_MSG_MENU_BODY_REPORTSCONS_OPEN, Integer.toString(report.getId()), report.getAccused().getUsername(), ManagerUtils.getPassedTime(report.getTime()), report.getType().getShortDesription());
                        } else if (lastResult.getLevel() > 0) {
                            // #1, test, оштрафован, 2 минуты, Нарушение в чате
                            txt = Lang.getDefLangMsg(LangType.GAME_MSG_MENU_BODY_REPORTSCONS_NEG, Integer.toString(report.getId()), report.getAccused().getUsername(), ManagerUtils.getPassedTime(report.getTime()), report.getType().getShortDesription());
                        } else {
                            // #1, test, отклонен, 2 минуты, нет доказательств
                            txt = Lang.getDefLangMsg(LangType.GAME_MSG_MENU_BODY_REPORTSCONS_POS, Integer.toString(report.getId()), report.getAccused().getUsername(), ManagerUtils.getPassedTime(report.getTime()), lastResult.getMsg());
                        }
                        //(§f<<<назад)[(cmd:report list {0})] §e| (§fвперед>>>)[(cmd:report list {1})]
                        StringBuilder sb = new StringBuilder();
                        sb.append("(").append(ManagerUtils.checkIn(txt)).append(")[(hvr:");
                        //сервер: x1, подал: ArtBorax, в: 23:08:01
                        sb.append(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_BODY_REPORTSCONS_HVR, report.getServer().getName(), report.getCreator().getUsername(), time.format(report.getTime())));
                        if (lastResult != null) {
                            for (ReportResult result : report.getResults()) {
                                sb.append("\n");
                                //23:08:01, уровень нарушения 3, рассмотрел: ArtBorax, Нецензурная лексика: мат.
                                sb.append(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_BODY_REPORTS_HVR_RESULT, time.format(result.getTime()), result.getPlayer().getUsername(), Integer.toString(result.getLevel()), ManagerUtils.checkIn(result.getMsg())));
                            }
                        }
                        sb.append(")(cmd:report info ").append(report.getId()).append(")]");
                        ap.addMsgs(txt, sb.toString());
                    }
                    if (page > 1 && reports.size() > Settings.CHAT_PAGE_SIZE) {
                        ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_FOOT_REPORTS, Integer.toString(page + 1)), Lang.getDefLangMsg(LangType.GAME_MSG_MENU_FOOT_REPORTS_CMD, Integer.toString(page - 1), Integer.toString(page + 1)));
                    } else if (page > 1 && reports.size() <= Settings.CHAT_PAGE_SIZE) {
                        ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_SPLITTER), Lang.getDefLangMsg(LangType.GAME_MSG_MENU_FOOT_REPORTS_FORWARD_CMD, Integer.toString(page - 1)));
                    } else if (page == 1 && reports.size() <= Settings.CHAT_PAGE_SIZE) {
                        ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_SPLITTER));
                    } else if (page == 1 && reports.size() > Settings.CHAT_PAGE_SIZE) {
                        ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_FOOT_REPORTS, Integer.toString(page + 1)), Lang.getDefLangMsg(LangType.GAME_MSG_MENU_FOOT_REPORTS_BACK_CMD, Integer.toString(page + 1)));
                    }
                } else {
                    ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_BODY_REPORTSCONS_EMPTY));
                    ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_SPLITTER));
                }
            }
            ManagerUtils.sendMessagePlayer(server, player, ap);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

}
