package net.bor.discordguard.core.command;

// @author ArtBorax
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;
import net.bor.discordguard.core.Manager;
import net.bor.discordguard.core.ManagerUtils;
import net.bor.discordguard.dao.map.Player;
import net.bor.discordguard.dao.map.Report;
import net.bor.discordguard.dao.map.ReportMsg;
import net.bor.discordguard.dao.map.ReportResult;
import net.bor.discordguard.dao.map.Server;
import net.bor.discordguard.lang.Lang;
import net.bor.discordguard.lang.LangType;
import net.bor.discordguard.map.ActionPlayer;
import net.bor.discordguard.map.ReportMessage;

public class CommandReportInfo {

    private static SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static void execute(ReportMessage rm) {
        Server server = null;
        Player player = null;
        Report report = null;
        try {
            server = Manager.inst.db.getServer(rm.getServerId());
            player = Manager.inst.db.getPlayer(rm.getPlayerId());
            report = Manager.inst.db.getReport(rm.getReportId());
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        if (server == null || player == null) {
            return;
        }
        if (report == null) {
            ManagerUtils.sendMessagePlayer(server, player, Lang.getDefLangMsg(LangType.GAME_MSG_REPORT_NOTFOUND, Integer.toString(rm.getReportId())));
            return;
        }
        ActionPlayer ap = new ActionPlayer();
        ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_HEAD_REPORTINFO, Integer.toString(rm.getReportId()), report.getAccused().getUsername()));
        //Подал 2018-03-16 23:08:47 ArtBorax сервер x1
        ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_BODY_REPORTINFO1, date.format(report.getTime()), report.getCreator().getUsername(), report.getServer().getName()));
        //Тип: Нарушение в чате штраф -150 кармы
        ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_BODY_REPORTINFO2, report.getType().getShortDesription()));

        Set<ReportResult> reportResults = report.getResults();
        if (reportResults == null || reportResults.isEmpty()) {
            ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_BODY_REPORTINFO_NORESULT));
        } else {
            for (ReportResult reportResult : reportResults) {
                ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_BODY_REPORTINFO_RESULTSPLIT, reportResult.getPlayer().getUsername()));
                //x1 2018-03-16 23:08:47 cтепень 1 
                ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_BODY_REPORTINFO_RESULT1, reportResult.getServer().getName(), date.format(reportResult.getTime()), Integer.toString(reportResult.getLevel())));
                if (reportResult.getMsg() != null && !reportResult.getMsg().isEmpty()) {
                    //Нецензурная лексика: мат
                    ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_BODY_REPORTINFO_RESULT2, reportResult.getMsg()));
                }
            }
            ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_SPLITTER));
        }
        Set<ReportMsg> reportMsgs = report.getMsgs();
        for (ReportMsg reportMsg : reportMsgs) {
            if (reportMsg.getPlayer().getPlayerId() != report.getAccused().getPlayerId()) {
                //x1 ArtBorax в 2018-03-16 23:08:47 дополнил: многострочный текст
                ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_BODY_REPORTINFO_MSG_ADD, reportMsg.getServer().getName(), reportMsg.getPlayer().getUsername(), date.format(reportMsg.getTime()), reportMsg.getMsg()));
            } else {
                //x1 ArtBorax в 2018-03-16 23:08:47 апеллировал: многострочный текст
                ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_BODY_REPORTINFO_MSG_APPEAL, reportMsg.getServer().getName(), reportMsg.getPlayer().getUsername(), date.format(reportMsg.getTime()), reportMsg.getMsg()));
            }
        }

        if (report.getTime().after(new Date(System.currentTimeMillis() - (24 * 60 * 60 * 1000)))) {
            if (report.getAccused().getPlayerId() == rm.getPlayerId()) {
                ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_REPORT_APPEAL, Integer.toString(rm.getReportId())), Lang.getDefLangMsg(LangType.GAME_MSG_REPORT_APPEAL_CMD, Integer.toString(rm.getReportId())));
            } else {
                ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_REPORT_ADD, Integer.toString(rm.getReportId())), Lang.getDefLangMsg(LangType.GAME_MSG_REPORT_ADD_CMD, Integer.toString(rm.getReportId())));
                if (player.getKarma() > report.getCostKarma() && player.getKarma() + player.getKarmaSpeed() > report.getCostKarma() && rm.getPlayerId() != report.getCreator().getPlayerId()) {
                    ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_REPORT_CONS, Integer.toString(rm.getReportId())), Lang.getDefLangMsg(LangType.GAME_MSG_REPORT_CONS_CMD, Integer.toString(rm.getReportId())));
                }
            }
        }
        ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_PLAYER_LOGCHAT, report.getAccused().getUsername()), Lang.getDefLangMsg(LangType.GAME_MSG_PLAYER_LOGCHAT_CMD, report.getAccused().getUsername()));
        ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_PLAYERSTAT, report.getAccused().getUsername()), Lang.getDefLangMsg(LangType.GAME_MSG_MENU_PLAYERSTAT_CMD, report.getAccused().getUsername()));
        ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_SPLITTER));
        ManagerUtils.sendMessagePlayer(server, player, ap);
    }

}
