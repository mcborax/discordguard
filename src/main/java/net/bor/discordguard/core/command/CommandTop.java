package net.bor.discordguard.core.command;

// @author ArtBorax
import java.sql.SQLException;
import java.util.List;
import net.bor.discordguard.core.Manager;
import net.bor.discordguard.core.ManagerUtils;
import net.bor.discordguard.dao.map.Player;
import net.bor.discordguard.dao.map.Server;
import net.bor.discordguard.lang.Lang;
import net.bor.discordguard.lang.LangType;
import net.bor.discordguard.map.ActionPlayer;

public class CommandTop {

    public static void execute(int serverId, int playerId) {
        Player player = null;
        try {
            player = Manager.inst.db.getPlayer(playerId);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        if (player == null) {
            return;
        }
        Server server = null;
        try {
            server = Manager.inst.db.getServer(serverId);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        if (server == null) {
            return;
        }
        try {
            List<Player> players = Manager.inst.db.getTopPlayers();
            ActionPlayer ap = new ActionPlayer();
            ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_HEAD_TOP));
            for (Player pl : players) {
                ap.addMsgs(
                        Lang.getDefLangMsg(LangType.GAME_MSG_MENU_BODY_TOP, pl.getUsername(), Integer.toString((int) (pl.getKarma() + pl.getKarmaSpeed())), Integer.toString(pl.getCreateReport()), Integer.toString(pl.getConsReport()), Integer.toString(pl.getAccusReport())),
                        Lang.getDefLangMsg(LangType.GAME_MSG_MENU_BODY_TOP_CMD, pl.getUsername(), Integer.toString((int) (pl.getKarma() + pl.getKarmaSpeed())), Integer.toString(pl.getCreateReport()), Integer.toString(pl.getConsReport()), Integer.toString(pl.getAccusReport())));
            }
            ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_SPLITTER));
            ManagerUtils.sendMessagePlayer(server, player, ap);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
