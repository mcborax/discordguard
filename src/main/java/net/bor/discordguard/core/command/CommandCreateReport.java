package net.bor.discordguard.core.command;

// @author ArtBorax
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Set;
import net.bor.discordguard.core.Manager;
import net.bor.discordguard.core.ManagerUtils;
import net.bor.discordguard.core.OnlineServer;
import net.bor.discordguard.dao.map.Player;
import net.bor.discordguard.dao.map.Report;
import net.bor.discordguard.dao.map.ReportResult;
import net.bor.discordguard.dao.map.ReportType;
import net.bor.discordguard.dao.map.Server;
import net.bor.discordguard.lang.Lang;
import net.bor.discordguard.lang.LangType;
import net.bor.discordguard.map.ActionPlayer;
import net.bor.discordguard.map.ReportMessage;
import net.bor.discordguard.rabbit.MsgType;

public class CommandCreateReport {

    public static void execute(ReportMessage rm) {
        Player target = null;
        Player player = null;
        try {
            player = Manager.inst.db.getPlayer(rm.getPlayerId());
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        if (player == null) {
            return;
        }
        Server server = null;
        try {
            server = Manager.inst.db.getServer(rm.getServerId());
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        if (server == null) {
            return;
        }
        if (rm.getAccusedUsername() != null && !rm.getAccusedUsername().isEmpty()) {
            try {
                target = Manager.inst.db.getPlayer(rm.getAccusedUsername());
            } catch (SQLException ex) {
                ManagerUtils.sendMessagePlayer(server, player, Lang.getDefLangMsg(LangType.GAME_MSG_ERROR_UNKNOWN));
                ex.printStackTrace();
            }
        }
        if (target == null) {
            ManagerUtils.sendMessagePlayer(server, player, Lang.getDefLangMsg(LangType.GAME_MSG_NOTFOUND_PLAYER, rm.getAccusedUsername()));
            return;
        }
        rm.setAccusedUsername(target.getUsername());
        //проверить подачу на себя самого
        if (rm.getPlayerId() == target.getPlayerId()) {
            ManagerUtils.sendMessagePlayer(server, player, Lang.getDefLangMsg(LangType.GAME_MSG_REPORT_NEW_HIMSELF));
            return;
        }
        //проверить порог доступа к созданию или созданию сразу с решением
        boolean isResultReport = false;
        if (player.getKarma() <= Manager.inst.karma.getPlayerReporter() || player.getKarma() + player.getKarmaSpeed() <= Manager.inst.karma.getPlayerReporter()) {
            ManagerUtils.sendMessagePlayer(server, player, Lang.getDefLangMsg(LangType.GAME_MSG_KARMA_LOW, Integer.toString(Manager.inst.karma.getPlayerReporter())));
            return;
        } else if (player.getKarma() > Manager.inst.karma.getPlayerGuard() && player.getKarma() + player.getKarmaSpeed() > Manager.inst.karma.getPlayerGuard()) {
            isResultReport = true;
        }
        //проверить когда игрок заходил последний раз
        if (!OnlineServer.isOnlinePlayer(target.getPlayerId())) {
            try {
                if (Manager.inst.db.getPlayerInfos(target, new Date(System.currentTimeMillis() - (48 * 60 * 60 * 1000))).isEmpty()) {
                    ManagerUtils.sendMessagePlayer(server, player, Lang.getDefLangMsg(LangType.GAME_MSG_ONLINE_PLAYER_TOOLONG));
                    return;
                }
            } catch (SQLException ex) {
                ManagerUtils.sendMessagePlayer(server, player, Lang.getDefLangMsg(LangType.GAME_MSG_ERROR_UNKNOWN));
                ex.printStackTrace();
                return;
            }
        }
        try {
            ActionPlayer ap = new ActionPlayer();
            //проверить все нерассмотренные репотры создающего за последний час
            if (!isResultReport) {
                List<Report> reports = Manager.inst.db.getReportsCreator(player, new Date(System.currentTimeMillis() - (60 * 60 * 1000)));
                if (reports != null && reports.size() > 0) {
                    for (Report report : reports) {
                        Set<ReportResult> result = report.getResults();
                        if (result == null || result.isEmpty()) {
                            ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_REPORT_PLAYER_ISOPEN));
                            ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_REPORTINFO, Integer.toString(report.getId())), Lang.getDefLangMsg(LangType.GAME_MSG_REPORTINFO_CMD, Integer.toString(report.getId())));
                            ManagerUtils.sendMessagePlayer(server, player, ap);
                            return;
                        }
                    }
                }
            }
            if (rm.getType() == null) {
                //отправить список типов
                ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_HEAD_REPORT_TYPE));
                List<ReportType> types = Manager.inst.db.getReportTypesActive();
                for (ReportType type : types) {
                    ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_REPORT_TYPE, Integer.toString(type.getId()), type.getShortDesription()), Lang.getDefLangMsg(LangType.GAME_MSG_REPORT_TYPE_CMD, type.getShortDesription(), type.getDesription(), target.getUsername(), Integer.toString(type.getId())));
                }
                ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_REPORT_TYPE_HELP), Lang.getDefLangMsg(LangType.GAME_MSG_REPORT_TYPE_CMD_HELP));
                ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_SPLITTER));
                ManagerUtils.sendMessagePlayer(server, player, ap);
                return;
            }
            //проверить тип
            ReportType reportType = Manager.inst.db.getReportType(rm.getType());
            if (reportType == null || reportType.isDeprecated()) {
                ManagerUtils.sendMessagePlayer(server, player, Lang.getDefLangMsg(LangType.GAME_MSG_REPORT_TYPE_HELP), Lang.getDefLangMsg(LangType.GAME_MSG_REPORT_TYPE_CMD_HELP));
                return;
            }
            //проверить на создание репорта сразу с решением
            if (isResultReport) {
                if (rm.getLevel() <= 0 || rm.getLevel() > 5) {
                    ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_HEAD_REPORT_LEVEL));
                    ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_REPORT_LEVEL), Lang.getDefLangMsg(LangType.GAME_MSG_REPORT_LEVEL_CMD, target.getUsername(), Integer.toString(reportType.getId())));
                    ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_HELP_REPORT_LEVEL), Lang.getDefLangMsg(LangType.GAME_MSG_MENU_HELP_REPORT_LEVEL_CMD));
                    ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_SPLITTER));
                    ManagerUtils.sendMessagePlayer(server, player, ap);
                    return;
                }
                //проверить поле меседжа на размер
                if (rm.getMsg() == null || rm.getMsg().trim().length() <= 3) {
                    ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_REPORTMSG_ERROR));
                    ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_REPORTMSG_HELP, target.getUsername(), Integer.toString(reportType.getId()), Integer.toString(rm.getLevel())));
                    ManagerUtils.sendMessagePlayer(server, player, ap);
                    return;
                }
                if (rm.getMsg().trim().length() > 200) {
                    ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_REPORTMSG_ERROR_TOOLONG));
                    ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_REPORTMSG_HELP, target.getUsername(), Integer.toString(reportType.getId()), Integer.toString(rm.getLevel())));
                    ManagerUtils.sendMessagePlayer(server, player, ap);
                    return;
                }
            }
            //проверить все заявки младше 48 часов и не рассмотренные на обвиняемого, если есть то предложить дополнение
            List<Report> reportsAccus = Manager.inst.db.getReportsAccused(target, reportType, new Date(System.currentTimeMillis() - (48 * 60 * 60 * 1000)));
            if (reportsAccus != null && reportsAccus.size() > 0) {
                for (Report report : reportsAccus) {
                    Set<ReportResult> result = report.getResults();
                    if (result == null || result.isEmpty()) {
                        ManagerUtils.sendMessagePlayer(server, player, Lang.getDefLangMsg(LangType.GAME_MSG_REPORT_ACCUSED_ISOPEN, Integer.toString(report.getId())), Lang.getDefLangMsg(LangType.GAME_MSG_REPORT_ACCUSED_ISOPEN_CMD, Integer.toString(report.getId())));
                        return;
                    }
                }
            }
            //проверить поля описания
            if (rm.getMsgs() == null || rm.getMsgs().isEmpty()) {
                Utils.sendMenuDescriptionReport(server, player, rm, -1, MsgType.REPORT_CREATE_ADD);
                return;
            }
            StringBuilder sbMsgs = new StringBuilder();
            int length = 0;
            for (String msg : rm.getMsgs()) {
                length = length + msg.trim().length();
                sbMsgs.append(msg);
                sbMsgs.append(" ");
            }
            if (length < 10 || MsgType.getType(rm.getTypeMsg()) == MsgType.REPORT_CREATE_ADD) {
                Utils.sendMenuDescriptionReport(server, player, rm, length, MsgType.REPORT_CREATE_ADD);
                return;
            }
            //окончательная проверка всех данных пройдена, создаетм репорт
            if (MsgType.getType(rm.getTypeMsg()) != MsgType.REPORT_CREATE_COMPL || rm.getTime() == null) {
                ManagerUtils.sendMessagePlayer(server, player, Lang.getDefLangMsg(LangType.GAME_MSG_ERROR_UNKNOWN));
                return;
            }
            //создание репорта
            Report report = ManagerUtils.createReport(server, player, target, reportType, rm.getTime());
            if (report != null) {
                try {
                    Manager.inst.db.createReportMsg(server, player, report, sbMsgs.toString().trim(), rm.getTime());
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                if (isResultReport) {
                    ReportResult reportResult = ManagerUtils.createReportAndCompl(report, server, player, rm.getMsg().trim(), rm.getTime(), rm.getLevel());
                    if (reportResult != null) {
                        target.setKarmaSpeed(target.getKarmaSpeed() + Manager.inst.karma.getPenaltyOfLevel(rm.getLevel(), target.getKarma()));
                        if (Manager.inst.db.updatePlayer(target)) {
                            //отправить новую карму на все онлайн сервера
                            ManagerUtils.sendChangeKarma(target);
                            //отправить сообщение игроку на все сервера о новом репорте
                            //!!!!!!! ДОБАВИТЬ КОМАНДУ ДЛЯ КЛИКА ПО СООБЩЕНИЮ
                            //ManagerUtils.sendMessagePlayer(null, target, Lang.getDefLangMsg(LangType.GAME_MSG_REPORT_ACCUSED_NEW_CONS, Integer.toString(report.getId()), reportType.getShortDesription(), report.getCreator().getUsername(), Integer.toString(penalty)));
                            //всем подписанным игрокам
                            //!!!!!!! ДОБАВИТЬ КОМАНДУ ДЛЯ КЛИКА ПО СООБЩЕНИЮ
                            ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_REPORT_CHANGE_NEW_CONS, player.getUsername(), Integer.toString(report.getId()), reportType.getShortDesription(), target.getUsername()));
                            player.setKarmaSpeed(player.getKarmaSpeed() + Manager.inst.karma.getRewardReport());
                            if (Manager.inst.db.updatePlayer(player)) {
                                //отправить новую карму на все онлайн сервера
                                ManagerUtils.sendChangeKarma(player);
                                //**{0}** создал репорт __{1}__ на **{2}**, тип репорта: _{3}_, степень: **{4}**, описание: __{6}__, {7}.
                                Manager.inst.discordBot.processLogMessage(Lang.getDefLangMsg(LangType.DISCORD_MSG_REPORT_NEWCONS,
                                        ManagerUtils.shieldingMarkdown(player.getUsername()),
                                        Integer.toString(report.getId()),
                                        ManagerUtils.shieldingMarkdown(target.getUsername()),
                                        reportType.getShortDesription(), 
                                        Integer.toString(rm.getLevel()),
                                        ManagerUtils.shieldingMarkdown(rm.getMsg().trim()),
                                        ManagerUtils.shieldingMarkdown(sbMsgs.toString().trim())), 0);
                            } else {
                                Manager.inst.console.outRED("Не получилось обновить плеера при создании репорта!");
                            }
                        } else {
                            Manager.inst.console.outRED("Не получилось обновить плеера при создании репорта!");
                        }
                    }
                } else {
                    //отправить сообщение игроку на все сервера о новом репорте
                    //!!!!!!! ДОБАВИТЬ КОМАНДУ ДЛЯ КЛИКА ПО СООБЩЕНИЮ
                    //ManagerUtils.sendMessagePlayer(null, target, Lang.getDefLangMsg(LangType.GAME_MSG_REPORT_ACCUSED_NEW, Integer.toString(report.getId()), reportType.getShortDesription(), report.getCreator().getUsername()));
                    ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_REPORT_CHANGE_NEW, report.getCreator().getUsername(), Integer.toString(report.getId()), reportType.getShortDesription(), report.getAccused().getUsername()));
                    //**{0}** подал репорт: __#{1}__, на **{2}**, тип репорта: _{3}_, описание: {4}.
                    Manager.inst.discordBot.processLogMessage(Lang.getDefLangMsg(LangType.DISCORD_MSG_REPORT_NEW,
                            ManagerUtils.shieldingMarkdown(player.getUsername()),
                            Integer.toString(report.getId()),
                            ManagerUtils.shieldingMarkdown(target.getUsername()),
                            reportType.getShortDesription(), 
                            ManagerUtils.shieldingMarkdown(sbMsgs.toString().trim())), 0);
                }
                ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_REPORTINFO, Integer.toString(report.getId())), Lang.getDefLangMsg(LangType.GAME_MSG_REPORTINFO_CMD, Integer.toString(report.getId())));
                //отписать всем о новом репорте
                ManagerUtils.sendMessagePlayer(null, null, ap);
            }
        } catch (SQLException ex) {
            ManagerUtils.sendMessagePlayer(server, player, Lang.getDefLangMsg(LangType.GAME_MSG_ERROR_UNKNOWN));
            ex.printStackTrace();
        }
    }

}
