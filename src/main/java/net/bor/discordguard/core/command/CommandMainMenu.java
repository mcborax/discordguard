package net.bor.discordguard.core.command;

// @author ArtBorax
import java.sql.SQLException;
import net.bor.discordguard.core.Manager;
import net.bor.discordguard.core.ManagerUtils;
import net.bor.discordguard.dao.map.Player;
import net.bor.discordguard.dao.map.Server;
import net.bor.discordguard.lang.Lang;
import net.bor.discordguard.lang.LangType;
import net.bor.discordguard.map.ActionPlayer;

public class CommandMainMenu {

    public static void execute(int serverId, int playerId) {
        Player player = null;
        try {
            player = Manager.inst.db.getPlayer(playerId);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        if (player == null) {
            return;
        }
        Server server = null;
        try {
            server = Manager.inst.db.getServer(serverId);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        if (server == null) {
            return;
        }
        ActionPlayer ap = new ActionPlayer();
        ManagerUtils.sendChatKarma(player, ap);
        ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_REPORS, player.getUsername()), Lang.getDefLangMsg(LangType.GAME_MSG_MENU_REPORS_CMD, player.getUsername()));
        ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_REPORSCONS), Lang.getDefLangMsg(LangType.GAME_MSG_MENU_REPORSCONS_CMD));
        ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_LOGCHAT), Lang.getDefLangMsg(LangType.GAME_MSG_MENU_LOGCHAT_CMD));
        ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_NEWREPORT), Lang.getDefLangMsg(LangType.GAME_MSG_MENU_NEWREPORT_CMD));
        ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_TOP), Lang.getDefLangMsg(LangType.GAME_MSG_MENU_TOP_CMD));
        ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_PLAYERSTAT, player.getUsername()), Lang.getDefLangMsg(LangType.GAME_MSG_MENU_PLAYERSTAT_CMD, player.getUsername()));
        ManagerUtils.sendMessagePlayer(server, player, ap);
    }

}
