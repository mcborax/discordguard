package net.bor.discordguard.core.command;

// @author ArtBorax
import java.sql.SQLException;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import net.bor.discordguard.core.Manager;
import net.bor.discordguard.core.ManagerUtils;
import net.bor.discordguard.dao.map.*;
import net.bor.discordguard.lang.Lang;
import net.bor.discordguard.lang.LangType;
import net.bor.discordguard.map.*;
import net.bor.discordguard.rabbit.MsgType;

public class Utils {

    private final static ConcurrentHashMap<Integer, Long> codeLink = new ConcurrentHashMap<>();

    public static void sendMenuDescriptionReport(Server server, Player player, ReportMessage rm, int length, MsgType type) {
        ActionPlayer ap = new ActionPlayer();
        if (length == -1) {
            ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_SPLITTER));
            ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_REPORTMSGS_WARN));
            rm.setMsgs(null);
        } else if (length < 10) {
            ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_SPLITTER));
            ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_REPORTMSGS_ERROR_TOOSHORT));
            rm.setMsgs(null);
        }
        rm.setTypeMsg(type.toString());
        rm.setPlayerUUID(player.getUuid());
        rm.setUsername(player.getUsername());
        Manager.inst.rabbit.putMsg(server.getId(), type, rm);
        ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_REPORTMSGS_ADD_HELP));
        if (rm.getMsgs() != null && !rm.getMsgs().isEmpty()) {
            if (rm.getReportId() == 0) {
                ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_HEAD_REPORT_MSGS, " NEW"));
            } else {
                ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_HEAD_REPORT_MSGS, Integer.toString(rm.getReportId())));
            }
            for (String msg : rm.getMsgs()) {
                ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_BODY_REPORT_MSGS, msg));
            }
            ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_REPORTMSGS_ADD_FULL), Lang.getDefLangMsg(LangType.GAME_MSG_REPORTMSGS_ADD_FULL_CMD));
        } else {
            ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_REPORTMSGS_ADD), Lang.getDefLangMsg(LangType.GAME_MSG_REPORTMSGS_ADD_CMD));
        }
        ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_SPLITTER));
        ManagerUtils.sendMessagePlayer(server, player, ap);
    }

    public static void sendChatKarma(int serverId, int playerId) {
        Player player = null;
        try {
            player = Manager.inst.db.getPlayer(playerId);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        if (player == null) {
            return;
        }
        Server server = null;
        try {
            server = Manager.inst.db.getServer(serverId);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        if (server == null) {
            return;
        }
        ActionPlayer ap = new ActionPlayer();
        ManagerUtils.sendChatKarma(player, ap);
        ManagerUtils.sendMessagePlayer(server, player, ap);
    }

    public synchronized static String getCodeLink(long discordUserID) {
        int code = (int) (Math.random() * 10000);
        if (code > 999 && code < 10000) {
            if (!codeLink.containsKey(code)) {
                codeLink.put(code, discordUserID);
                return Integer.toString(code);
            }
        }
        return getCodeLink(discordUserID);
    }

    public synchronized static Long getDiscordUserID_Code(int code) {
        Long result = codeLink.remove(code);
        if (result != null && result > 0) {
            for (int key : codeLink.keySet()) {
                if (Objects.equals(codeLink.get(key), result)) {
                    codeLink.remove(key);
                }
            }
        }
        return result;
    }

}
