package net.bor.discordguard.core.command;

// @author ArtBorax
import java.sql.SQLException;
import java.util.Date;
import java.util.Set;
import net.bor.discordguard.core.Manager;
import net.bor.discordguard.core.ManagerUtils;
import net.bor.discordguard.dao.map.Player;
import net.bor.discordguard.dao.map.Report;
import net.bor.discordguard.dao.map.ReportResult;
import net.bor.discordguard.dao.map.Server;
import net.bor.discordguard.lang.Lang;
import net.bor.discordguard.lang.LangType;
import net.bor.discordguard.map.ActionPlayer;
import net.bor.discordguard.map.ReportMessage;

public class CommandConsiderReport {

    public static void execute(ReportMessage rm) {
        //report consider 12 3 краткое описание расследования
        Server server = null;
        Player player = null;
        Report report = null;
        try {
            server = Manager.inst.db.getServer(rm.getServerId());
            player = Manager.inst.db.getPlayer(rm.getPlayerId());
            report = Manager.inst.db.getReport(rm.getReportId());
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        if (server == null || player == null) {
            return;
        }
        if (report == null) {
            ManagerUtils.sendMessagePlayer(server, player, Lang.getDefLangMsg(LangType.GAME_MSG_REPORT_NOTFOUND, Integer.toString(rm.getReportId())));
            return;
        }
        if (report.getCreator().getPlayerId() == rm.getPlayerId()) {
            ManagerUtils.sendMessagePlayer(server, player, Lang.getDefLangMsg(LangType.GAME_MSG_REPORTCONS_HIMSELF_CREATOR));
            return;
        }
        if (report.getAccused().getPlayerId() == rm.getPlayerId()) {
            ManagerUtils.sendMessagePlayer(server, player, Lang.getDefLangMsg(LangType.GAME_MSG_REPORTCONS_HIMSELF_ACCUSED));
            return;
        }
        int lastLevel = -2;
        int tempId = 0;
        for (ReportResult reportResult : report.getResults()) {
            if (reportResult.getPlayer().getPlayerId() == rm.getPlayerId()) {
                ManagerUtils.sendMessagePlayer(server, player, Lang.getDefLangMsg(LangType.GAME_MSG_REPORTCONS_CONS_ALREADY));
                return;
            }
            if (reportResult.getId() > tempId) {
                tempId = reportResult.getId();
                lastLevel = reportResult.getLevel();
            }
        }
        if (report.getTime().before(new Date(System.currentTimeMillis() - (48 * 60 * 60 * 1000)))) {
            ManagerUtils.sendMessagePlayer(server, player, Lang.getDefLangMsg(LangType.GAME_MSG_REPORTCONS_EXPIRATION_END));
            return;
        }
        //проверить порог доступа по карме
        if (player.getKarma() <= report.getCostKarma() || player.getKarma() + player.getKarmaSpeed() <= report.getCostKarma()) {
            ManagerUtils.sendMessagePlayer(server, player, Lang.getDefLangMsg(LangType.GAME_MSG_KARMA_LOW, Integer.toString(report.getCostKarma())));
            return;
        }
        ActionPlayer ap = new ActionPlayer();
        if (rm.getLevel() <= -2 || rm.getLevel() > 5) {
            ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_HEAD_REPORT_LEVEL));
            ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_REPORTCONS_LEVEL), Lang.getDefLangMsg(LangType.GAME_MSG_REPORTCONS_LEVEL_CMD, Integer.toString(rm.getReportId())));
            ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_HELP_REPORT_LEVEL), Lang.getDefLangMsg(LangType.GAME_MSG_MENU_HELP_REPORT_LEVEL_CMD));
            ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_SPLITTER));
            ManagerUtils.sendMessagePlayer(server, player, ap);
            return;
        }
        if (lastLevel == rm.getLevel()) {
            ManagerUtils.sendMessagePlayer(server, player, Lang.getDefLangMsg(LangType.GAME_MSG_REPORTCONS_LEVEL_ALREADY));
            return;
        }
        //проверить поле меседжа на размер
        if (rm.getMsg() == null || rm.getMsg().trim().length() <= 3) {
            ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_REPORTMSG_ERROR));
            ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_REPORTCONS_MSG_HELP, Integer.toString(rm.getReportId()), Integer.toString(rm.getLevel())));
            ManagerUtils.sendMessagePlayer(server, player, ap);
            return;
        }
        if (rm.getMsg().trim().length() > 200) {
            ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_REPORTMSG_ERROR_TOOLONG));
            ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_REPORTCONS_MSG_HELP, Integer.toString(rm.getReportId()), Integer.toString(rm.getLevel())));
            ManagerUtils.sendMessagePlayer(server, player, ap);
            return;
        }
        Set<Player> players = null;
        try {
            players = Manager.inst.db.createReportResult(report, server, player, rm.getMsg().trim(), rm.getTime(), rm.getLevel(), Manager.inst.karma);
        } catch (SQLException ex) {
            ex.printStackTrace();
            ManagerUtils.sendMessagePlayer(server, player, Lang.getDefLangMsg(LangType.GAME_MSG_ERROR_UNKNOWN));
        }
        if (players != null && !players.isEmpty()) {
            for (Player playerChange : players) {
                //отправить новую карму на все онлайн сервера
                ManagerUtils.sendChangeKarma(playerChange);
            }
            if (players.size() == 3) {
                ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_REPORT_CHANGE_CONS, Integer.toString(rm.getReportId()), player.getUsername()));
                //**{0}** рассмотрел репорт __{1}__ на **{2}**, тип: _{3}_, степень: **{4}**, описание: __{5}__.
                Manager.inst.discordBot.processLogMessage(Lang.getDefLangMsg(LangType.DISCORD_MSG_REPORT_CONS,
                        ManagerUtils.shieldingMarkdown(player.getUsername()),
                        Integer.toString(report.getId()),
                        ManagerUtils.shieldingMarkdown(report.getAccused().getUsername()),
                        report.getType().getShortDesription(),
                        Integer.toString(rm.getLevel()),
                        ManagerUtils.shieldingMarkdown(rm.getMsg().trim())), 0);
            } else {
                ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_REPORT_CHANGE_CONSS, Integer.toString(rm.getReportId()), player.getUsername()));
                //**{0}** ПЕРЕСМОТРЕЛ репорт __{1}__ на **{2}**, тип: _{3}_, степень: **{4}**, описание: __{5}__.
                Manager.inst.discordBot.processLogMessage(Lang.getDefLangMsg(LangType.DISCORD_MSG_REPORT_CONSS,
                        ManagerUtils.shieldingMarkdown(player.getUsername()),
                        Integer.toString(report.getId()),
                        ManagerUtils.shieldingMarkdown(report.getAccused().getUsername()),
                        report.getType().getShortDesription(),
                        Integer.toString(rm.getLevel()),
                        ManagerUtils.shieldingMarkdown(rm.getMsg().trim())), 0);
            }
            ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_REPORTINFO, Integer.toString(rm.getReportId())), Lang.getDefLangMsg(LangType.GAME_MSG_REPORTINFO_CMD, Integer.toString(rm.getReportId())));
            ManagerUtils.sendMessagePlayer(null, null, ap);
        } else {
            ManagerUtils.sendMessagePlayer(server, player, Lang.getDefLangMsg(LangType.GAME_MSG_ERROR_UNKNOWN));
        }
    }

}
