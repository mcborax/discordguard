package net.bor.discordguard.core.command;

// @author ArtBorax
import java.sql.SQLException;
import net.bor.discordguard.core.Manager;
import net.bor.discordguard.core.ManagerUtils;
import net.bor.discordguard.core.discord.CommandPrivateHelpChat;
import net.bor.discordguard.dao.map.Player;
import net.bor.discordguard.dao.map.Server;
import net.bor.discordguard.lang.Lang;
import net.bor.discordguard.lang.LangType;
import net.bor.discordguard.map.ActionPlayer;

public class CommandLink {

    public static void execute(ActionPlayer msg) {
        Player player = null;
        try {
            player = Manager.inst.db.getPlayer(msg.getPlayerId());
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        if (player == null) {
            return;
        }
        Server server = null;
        try {
            server = Manager.inst.db.getServer(msg.getServerId());
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        if (server == null) {
            return;
        }
        if (msg.getMsg() != null && !msg.getMsg().isEmpty()) {
            int code = 0;
            try {
                code = Integer.parseInt(msg.getMsg());
            } catch (Exception ex) {
            }

            if (code > 999 && code < 10000) {
                Long discordUserId = Utils.getDiscordUserID_Code(code);
                if (discordUserId == null || discordUserId < 10000) {
                    ManagerUtils.sendMessagePlayer(server, player, Lang.getDefLangMsg(LangType.GAME_MSG_LINKED_ERROR));
                    return;
                } else {
                    try {
                        Long oldDiscordUserId = player.getDiscordUserID();
                        if (oldDiscordUserId == null) {
                            player.setKarmaSpeed(player.getKarmaSpeed() + Manager.inst.karma.getBonusLinkDiscord());
                        }
                        player.setDiscordUserID(discordUserId);
                        Manager.inst.db.updatePlayer(player);
                        ManagerUtils.sendMessagePlayer(server, player, Lang.getDefLangMsg(LangType.GAME_MSG_LINKED));
                        Manager.inst.discordBot.processPrivateMessage(discordUserId, Lang.getDefLangMsg(LangType.DISCORD_MSG_LINKED, player.getUsername()));
                        Manager.inst.discordBot.processPrivateMessage(discordUserId, Lang.getDefLangMsg(LangType.DISCORD_MSG_UNLINK_HELP));
                        CommandPrivateHelpChat.exrcute(discordUserId);
                        if (oldDiscordUserId == null) {
                            ManagerUtils.sendMessagePlayer(null, null, Lang.getDefLangMsg(LangType.GAME_MSG_BONUS_LINKED, player.getUsername(), Integer.toString(Manager.inst.karma.getBonusLinkDiscord())));
                        }
                        Manager.inst.discordBot.processLogMessage(Lang.getDefLangMsg(LangType.DISCORD_MSG_LINKED_LOG, ManagerUtils.shieldingMarkdown(player.getUsername())), 0);
                        return;
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }
                }
            } else {
                Manager.inst.console.outRED("!!!Неожиданно от игрока пришел неверный код!!! " + player.getUsername() + " [" + code + "]");
            }
        } else if (player.getDiscordUserID() == null || player.getDiscordUserID() < 10000) {
            ManagerUtils.sendMessagePlayer(server, player, Lang.getDefLangMsg(LangType.DISCORD_MSG_NEED_LINKED));
            return;
        } else {
            try {
                long discordUserId = player.getDiscordUserID();
                player.setDiscordUserID(0);
                Manager.inst.db.updatePlayer(player);
                ManagerUtils.sendMessagePlayer(server, player, Lang.getDefLangMsg(LangType.GAME_MSG_UNLINKED));
                String text = Lang.getDefLangMsg(LangType.DISCORD_MSG_UNLINKED, player.getUsername());
                Manager.inst.discordBot.processPrivateMessage(discordUserId, text);
                Manager.inst.discordBot.processLogMessage(text, 0);
                return;
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        ManagerUtils.sendMessagePlayer(server, player, Lang.getDefLangMsg(LangType.GAME_MSG_ERROR_UNKNOWN));
    }
}
