package net.bor.discordguard.core.command;

// @author ArtBorax
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import net.bor.discordguard.Settings;
import net.bor.discordguard.core.Manager;
import net.bor.discordguard.core.ManagerUtils;
import net.bor.discordguard.dao.map.Chat;
import net.bor.discordguard.dao.map.Player;
import net.bor.discordguard.dao.map.Server;
import net.bor.discordguard.lang.Lang;
import net.bor.discordguard.lang.LangType;
import net.bor.discordguard.map.ActionPlayer;

public class CommandLogChat {

    private static SimpleDateFormat datetime = new SimpleDateFormat("HH:mm:ss");
    private static SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");

    public static void execute(ActionPlayer msg) {
        Player target = null;
        Player player = null;
        try {
            player = Manager.inst.db.getPlayer(msg.getPlayerId());
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        if (player == null) {
            return;
        }
        Server server = null;
        try {
            server = Manager.inst.db.getServer(msg.getServerId());
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        if (server == null) {
            return;
        }

        int page = 1;
        if (msg.getMsg() != null && !msg.getMsg().isEmpty()) {
            try {
                page = Integer.parseInt(msg.getMsg());
            } catch (Exception ex) {
            }
        }
        if (page < 1) {
            page = 1;
        }
        try {
            ActionPlayer ap = new ActionPlayer();
            if (msg.getUsername() != null && !msg.getUsername().isEmpty()) {
                try {
                    target = Manager.inst.db.getPlayer(msg.getUsername());
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                if (target == null) {
                    ManagerUtils.sendMessagePlayer(server, player, Lang.getDefLangMsg(LangType.GAME_MSG_NOTFOUND_PLAYER, msg.getUsername()));
                    return;
                }
                List<Chat> chats = Manager.inst.db.getChatLog(target, (page - 1) * Settings.CHAT_PAGE_SIZE, Settings.CHAT_PAGE_SIZE + 1, 3);
                if (chats != null && chats.size() > 0) {
                    ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_HEAD_LOGCHAT_PLAYER, date.format(chats.get(0).getTime()), target.getUsername()));
                    for (int i = chats.size() - 1; i >= 0; i--) {
                        Chat chat = chats.get(i);
                        if (i >= Settings.CHAT_PAGE_SIZE) {
                            continue;
                        }
                        //23:08:01 x1 текст сообщения
                        String playerMsg = chat.getMsg();
                        if (chat.getType() > 0 && chat.getType() != 3) {
                            playerMsg = "/".concat(playerMsg);
                        }
                        if (chat.getCensLevel() > 0) {
                            String txt = Lang.getDefLangMsg(LangType.GAME_MSG_MENU_BODY_LOGCHAT_MSGCENS, datetime.format(chat.getTime()), chat.getServer().getName(), ManagerUtils.checkIn(playerMsg));
                            ap.addMsgs(txt, Lang.getDefLangMsg(LangType.GAME_MSG_MENU_BODY_LOGCHAT_MSG_CENS_CMD, txt, Integer.toString(chat.getCensLevel())));
                        } else if (chat.getType() == 3) {
                            ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_BODY_LOGCHAT_MSG_DISCORD, datetime.format(chat.getTime()), chat.getServer().getName(), playerMsg));
                        } else if (chat.getType() > 0) {
                            ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_BODY_LOGCHAT_MSGCMD, datetime.format(chat.getTime()), chat.getServer().getName(), playerMsg));
                        } else {
                            ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_BODY_LOGCHAT_MSG, datetime.format(chat.getTime()), chat.getServer().getName(), playerMsg));
                        }
                    }
                    if (page > 1 && chats.size() > Settings.CHAT_PAGE_SIZE) {
                        ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_FOOT_LOGCHAT, Integer.toString(page + 1), target.getUsername().concat(" ")), Lang.getDefLangMsg(LangType.GAME_MSG_MENU_FOOT_LOGCHAT_CMD, Integer.toString(page - 1), Integer.toString(page + 1), target.getUsername().concat(" ")));
                    } else if (page > 1 && chats.size() <= Settings.CHAT_PAGE_SIZE) {
                        ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_SPLITTER), Lang.getDefLangMsg(LangType.GAME_MSG_MENU_FOOT_LOGCHAT_FORWARD_CMD, Integer.toString(page - 1), target.getUsername().concat(" ")));
                    } else if (page == 1 && chats.size() <= Settings.CHAT_PAGE_SIZE) {
                        ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_SPLITTER));
                    } else if (page == 1 && chats.size() > Settings.CHAT_PAGE_SIZE) {
                        ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_FOOT_LOGCHAT, Integer.toString(page + 1), target.getUsername().concat(" ")), Lang.getDefLangMsg(LangType.GAME_MSG_MENU_FOOT_LOGCHAT_BACK_CMD, Integer.toString(page + 1), target.getUsername().concat(" ")));
                    }
                } else {
                    ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_HEAD_LOGCHAT_PLAYER, "----------", target.getUsername()));
                    ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_EMPTY));
                    ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_SPLITTER));
                }
            } else {
                List<Chat> chats = Manager.inst.db.getChatLog((page - 1) * Settings.CHAT_PAGE_SIZE, Settings.CHAT_PAGE_SIZE + 1, 0);
                if (chats != null && chats.size() > 0) {
                    ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_HEAD_LOGCHAT, date.format(chats.get(0).getTime())));
                    for (int i = chats.size() - 1; i >= 0; i--) {
                        Chat chat = chats.get(i);
                        if (i >= Settings.CHAT_PAGE_SIZE) {
                            continue;
                        }
                        //23:08:01 x1 test: текст сообщения
                        String playerMsg = chat.getMsg();
                        if (chat.getType() > 0 && chat.getType() != 3) {
                            playerMsg = "/".concat(playerMsg);
                        }
                        if (chat.getCensLevel() > 0) {
                            String txt = Lang.getDefLangMsg(LangType.GAME_MSG_MENU_BODY_LOGCHAT_PLAYER_MSGCENS, datetime.format(chat.getTime()), chat.getServer().getName(), chat.getPlayer().getUsername(), ManagerUtils.checkIn(playerMsg));
                            ap.addMsgs(txt, Lang.getDefLangMsg(LangType.GAME_MSG_MENU_BODY_LOGCHAT_PLAYER_MSG_CENS_CMD, txt, chat.getPlayer().getUsername(), Integer.toString(chat.getCensLevel())));
                        } else if (chat.getType() == 3) {
                            String txt = Lang.getDefLangMsg(LangType.GAME_MSG_MENU_BODY_LOGCHAT_PLAYER_MSG_DISCORD, datetime.format(chat.getTime()), chat.getServer().getName(), chat.getPlayer().getUsername(), ManagerUtils.checkIn(chat.getMsg()));
                            ap.addMsgs(txt, Lang.getDefLangMsg(LangType.GAME_MSG_MENU_BODY_LOGCHAT_PLAYER_MSG_CMD_DISCORD, txt, chat.getPlayer().getUsername()));
                        } else {
                            String txt = Lang.getDefLangMsg(LangType.GAME_MSG_MENU_BODY_LOGCHAT_PLAYER_MSG, datetime.format(chat.getTime()), chat.getServer().getName(), chat.getPlayer().getUsername(), ManagerUtils.checkIn(chat.getMsg()));
                            ap.addMsgs(txt, Lang.getDefLangMsg(LangType.GAME_MSG_MENU_BODY_LOGCHAT_PLAYER_MSG_CMD, txt, chat.getPlayer().getUsername()));
                        }
                    }
                    if (page > 1 && chats.size() > Settings.CHAT_PAGE_SIZE) {
                        ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_FOOT_LOGCHAT, Integer.toString(page + 1)), Lang.getDefLangMsg(LangType.GAME_MSG_MENU_FOOT_LOGCHAT_CMD, Integer.toString(page - 1), Integer.toString(page + 1)));
                    } else if (page > 1 && chats.size() <= Settings.CHAT_PAGE_SIZE) {
                        ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_SPLITTER), Lang.getDefLangMsg(LangType.GAME_MSG_MENU_FOOT_LOGCHAT_FORWARD_CMD, Integer.toString(page - 1)));
                    } else if (page == 1 && chats.size() <= Settings.CHAT_PAGE_SIZE) {
                        ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_SPLITTER));
                    } else if (page == 1 && chats.size() > Settings.CHAT_PAGE_SIZE) {
                        ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_FOOT_LOGCHAT, Integer.toString(page + 1)), Lang.getDefLangMsg(LangType.GAME_MSG_MENU_FOOT_LOGCHAT_BACK_CMD, Integer.toString(page + 1)));
                    }
                } else {
                    ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_HEAD_LOGCHAT, "----------"));
                    ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_EMPTY));
                    ap.addMsgs(Lang.getDefLangMsg(LangType.GAME_MSG_MENU_SPLITTER));
                }
            }
            ManagerUtils.sendMessagePlayer(server, player, ap);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

}
