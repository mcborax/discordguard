package net.bor.discordguard.core;

// @author ArtBorax
public class FloodMsg {

    
    private int count;
    private long time;

    public FloodMsg(long time) {
        this.time = time;
        count = 1;
    }

    public int getCount() {
        return count;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public void setCount(int count) {
        this.count = count;
    }

}
