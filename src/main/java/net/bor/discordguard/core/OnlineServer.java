package net.bor.discordguard.core;

// @author ArtBorax
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import net.bor.discordguard.Settings;

public class OnlineServer {

    private final static ConcurrentHashMap<Online, Set<Online>> serversOnline = new ConcurrentHashMap<>();

    private static class Online {

        final int id;
        long lastTime;

        private Online(int id, long lastTime) {
            this.id = id;
            this.lastTime = lastTime;
        }

        private Online(int serverId) {
            this.id = serverId;
        }

        public void setOnlineTime(long time) {
            this.lastTime = time;
        }

        @Override
        public int hashCode() {
            return this.id;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final Online other = (Online) obj;
            if (this.id != other.id) {
                return false;
            }
            return true;
        }
    }

    public static boolean isOnlineServer(int serverId) {
        long curTime = System.currentTimeMillis();
        for (Online server : serversOnline.keySet()) {
            if (curTime - Settings.PING_TIMEOUT > server.lastTime) {
                serversOnline.remove(server);
                ManagerUtils.serverOfflineNow(server.id);
            } else if (server.id == serverId) {
                return true;
            }
        }
        return false;
    }

    public static boolean isOnlinePlayer(int playerId) {
        long curTime = System.currentTimeMillis();
        for (Online server : serversOnline.keySet()) {
            if (curTime - Settings.PING_TIMEOUT < server.lastTime) {
                for (Online player : serversOnline.get(server)) {
                    if (player.id == playerId && curTime - Settings.PING_TIMEOUT < player.lastTime) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static boolean isOnlinePlayer(int serverId, int playerId) {
        if (!isOnlineServer(serverId)) {
            return false;
        }
        long curTime = System.currentTimeMillis();
        Set<Online> playerOnline = serversOnline.get(new Online(serverId));
        boolean flag = false;
        for (Online player : playerOnline) {
            if (curTime - Settings.PING_TIMEOUT > player.lastTime) {
                playerOnline.remove(player);
            } else if (player.id == playerId) {
                flag = true;
            }
        }
        return flag;
    }

    public static long updateOnlinePlayer(int serverId, int playerId) {
        Set<Online> serverOnline = serversOnline.get(new Online(serverId));
        long curTime = System.currentTimeMillis();
        if (serverOnline == null) {
            Set<Online> playerOnline = Collections.newSetFromMap(new ConcurrentHashMap<Online, Boolean>());
            playerOnline.add(new Online(playerId, curTime));
            serversOnline.put(new Online(serverId, curTime), playerOnline);
        } else {
            for (Online online : serverOnline) {
                if (online.id == playerId) {
                    long result = curTime - online.lastTime;
                    online.lastTime = curTime;
                    return result;
                }
            }
            serverOnline.add(new Online(playerId, curTime));
        }
        return 0;
    }

    public static long removeOnlinePlayer(int serverId, int playerId) {
        Set<Online> serverOnline = serversOnline.get(new Online(serverId));
        if (serverOnline == null) {
            return 0;
        }
        long result = 0;
        for (Online online : serverOnline) {
            if (online.id == playerId) {
                result = System.currentTimeMillis() - online.lastTime;
                serverOnline.remove(online);
                break;
            }
        }
        return result;
    }

    public static List<Integer> getServerOnlinePlayers(int serverId) {
        List<Integer> result = new ArrayList<>();
        if (!isOnlineServer(serverId)) {
            return result;
        }
        long curTime = System.currentTimeMillis() - Settings.PING_TIMEOUT;
        serversOnline.get(new Online(serverId)).stream().filter((player) -> (curTime < player.lastTime)).forEach((player) -> {
            result.add(player.id);
        });
        return result;
    }

    public static List<Integer> getServersOnlinePlayer(int playerId) {
        List<Integer> result = new ArrayList<>();
        long curTime = System.currentTimeMillis() - Settings.PING_TIMEOUT;
        serversOnline.keySet().stream().filter((server) -> (curTime <= server.lastTime)).forEach((server) -> {
            serversOnline.get(server).stream().filter((player) -> (curTime <= player.lastTime)).forEach((item) -> {
                result.add(server.id);
            });
        });
        return result;
    }

    public static List<Integer> getServersOnline() {
        List<Integer> result = new ArrayList<>();
        long curTime = System.currentTimeMillis() - Settings.PING_TIMEOUT;
        for (Online server : serversOnline.keySet()) {
            if (curTime > server.lastTime) {
                serversOnline.remove(server);
                ManagerUtils.serverOfflineNow(server.id);
            } else {
                result.add(server.id);
            }
        }
        return result;
    }

    public static HashMap<Integer, HashMap<Integer, Long>> updateOnline(int serverId, Set<Integer> playersId) {
        long curTime = System.currentTimeMillis();
        Set<Online> playerOnline = Collections.newSetFromMap(new ConcurrentHashMap<Online, Boolean>());
        playersId.stream().forEach((playerId) -> {
            playerOnline.add(new Online(playerId, curTime));
        });
        HashMap<Integer, HashMap<Integer, Long>> lastOnlinePlayer = new HashMap<>();
        for (Online key : serversOnline.keySet()) {
            if (curTime - key.lastTime > Settings.PING_TIMEOUT) {
                HashMap<Integer, Long> lasts = lastOnlinePlayer.get(key.id);
                if (lasts == null) {
                    lasts = new HashMap<>();
                    lastOnlinePlayer.put(key.id, lasts);
                }
                Set<Online> rem = serversOnline.remove(key);
                if (rem != null) {
                    for (Online on : rem) {
                        lasts.put(on.id, curTime - on.lastTime > Settings.PING_TIMEOUT ? Settings.PING_TIMEOUT : curTime - on.lastTime);
                    }
                }
                ManagerUtils.serverOfflineNow(key.id);
            } else if (key.id == serverId) {
                key.setOnlineTime(curTime);
            }
        }
        Set<Online> lastOnline = serversOnline.put(new Online(serverId, curTime), playerOnline);
        if (lastOnline != null) {
            HashMap<Integer, Long> lasts = lastOnlinePlayer.get(serverId);
            if (lasts == null) {
                lasts = new HashMap<>();
                lastOnlinePlayer.put(serverId, lasts);
            }
            for (Online on : lastOnline) {
                lasts.put(on.id, curTime - on.lastTime > Settings.PING_TIMEOUT ? Settings.PING_TIMEOUT : curTime - on.lastTime);
            }
        } else {
            ManagerUtils.serverOnlineNow(serverId);
        }
        return lastOnlinePlayer;
    }

}
