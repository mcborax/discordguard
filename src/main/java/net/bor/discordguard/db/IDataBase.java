package net.bor.discordguard.db;

// @author ArtBorax
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Set;
import net.bor.discordguard.dao.map.*;
import net.bor.discordguard.karma.IKarma;

public interface IDataBase {

    public List<String> getAllUsernamePlayers() throws SQLException;

    public Player getPlayer(int id) throws SQLException;

    public Player getPlayer(String username) throws SQLException;

    public Player getPlayerDiscordUserID(long discordUserID) throws SQLException;

    public List<Player> getTopPlayers() throws SQLException;

    public boolean updatePlayer(Player player) throws SQLException;

    public User getUser(String username, String uuid) throws SQLException;

    public Server getServer(int id) throws SQLException;

    public List<Server> getServers() throws SQLException;

    public void addServerChat(Server server, Player player, String msg, Date time, int cens, int type) throws SQLException;

    public ReportType getReportType(int id) throws SQLException;

    public List<ReportType> getReportTypes() throws SQLException;

    public List<ReportType> getReportTypesActive() throws SQLException;

    public Player getOrCreatePlayer(User user, Double karmaNewPlayer) throws SQLException;

    public Report getReport(int reportId) throws SQLException;

    public Report createReport(Server server, Player creator, Player accused, ReportType type, int costKarma, Date time) throws SQLException;

    public ReportMsg createReportMsg(Server server, Player player, Report report, String msg, Date time) throws SQLException;

    public Set<Player> createReportResult(Report report, Server server, Player player, String msg, Date time, int level, IKarma karma) throws SQLException;

    public ReportResult createReportResult(Report report, Server server, Player player, String msg, Date time, int level) throws SQLException;

    public PlayerInfo getPlayerInfo(int serverId, Player player) throws SQLException;

    public PlayerInfo getPlayerInfo(Server server, Player player) throws SQLException;

    public PlayerInfo createPlayerInfo(int serverId, Player player) throws SQLException;

    public List<PlayerInfo> getPlayerInfos(Player player, Date time) throws SQLException;

    public List<PlayerInfo> getPlayerInfos(Player player) throws SQLException;

    public void updatePlayerInfo(PlayerInfo playerInfo) throws SQLException;

    public List<Report> getReports(Player target, int offset, int limit) throws SQLException;

    public List<Report> getReports(double costKarma, int offset, int limit) throws SQLException;

    public List<Report> getReportsCreator(Player player, Date date) throws SQLException;

    public List<Report> getReportsAccused(Player target, ReportType type, Date date) throws SQLException;

    public List<Chat> getChatLog(Player player, int offset, int limit, int maxType) throws SQLException;

    public List<Chat> getChatLog(int offset, int limit, int maxType) throws SQLException;

    public void shutdown();
}
