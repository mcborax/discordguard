package net.bor.discordguard.db;

import java.util.HashMap;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.FlushModeType;
import net.bor.discordguard.console.IConsole;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;

// @author ArtBorax
public class DBConnector {

    private StandardServiceRegistry registry;
    private SessionFactory sessionFactory;
    private final IConsole console;
    private final String host;
    private final String dbName;
    private final String username;
    private final String pass;
    private final Class[] clazz;

    public DBConnector(IConsole console, String host, String dbName, String username, String pass, Class... clazz) {
        this.console = console;
        this.host = host;
        this.dbName = dbName;
        this.username = username;
        this.pass = pass;
        this.clazz = clazz;
    }

    private synchronized void createSessionFactory() {
        if (sessionFactory == null) {
            try {
                console.out("Connecting to database - " + dbName);

                Map<String, String> settings = new HashMap<>();
                settings.put(Environment.DRIVER, "com.mysql.cj.jdbc.Driver");
                settings.put(Environment.URL, "jdbc:mysql://" + host + "/" + dbName + "?characterEncoding=UTF-8&serverTimezone=UTC");
                settings.put(Environment.USER, username);
                settings.put(Environment.PASS, pass);
                settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL5Dialect");
//                settings.put(Environment.POOL_SIZE, "100");

                settings.put(Environment.AUTO_CLOSE_SESSION, "true");

                settings.put(Environment.CONNECTION_PROVIDER, "org.hibernate.service.jdbc.connections.internal.C3P0ConnectionProvider");
                settings.put(Environment.C3P0_ACQUIRE_INCREMENT, "10");
                settings.put(Environment.C3P0_MIN_SIZE, "5");
                settings.put(Environment.C3P0_MAX_SIZE, "2500");
                settings.put(Environment.C3P0_TIMEOUT, "30");
                settings.put(Environment.C3P0_MAX_STATEMENTS, "500");
                settings.put(Environment.C3P0_IDLE_TEST_PERIOD, "60");

                //validate - проверяет схему
                //update - обновляет и создает новые таблицы
                //crate - !удаляются все таблицы и создаются снова!
                //crate-drop - !!!уничтожает базу по закрытию сейщенфэктори!!!
                settings.put(Environment.HBM2DDL_AUTO, "update");
                settings.put(Environment.SHOW_SQL, "false"); //debug
//                settings.put(Environment.SHOW_SQL, "true"); //debug

//                settings.put(Environment.USE_SECOND_LEVEL_CACHE, "true");
//                settings.put(Environment.CACHE_REGION_FACTORY, "org.hibernate.cache.ehcache.EhCacheRegionFactory");
                registry = new StandardServiceRegistryBuilder().applySettings(settings).build();
                MetadataSources meta = new MetadataSources(registry);
                for (Class claz : clazz) {
                    meta.addAnnotatedClass(claz);
                }
                sessionFactory = meta.buildMetadata().buildSessionFactory();
                console.out("Connection ready db:" + dbName);
            } catch (Exception e) {
                e.printStackTrace();
                shutdown();
            }
        }
    }
    
    public EntityManager getEntityManager() {
        if (sessionFactory == null) {
            createSessionFactory();
        }
        EntityManager entityManager = sessionFactory.createEntityManager();
        entityManager.setFlushMode(FlushModeType.AUTO);
        return entityManager;
    }

    public void shutdown() {
        if (registry != null) {
            StandardServiceRegistryBuilder.destroy(registry);
            sessionFactory = null;
            console.out("Disconnecting from database - " + dbName);
        }
    }
}
