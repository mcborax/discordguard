package net.bor.discordguard.db;

// @author ArtBorax
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Set;
import net.bor.discordguard.config.IConfig;
import net.bor.discordguard.console.IConsole;
import net.bor.discordguard.dao.*;
import net.bor.discordguard.dao.map.*;
import net.bor.discordguard.karma.IKarma;

public class DataBase implements IDataBase {

    private final IConfig config;
    private final IConsole console;
    private final DBConnector dbDiscordGuard;
    private final DBConnector dbLauncher;
    private final DBConnector dbSite;

    public DataBase(IConfig config, IConsole console) {
        this.config = config;
        this.console = console;
        dbDiscordGuard = new DBConnector(console, config.getDBDiscordGuardHost(), config.getDBDiscordGuardName(), config.getDBDiscordGuardUsername(), config.getDBDiscordGuardPass(),
                Player.class, Chat.class, Server.class, QueueMsg.class, Report.class, ReportMsg.class, ReportResult.class, PlayerInfo.class, PlayerInfoPK.class, ReportType.class
        );
        dbSite = new DBConnector(console, config.getDBUsersHost(), config.getDBUsersName(), config.getDBUsersUsername(), config.getDBUsersPass(),
                User.class
        );
        dbLauncher = new DBConnector(console, config.getDBLauncherHost(), config.getDBLauncherName(), config.getDBLauncherUsername(), config.getDBLauncherPass(),
                Ban.class
        );
    }

    @Override
    public List<String> getAllUsernamePlayers() throws SQLException {
        PlayerDAO dao = new PlayerDAO(dbDiscordGuard.getEntityManager());
        return dao.findAllUsername();
    }

    @Override
    public Player getPlayer(int id) throws SQLException {
        PlayerDAO dao = new PlayerDAO(dbDiscordGuard.getEntityManager());
        return dao.find(id);
    }

    @Override
    public List<Player> getTopPlayers() throws SQLException {
        PlayerDAO dao = new PlayerDAO(dbDiscordGuard.getEntityManager());
        return dao.finds();
    }

    @Override
    public Player getPlayer(String username) throws SQLException {
        PlayerDAO dao = new PlayerDAO(dbDiscordGuard.getEntityManager());
        return dao.find(username);
    }

    @Override
    public Player getPlayerDiscordUserID(long discordUserID) throws SQLException {
        PlayerDAO dao = new PlayerDAO(dbDiscordGuard.getEntityManager());
        return dao.findDiscord(discordUserID);
    }

    @Override
    public boolean updatePlayer(Player player) throws SQLException {
        PlayerDAO dao = new PlayerDAO(dbDiscordGuard.getEntityManager());
        return dao.update(player);
    }

    @Override
    public User getUser(String username, String uuid) throws SQLException {
        UserDAO dao = new UserDAO(dbSite.getEntityManager());
        return dao.getUser(username, uuid);
    }

    @Override
    public Server getServer(int id) throws SQLException {
        ServerDAO dao = new ServerDAO(dbDiscordGuard.getEntityManager());
        return dao.find(id);
    }

    @Override
    public List<Server> getServers() throws SQLException {
        ServerDAO dao = new ServerDAO(dbDiscordGuard.getEntityManager());
        return dao.findAll();
    }

    @Override
    public void addServerChat(Server server, Player player, String msg, Date time, int cens, int type) throws SQLException {
        ChatDAO dao = new ChatDAO(dbDiscordGuard.getEntityManager());
        dao.addServerChat(server, player, msg, time, cens, type);
    }

    @Override
    public Player getOrCreatePlayer(User user, Double karmaNewPlayer) throws SQLException {
        if (user == null) {
            return null;
        }
        PlayerDAO dao = new PlayerDAO(dbDiscordGuard.getEntityManager());
        return dao.persist(user, karmaNewPlayer);
    }

    @Override
    public ReportType getReportType(int id) throws SQLException {
        ReportTypeDAO dao = new ReportTypeDAO(dbDiscordGuard.getEntityManager());
        return dao.find(id);
    }

    @Override
    public List<ReportType> getReportTypes() throws SQLException {
        ReportTypeDAO dao = new ReportTypeDAO(dbDiscordGuard.getEntityManager());
        return dao.findAll();
    }

    @Override
    public List<ReportType> getReportTypesActive() throws SQLException {
        ReportTypeDAO dao = new ReportTypeDAO(dbDiscordGuard.getEntityManager());
        return dao.findActive();
    }

    @Override
    public List<Chat> getChatLog(Player player, int offset, int limit, int maxType) throws SQLException {
        ChatDAO dao = new ChatDAO(dbDiscordGuard.getEntityManager());
        return dao.finds(player, offset, limit, maxType);
    }

    @Override
    public List<Chat> getChatLog(int offset, int limit, int maxType) throws SQLException {
        ChatDAO dao = new ChatDAO(dbDiscordGuard.getEntityManager());
        return dao.finds(offset, limit, maxType);
    }

    @Override
    public List<Report> getReports(Player player, int offset, int limit) throws SQLException {
        ReportDAO dao = new ReportDAO(dbDiscordGuard.getEntityManager());
        return dao.finds(player, offset, limit);
    }

    @Override
    public Report getReport(int reportId) throws SQLException {
        ReportDAO dao = new ReportDAO(dbDiscordGuard.getEntityManager());
        return dao.find(reportId);
    }

    @Override
    public List<Report> getReports(double costKarma, int offset, int limit) throws SQLException {
        ReportDAO dao = new ReportDAO(dbDiscordGuard.getEntityManager());
        return dao.finds(costKarma, offset, limit);
    }

    @Override
    public List<Report> getReportsCreator(Player player, Date date) throws SQLException {
        ReportDAO dao = new ReportDAO(dbDiscordGuard.getEntityManager());
        return dao.findsCreator(player, date);
    }

    @Override
    public List<Report> getReportsAccused(Player player, ReportType type, Date date) throws SQLException {
        ReportDAO dao = new ReportDAO(dbDiscordGuard.getEntityManager());
        return dao.findsAccused(player, type, date);
    }

    @Override
    public Report createReport(Server server, Player creator, Player accused, ReportType type, int costKarma, Date time) throws SQLException {
        if (server == null || creator == null || accused == null || type == null) {
            return null;
        }
        ReportDAO dao = new ReportDAO(dbDiscordGuard.getEntityManager());
        return dao.persist(server, creator, accused, type, costKarma, time);
    }

    @Override
    public ReportMsg createReportMsg(Server server, Player player, Report report, String msg, Date time) throws SQLException {
        if (report == null || server == null || player == null || msg == null || msg.isEmpty() || time == null) {
            return null;
        }
        ReportMsgDAO dao = new ReportMsgDAO(dbDiscordGuard.getEntityManager());
        return dao.persist(server, player, report, msg, time);
    }

    @Override
    public Set<Player> createReportResult(Report report, Server server, Player player, String msg, Date time, int level, IKarma karma) throws SQLException {
        if (report == null || server == null || player == null || time == null) {
            return null;
        }
        ReportResultDAO dao = new ReportResultDAO(dbDiscordGuard.getEntityManager(), karma);
        return dao.persistAndUpdate(report, server, player, msg, time, level);
    }

    @Override
    public ReportResult createReportResult(Report report, Server server, Player player, String msg, Date time, int level) throws SQLException {
        if (report == null || server == null || player == null || time == null) {
            return null;
        }
        ReportResultDAO dao = new ReportResultDAO(dbDiscordGuard.getEntityManager());
        return dao.persist(report, server, player, msg, time, level);
    }

    @Override
    public PlayerInfo getPlayerInfo(int serverId, Player player) throws SQLException {
        Server server = getServer(serverId);
        if (server == null) {
            return null;
        }
        return getPlayerInfo(server, player);
    }

    @Override
    public PlayerInfo getPlayerInfo(Server server, Player player) throws SQLException {
        PlayerInfoDAO dao = new PlayerInfoDAO(dbDiscordGuard.getEntityManager());
        return dao.find(server, player);
    }

    @Override
    public PlayerInfo createPlayerInfo(int serverId, Player player) throws SQLException {
        Server server = getServer(serverId);
        if (server == null) {
            return null;
        }
        PlayerInfoDAO dao = new PlayerInfoDAO(dbDiscordGuard.getEntityManager());
        return dao.persist(server, player);
    }

    @Override
    public List<PlayerInfo> getPlayerInfos(Player player) throws SQLException {
        PlayerInfoDAO dao = new PlayerInfoDAO(dbDiscordGuard.getEntityManager());
        return dao.finds(player);
    }

    @Override
    public List<PlayerInfo> getPlayerInfos(Player player, Date time) throws SQLException {
        PlayerInfoDAO dao = new PlayerInfoDAO(dbDiscordGuard.getEntityManager());
        return dao.finds(player, time);
    }

    @Override
    public void updatePlayerInfo(PlayerInfo playerInfo) throws SQLException {
        PlayerInfoDAO dao = new PlayerInfoDAO(dbDiscordGuard.getEntityManager());
        dao.update(playerInfo);
    }

    @Override
    public void shutdown() {
        dbLauncher.shutdown();
        dbSite.shutdown();
        dbDiscordGuard.shutdown();
    }

}
