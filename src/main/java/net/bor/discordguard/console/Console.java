package net.bor.discordguard.console;

// @author ArtBorax
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import jlibs.core.lang.Ansi;
import net.bor.discordguard.Settings;

public class Console implements IConsole {

    public static Ansi ansiRed = new Ansi(Ansi.Attribute.NORMAL, Ansi.Color.RED, Ansi.Color.BLACK);
    public static Ansi ansiYellow = new Ansi(Ansi.Attribute.NORMAL, Ansi.Color.YELLOW, Ansi.Color.BLACK);
    public static Ansi ansiGreen = new Ansi(Ansi.Attribute.NORMAL, Ansi.Color.GREEN, Ansi.Color.BLACK);
    public static Ansi ansiBlue = new Ansi(Ansi.Attribute.NORMAL, Ansi.Color.BLUE, Ansi.Color.BLACK);
    public static Ansi ansiCyan = new Ansi(Ansi.Attribute.NORMAL, Ansi.Color.CYAN, Ansi.Color.BLACK);
    public static SimpleDateFormat fulldata = new SimpleDateFormat("yyyy.MM.dd [HH:mm:ss] ");
    public static SimpleDateFormat data = new SimpleDateFormat("HH:mm:ss");

    @Override
    public void out(String msg) {
        long curTime = System.currentTimeMillis();
        logFile(msg, curTime);

        System.out.println(data.format(curTime).concat(" ").concat(msg));
    }

    @Override
    public void outCYAN(String msg) {
        long curTime = System.currentTimeMillis();
        logFile(msg, curTime);
        System.out.print(data.format(curTime));
        ansiCyan.out(" ".concat(msg).concat("\n"));
    }

    @Override
    public void outRED(String msg) {
        long curTime = System.currentTimeMillis();
        logFile(msg, curTime);
        System.out.print(data.format(curTime));
        ansiRed.out(" ".concat(msg).concat("\n"));
    }

    @Override
    public void outYELLOW(String msg) {
        long curTime = System.currentTimeMillis();
        logFile(msg, curTime);
        System.out.print(data.format(curTime));
        ansiYellow.out(" ".concat(msg).concat("\n"));
    }

    @Override
    public void outGREEN(String msg) {
        long curTime = System.currentTimeMillis();
        logFile(msg, curTime);
        System.out.print(data.format(curTime));
        ansiGreen.out(" ".concat(msg).concat("\n"));
    }

    @Override
    public void outBLUE(String msg) {
        long curTime = System.currentTimeMillis();
        logFile(msg, curTime);
        System.out.print(data.format(curTime));
        ansiBlue.out(" ".concat(msg).concat("\n"));
    }

    private static void logFile(String msg, long curTime) {
        PrintWriter out = null;
        try {
            out = new PrintWriter(new FileOutputStream(Settings.LOG_FILENAME, true));
            out.println(fulldata.format(curTime).concat(msg));
        } catch (FileNotFoundException ex) {
            ansiRed.out("Файл " + Settings.LOG_FILENAME + " выдал ошибку!\n");
        }
        if (out != null) {
            out.close();
        }
    }

}
