package net.bor.discordguard.console;

// @author ArtBorax
public interface IConsole {

    public void out(String msg);

    public void outCYAN(String msg);

    public void outRED(String msg);

    public void outYELLOW(String msg);

    public void outGREEN(String msg);

    public void outBLUE(String msg);

}
