package net.bor.discordguard.karma;

// @author ArtBorax
public interface IKarma {

    public void reload();

    public int getPenaltyOfLevel(int level, Double karma);

    public int getShiftReport(double playerKarma);

    public int getPlayerBan();

    public int getPlayerGuard();

    public int getPlayerReporter();

    public int getMaxAutoUp();

    public double getNewPlayer();

    public int getPercentSpeed();

    public double getSpeed1minute();

    public double getKeffLevel();

    public double getMultLevel();

    public double getMinPenalty();

    public int getRewardReport();

    public int getPenaltyLevelReport();

    public boolean idModerationByDictionary(Double karma, Double karmaSpeed);

    public double getActionPlayer(int action, double karma);

    public double getProcessKarma(double karma, double karmaSpeed);

    public double getProcessKarmaSpeed(double karma, double karmaSpeed);

    public boolean isMute(double karma, double karmaSpeed);

    public boolean isDiscordSendMsg(double karma, double karmaSpeed);

    public boolean isDiscordSendMsgBroadcast(double karma, double karmaSpeed);

    public int getBonusLinkDiscord();

}
