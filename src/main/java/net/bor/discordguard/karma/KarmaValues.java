package net.bor.discordguard.karma;

// @author ArtBorax
public class KarmaValues {

    public int KarmaMaxReport = 200;
    public int KarmaShiftReport = 10;
    public int KarmaPlayerBan = -1000;
    public int KarmaPlayerGuard = 200;
    public int KarmaPlayerReporter = 5;
    public int KarmaMaxAutoUp = 10;
    public double KarmaNewPlayer = 0;
    public int KarmaPercentSpeed = 10;
    public double KarmaSpeed1minute = 1.0;
    public double KarmaKeffLevel = 40.0;
    public double KarmaMultLevel = 2.0;
    public double KarmaMinPenalty = 5.0;
    public double KarmaKeffActionPlayer = 5.0;
    public int KarmaRewardReport = 10;
    public int KarmaPenaltyLevelReport = 3;
    public int KarmaModerationByDictionary = -50;
    public int PlayerMute = -150;
    public int DiscordSendMsg = 5;
    public int DiscordSendMsgBroadcast = 150;
    public int BonusLinkDiscord = 30;
}
