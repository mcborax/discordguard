package net.bor.discordguard.karma;

// @author ArtBorax
import java.io.IOException;
import net.bor.discordguard.Settings;
import net.bor.discordguard.config.IConfig;
import net.bor.discordguard.console.IConsole;

public class Karma implements IKarma {

    private final IConsole console;
    private final IConfig config;

    private KarmaValues karmaValues;

    public Karma(IConsole console, IConfig config) {
        this.console = console;
        this.config = config;
        reload();
    }

    private KarmaValues load(String filename) {
        try {
            KarmaValues result = config.load(filename, KarmaValues.class);
            if (result == null) {
                result = new KarmaValues();
                config.save(result, filename);
            }
            return result;
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public void reload() {
        karmaValues = load(Settings.CONFIG_KARMA_FILENAME);
    }

    @Override
    public double getActionPlayer(int action, double karma) {
        if (action == 0 || karma >= karmaValues.KarmaMaxAutoUp) {
            return 0;
        }
        double adsk = Math.abs(karma);
        if (adsk < 10) {
            return ((10.0 * karmaValues.KarmaKeffActionPlayer) / 100000.0) * (double) action;
        } else {
            return ((adsk * karmaValues.KarmaKeffActionPlayer) / 100000.0) * (double) action;
        }
    }

    @Override
    public double getProcessKarma(double karma, double karmaSpeed) {
        if (karmaSpeed == 0) {
            return karma;
        }
        if (karmaSpeed <= karmaValues.KarmaSpeed1minute && karmaSpeed >= 0 - karmaValues.KarmaSpeed1minute) {
            return karma;
        }
        if (karmaSpeed < 0) {
            return karma - (karmaValues.KarmaSpeed1minute / karmaValues.KarmaPercentSpeed);
        } else {
            return karma + (karmaValues.KarmaSpeed1minute / karmaValues.KarmaPercentSpeed);
        }
    }

    @Override
    public double getProcessKarmaSpeed(double karma, double karmaSpeed) {
        if (karmaSpeed == 0) {
            return 0;
        }
        if (karmaSpeed <= karmaValues.KarmaSpeed1minute && karmaSpeed >= 0 - karmaValues.KarmaSpeed1minute) {
            return 0;
        }
        if (karmaSpeed < 0) {
            return karmaSpeed + karmaValues.KarmaSpeed1minute;
        } else {
            return karmaSpeed - karmaValues.KarmaSpeed1minute;
        }
    }

    @Override
    public int getPenaltyOfLevel(int level, Double karma) {
        if (level < 1) {
            return 0;
        }
        double penalty = karmaValues.KarmaMinPenalty * ((double) level);
        if (karma > 0) {
            penalty = (karmaValues.KarmaMinPenalty * ((double) level)) + (karma / karmaValues.KarmaKeffLevel);
        }
        return 0 - ((int) (penalty * karmaValues.KarmaPercentSpeed));
    }

    @Override
    public int getShiftReport(double playerKarma) {
        int costKarma = (int) (playerKarma + karmaValues.KarmaShiftReport);
        if (costKarma > karmaValues.KarmaMaxReport) {
            return karmaValues.KarmaMaxReport;
        }
        return costKarma;
    }

    @Override
    public int getPlayerBan() {
        return karmaValues.KarmaPlayerBan;
    }

    @Override
    public int getPlayerGuard() {
        return karmaValues.KarmaPlayerGuard;
    }

    @Override
    public int getPlayerReporter() {
        return karmaValues.KarmaPlayerReporter;
    }

    @Override
    public int getMaxAutoUp() {
        return karmaValues.KarmaMaxAutoUp;
    }

    @Override
    public double getNewPlayer() {
        return karmaValues.KarmaNewPlayer;
    }

    @Override
    public int getPercentSpeed() {
        return karmaValues.KarmaPercentSpeed;
    }

    @Override
    public double getSpeed1minute() {
        return karmaValues.KarmaSpeed1minute;
    }

    @Override
    public double getKeffLevel() {
        return karmaValues.KarmaKeffLevel;
    }

    @Override
    public double getMultLevel() {
        return karmaValues.KarmaMultLevel;
    }

    @Override
    public double getMinPenalty() {
        return karmaValues.KarmaMinPenalty;
    }

    @Override
    public int getRewardReport() {
        return karmaValues.KarmaRewardReport;
    }

    @Override
    public int getPenaltyLevelReport() {
        return karmaValues.KarmaPenaltyLevelReport;
    }

    @Override
    public boolean idModerationByDictionary(Double karma, Double karmaSpeed) {
        if (karma + karmaSpeed <= karmaValues.KarmaModerationByDictionary) {
            return true;
        }
        return false;
    }

    @Override
    public boolean isMute(double karma, double karmaSpeed) {
        return karma + karmaSpeed < karmaValues.PlayerMute;
    }

    @Override
    public boolean isDiscordSendMsg(double karma, double karmaSpeed) {
        return karma + karmaSpeed >= karmaValues.DiscordSendMsg && karma >= karmaValues.DiscordSendMsg;
    }

    @Override
    public boolean isDiscordSendMsgBroadcast(double karma, double karmaSpeed) {
        return karma + karmaSpeed >= karmaValues.DiscordSendMsgBroadcast && karma >= karmaValues.DiscordSendMsgBroadcast;
    }

    @Override
    public int getBonusLinkDiscord() {
        return karmaValues.BonusLinkDiscord;
    }

}
