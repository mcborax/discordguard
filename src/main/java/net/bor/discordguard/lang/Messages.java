package net.bor.discordguard.lang;

// @author ArtBorax
import java.util.HashMap;

class Messages {

    private final static HashMap<LangType, String> values = new HashMap<>();

    static {
        values.put(LangType.GAME_MSG_MENU_HEAD_TOP, "§6------------ §fТоп игроков §6------------");
        values.put(LangType.GAME_MSG_MENU_BODY_TOP, "§6{0} §7карма §f{1} §7репорты §a{2}§7/§e{3}/§4{4}");
        values.put(LangType.GAME_MSG_MENU_BODY_TOP_CMD, "§6{0} §7карма §f{1} §7репорты (§a{2})[(hvr:§aСоздал репортов на игроков)]§7/(§e{3})[(hvr:§eРассмотрел репортов)]§7/(§4{4})[(hvr:§4Получил репортов от игроков!)]");

        values.put(LangType.GAME_MSG_MODER_OFF, "§aТвои сообщения больше не модерируются!");
        values.put(LangType.GAME_MSG_MODER_ON, "§cТеперь твои сообщения будут проверяться модераторами и только после одобрения будут отправлены в чат!");
        values.put(LangType.GAME_MSG_MODER_CANCEL, "§cТвоё сообщение не прошло модерацию, поскольку содержит ошибки или несуществующие слова!");

        values.put(LangType.GAME_MSG_MUTE, "§cУ тебя мут дружище! Из за очень низкой кармы.");

        values.put(LangType.GAME_MSG_FLOOD_WARN, "§cТы флудишь! Прекрати или тебя оштрафуют!!!");

        values.put(LangType.GAME_MSG_REPORT_CHANGE_APPEAL, "§7Репорт §b{0} §eапеллирует §9{1}§7! Советую пересмотреть.");
        values.put(LangType.GAME_MSG_REPORT_CHANGE_ADD, "§7Репорт §b{0} §eдополнен §a{1}§7! Советую пересмотреть.");
        values.put(LangType.GAME_MSG_REPORT_CHANGE_CONS, "§7Репорт §b{0} §7рассмотрел §a{1}§7! §cСоветую проверить!!!");
        values.put(LangType.GAME_MSG_REPORT_CHANGE_CONSS, "§7Репорт §b{0} §8ПЕРЕСМОТРЕЛ §a{1}§7! §cСоветую проверить!!!");
        values.put(LangType.GAME_MSG_REPORT_CHANGE_NEW, "§a{0}§7, §4подал репорт §b{1} §c{2}§7, на: §e{3}§7!");
        values.put(LangType.GAME_MSG_REPORT_CHANGE_NEW_CONS, "§a{0}§7, создал репорт §b{1} §c{2}§7, на: §e{3}§7!");
        values.put(LangType.GAME_MSG_REPORT_CHANGE_CREATE, "§a{0}§7, создал репорт §b{1} §c{2}§7, сервер: §b{3}§7, на: §e{4}§7!");

        values.put(LangType.GAME_MSG_REPORT_APPEAL, "§b/report add {0} §7Попытаться оспорить репорт?");
        values.put(LangType.GAME_MSG_REPORT_APPEAL_CMD, "§7Попытаться (§8<§cОСПОРИТЬ§8>)[(cmd:report add {0})(hvr:§eЕсли ты действительно не виновен и у тебя есть доказательства!)] §7репорт?");
        values.put(LangType.GAME_MSG_REPORT_NOTFOUND, "§cРепорт с номером §b{0}§c не найден!");

        values.put(LangType.GAME_MSG_REPORT_ACCUSED_NEW, "§cНа тебя поступил репорт: #§f{0}§c, §b{1}§c, от: §b{2}§c, репорт ожидает решения!");
        values.put(LangType.GAME_MSG_REPORT_ACCUSED_NEW_CONS, "§cНа тебя поступил репорт: #§f{0}§c, §b{1}§c от: §b{2}§c, репорт решен: §b{3}§c твоей кармы!");
        values.put(LangType.GAME_MSG_REPORT_ACCUSED_CANCEL, "§aРепорт с номером: §b{0}§a, решен: §b{1}§a, в твою пользу!");
        values.put(LangType.GAME_MSG_REPORT_ACCUSED_CONS, "§cРепорт с номером: §b{0}§c, решен: §b{1}§c, НЕ в твою пользу, §b{2}§c твоей кармы!");
        values.put(LangType.GAME_MSG_NOTFOUND_PLAYER, "§cИгрок с ником §b{0}§c не найден!");

        values.put(LangType.GAME_MSG_KARMA_LOW, "§cТебе не хватает кармы, нужно иметь больше §a{0}§c!");
        values.put(LangType.GAME_MSG_ONLINE_PLAYER_TOOLONG, "§cИгрок был слишком давно на проекте!");
        values.put(LangType.GAME_MSG_REPORT_NEW_HIMSELF, "§cНельзя подать репорт на самого себя, даже если очень хочется!");

        values.put(LangType.GAME_MSG_REPORT_TYPE, "§f{0} §7- §b{1}");
        values.put(LangType.GAME_MSG_REPORT_TYPE_CMD, "(§8<§b{0}§8>)[(hvr:§e{1})(cmd:report new {2} {3})]");
        values.put(LangType.GAME_MSG_REPORT_TYPE_HELP, "§7Выбери тип указав его номер в команде.");
        values.put(LangType.GAME_MSG_REPORT_TYPE_CMD_HELP, "§7Выбери тип нажав на него в чате.");

        values.put(LangType.GAME_MSG_MENU_HEAD_REPORT_LEVEL, "§6------------ §fСтепень нарушения §6------------");
        values.put(LangType.GAME_MSG_MENU_HELP_REPORT_LEVEL, "§7Выбери степень нарушения указав цифру в команде.");
        values.put(LangType.GAME_MSG_REPORT_LEVEL,
                "§f1 §7- §bШуточное оскорбление игрока без мата.\n"
                + "§f2 §7- §bОскорбление игрока без мата.\n"
                + "§f3 §7- §bМат без оскорблений кого либо.\n"
                + "§f4 §7- §bМножественный мат, оскорбления.\n"
                + "§f5 §7- §bПрименять крайне редко, тебя могут оштрафовать!");
        values.put(LangType.GAME_MSG_MENU_HELP_REPORT_LEVEL_CMD, "§7Выбери степень нарушения нажав на нее в чате и §fнапиши краткое описание!");
        values.put(LangType.GAME_MSG_REPORT_LEVEL_CMD,
                "(§8<§bМинимальное нарушение§8>)[(hvr:§eНапример: шуточное оскорбление игрока без мата.)(smd:report new {0} {1} 1 )]\n"
                + "(§8<§bНезначительное нарушение§8>)[(hvr:§eНапример: оскорбление игрока без мата)(smd:report new {0} {1} 2 )]\n"
                + "(§8<§bСреднее нарушение§8>)[(hvr:§eНапример: мат без оскорблений кого либо.)(smd:report new {0} {1} 3 )]\n"
                + "(§8<§bСильное нарушение§8>)[(hvr:§eНапример: множественный мат, оскорбления.)(smd:report new {0} {1} 4 )]\n"
                + "(§8<§bВышка§8>)[(hvr:§cПрименять крайне редко, тебя могут оштрафовать!)(smd:report new {0} {1} 5 )]");

        values.put(LangType.GAME_MSG_REPORT_CONS, "§b/report consider {0} §7рассмотреть этот репорт.");
        values.put(LangType.GAME_MSG_REPORT_CONS_CMD, "(§8<§aРАССМОТРЕТЬ§8>)[(cmd:report consider {0})] §7этот репорт?");

        values.put(LangType.GAME_MSG_REPORTCONS_EXPIRATION_END, "§cРепорт заблокирован! С момента создания прошло больше 48 часов.");

        values.put(LangType.GAME_MSG_REPORTCONS_LEVEL_ALREADY, "§cНельзя выбрать эту степень, поскольку она уже установлена!");

        values.put(LangType.GAME_MSG_REPORTCONS_LEVEL,
                "§f-1 §7- §bОтклонить репорт и выдать штраф за подачу.\n"
                + "§f0 §7- §bРепорт будет отклонен и никто не пострадает.\n"
                + "§f1 §7- §bШуточное оскорбление игрока без мата.\n"
                + "§f2 §7- §bОскорбление игрока без мата.\n"
                + "§f3 §7- §bМат без оскорблений кого либо.\n"
                + "§f4 §7- §bМножественный мат, оскорбления.\n"
                + "§f5 §7- §bПрименять крайне редко, тебя могут оштрафовать!");
        values.put(LangType.GAME_MSG_REPORTCONS_LEVEL_CMD,
                "(§8<§bОтклонить со штрафом§8>)[(hvr:§eОтклонить репорт и выдать штраф за подачу и/или неверный рассмотр.)(smd:report consider {0} -1 )]\n"
                + "(§8<§bОтклонить без штрафов и наград§8>)[(hvr:§eРепорт будет отклонен и никто не пострадает.)(smd:report consider {0} 0 )]\n"
                + "(§8<§bМинимальное нарушение§8>)[(hvr:§eНапример: шуточное оскорбление игрока без мата.)(smd:report consider {0} 1 )]\n"
                + "(§8<§bНезначительное нарушение§8>)[(hvr:§eНапример: оскорбление игрока без мата)(smd:report consider {0} 2 )]\n"
                + "(§8<§bСреднее нарушение§8>)[(hvr:§eНапример: мат без оскорблений кого либо.)(smd:report consider {0} 3 )]\n"
                + "(§8<§bСильное нарушение§8>)[(hvr:§eНапример: множественный мат, оскорбления.)(smd:report consider {0} 4 )]\n"
                + "(§8<§bВышка§8>)[(hvr:§cПрименять крайне редко, тебя могут оштрафовать!)(smd:report consider {0} 5 )]");

        values.put(LangType.GAME_MSG_REPORTCONS_MSG_HELP, "§7Пример: §b/report consider {0} {1} §fНарушение правил проекта");

        values.put(LangType.GAME_MSG_REPORTCONS_HIMSELF_ACCUSED, "§cТы не можешь рассмотреть этот репорт, потому что он подан на тебя!");
        values.put(LangType.GAME_MSG_REPORTCONS_HIMSELF_CREATOR, "§cТы не можешь рассмотреть этот репорт, потому что ты его создал!");
        values.put(LangType.GAME_MSG_REPORTCONS_CONS_ALREADY, "§cТы не можешь рассмотреть этот репорт, потому что ты его уже рассматривал ранее!");

        values.put(LangType.GAME_MSG_MENU_HEAD_REPORT_TYPE, "§6------------ §fТипы нарушений §6------------");
        values.put(LangType.GAME_MSG_REPORT_TYPE_ERROR, "§cНе верно указан тип репорта!");
        values.put(LangType.GAME_MSG_REPORTMSG_ERROR, "§cНе корректно указано краткое описание репорта!");
        values.put(LangType.GAME_MSG_REPORTMSG_ERROR_TOOLONG, "§cНе корректно указано краткое описание репорта, оно слишком длинное!");

        values.put(LangType.GAME_MSG_REPORTMSG_HELP, "§7Пример: §b/report new {0} {1} {2} §fМат в чате");

        values.put(LangType.GAME_MSG_REPORTMSGS_WARN, "§aНапиши максимально подробно причину репорта. §cПлохое описание без доказательств и ТЫ ПОЛУЧИШЬ ШТРАФ!!!");
        values.put(LangType.GAME_MSG_REPORTMSGS_ERROR_TOOSHORT, "§cТакое описание не годится, давай снова!");
        values.put(LangType.GAME_MSG_MENU_HEAD_REPORT_MSGS, "§6------ §fТвое описание репорта #§a{0} §6------");
        values.put(LangType.GAME_MSG_MENU_BODY_REPORT_MSGS, "§3{0}");

        values.put(LangType.GAME_MSG_MENU_HEAD_REPORTINFO, "§6------ §fИнформация репорта #§a{0} §fна §a{1} §6------");
        //Подал 2018-03-16 23:08:47 ArtBorax сервер x1
        //Тип: Нарушение в чате штраф -150 кармы
        values.put(LangType.GAME_MSG_MENU_BODY_REPORTINFO1, "§7Подал §b{0} §a{1} §7сервер §b{2}");
        values.put(LangType.GAME_MSG_MENU_BODY_REPORTINFO2, "§7Тип: §b{0}");
        values.put(LangType.GAME_MSG_MENU_BODY_REPORTINFO_NORESULT, "§7Репорт еще никто не рассмотрел");

        values.put(LangType.GAME_MSG_MENU_BODY_REPORTINFO_RESULTSPLIT, "§f------- Рассмотрел: §a{0} §f-------");
        //x1 2018-03-16 23:08:47 cтепень 1 
        //Нецензурная лексика: мат
        values.put(LangType.GAME_MSG_MENU_BODY_REPORTINFO_RESULT1, "§7{0} {1} степень §c{2}");
        values.put(LangType.GAME_MSG_MENU_BODY_REPORTINFO_RESULT2, "§c{0}");

        //x1 ArtBorax в 2018-03-16 23:08:47 дополнил: многострочный текст
        values.put(LangType.GAME_MSG_MENU_BODY_REPORTINFO_MSG_ADD, "§7{0} §a{1} §7в {2} §fдополнил:\n§d{3}");
        //x1 ArtBorax в 2018-03-16 23:08:47 апеллировал: многострочный текст
        values.put(LangType.GAME_MSG_MENU_BODY_REPORTINFO_MSG_APPEAL, "§7{0} §c{1} §7в {2} §fапеллировал:\n§c{3}");

        values.put(LangType.GAME_MSG_REPORTINFO, "§b/report info {0} §7смотреть репорт");
        values.put(LangType.GAME_MSG_REPORTINFO_CMD, "(§8<§aСМОТРЕТЬ РЕПОРТ§8>)[(cmd:report info {0})]");

        values.put(LangType.GAME_MSG_REPORTMSGS_ADD_HELP, "§7Отправляй сообщения просто в чат и они будут добавляться в описание. Отправлять можно несколько сообщений если в одно не помещается.");
        values.put(LangType.GAME_MSG_REPORTMSGS_ADD, "§cOтменить репорт: §b/report cancel");
        values.put(LangType.GAME_MSG_REPORTMSGS_ADD_FULL, "§aОтправить репорт: §b/report complete §cотменить репорт: §b/report cancel");
        values.put(LangType.GAME_MSG_REPORTMSGS_ADD_CMD, "(§8<§4Отменить репорт§8>)[(cmd:report cancel)]");
        values.put(LangType.GAME_MSG_REPORTMSGS_ADD_FULL_CMD, "(§8<§aОтправить репорт§8>)[(cmd:report complete)(hvr:§c!!! В Н И М А Н И Е !!!\nНеправильно составленный репорт приведет к списанию кармы у тебя - §4ШТРАФ!\n§eПосле нажатия этой кнопки отменить репорт нельзя!)] §8- (§8<§4Отменить репорт§8>)[(cmd:report cancel)]");

        values.put(LangType.GAME_MSG_REPORT_PLAYER_ISOPEN, "§cТы уже создавал недавно репорт и его еще не обработали!");
        values.put(LangType.GAME_MSG_REPORT_ACCUSED_ISOPEN, "§cНа игрока уже есть репорт, дополнить? §b/report add {0}");
        values.put(LangType.GAME_MSG_REPORT_ACCUSED_ISOPEN_CMD, "§cНа игрока уже есть репорт, (§8<§aДОПОЛНИТЬ§8>§c?)[(cmd:report add {0})]");
        values.put(LangType.GAME_MSG_REPORT_ADD, "§b/report add {0} §7дополнить этот репорт.");
        values.put(LangType.GAME_MSG_REPORT_ADD_CMD, "(§8<§aДОПОЛНИТЬ§8>)[(cmd:report add {0})] §7этот репорт?");

        values.put(LangType.GAME_MSG_REPORTMSGS_ADD_EXPIRATION_END, "§cРепорт заблокирован! С момента создания прошло больше 24 часов.");

        values.put(LangType.GAME_MSG_KARMA_PLAYER_POS, "§2У тебя хорошая карма: §a{0}");
        values.put(LangType.GAME_MSG_KARMA_PLAYER_NEG, "§4У тебя плохая карма: §c{0}");

        values.put(LangType.GAME_MSG_MENU_REPORS_PLAYER, "§7Репорты §b/reports {0}");
        values.put(LangType.GAME_MSG_MENU_REPORS_PLAYER_CMD, "(§8<§6Репорты на {0}§8>)[(cmd:reports {0})]");
        values.put(LangType.GAME_MSG_MENU_REPORS, "§7Репорты на меня §b/reports {0}");
        values.put(LangType.GAME_MSG_MENU_REPORS_CMD, "(§8<§6Репорты на меня§8>)[(cmd:reports {0})]");
        values.put(LangType.GAME_MSG_MENU_REPORSCONS, "§7Доступные для рассмотра репорты §b/reports");
        values.put(LangType.GAME_MSG_MENU_REPORSCONS_CMD, "(§8<§6Доступные для рассмотра репорты§8>)[(cmd:reports)]");
        values.put(LangType.GAME_MSG_MENU_LOGCHAT, "§7История чата §b/report log");
        values.put(LangType.GAME_MSG_MENU_LOGCHAT_CMD, "(§8<§6История чата§8>)[(cmd:report log)]");
        values.put(LangType.GAME_MSG_MENU_NEWREPORT, "§7Подать репорт на игрока §b/report new НИК");
        values.put(LangType.GAME_MSG_MENU_NEWREPORT_CMD, "(§8<§6Подать репорт на игрока§8>)[(smd:report new НИК)]");
        values.put(LangType.GAME_MSG_MENU_TOP, "§7Топ игроков §b/report top");
        values.put(LangType.GAME_MSG_MENU_TOP_CMD, "(§8<§6Топ игроков§8>)[(cmd:report top)]");
        values.put(LangType.GAME_MSG_MENU_PLAYERSTAT, "§7Стата §b/report stat {0}");
        values.put(LangType.GAME_MSG_MENU_PLAYERSTAT_CMD, "(§8<§6Стата {0}§8>)[(cmd:report stat {0})]");

        values.put(LangType.GAME_MSG_MENU_HEAD_REPORTSCONS, "§6---- §fДоступные репорты за 48 часов §6----");
        values.put(LangType.GAME_MSG_MENU_BODY_REPORTSCONS_EMPTY, "§aНет ни одного репорта который ты можешь рассмотреть или пересмотреть!");
        values.put(LangType.GAME_MSG_MENU_BODY_REPORTSCONS_OPEN, "§7#{0}, §c{1}§7, §aне решен§7, {2}, §c{3}§7.");
        values.put(LangType.GAME_MSG_MENU_BODY_REPORTSCONS_NEG, "§7#{0}, §c{1}§7, §cоштрафован§7, {2}, §c{3}§7.");
        values.put(LangType.GAME_MSG_MENU_BODY_REPORTSCONS_POS, "§7#{0}, §c{1}§7, отклонен§7, {2}, §a{3}§7.");
        values.put(LangType.GAME_MSG_MENU_BODY_REPORTSCONS_HVR, "§7Сервер: §b{0}§7, подал: §a{1}§7, в: {2}");

        values.put(LangType.GAME_MSG_PLAYER_LOGCHAT, "§7История чата §b/report log {0}");
        values.put(LangType.GAME_MSG_PLAYER_LOGCHAT_CMD, "(§8<§aИстория чата {0}§8>)[(cmd:report log {0})]");

        //2018-03-16 23:08:47 x1 test: текст сообщения
        values.put(LangType.GAME_MSG_MENU_BODY_LOGCHAT_PLAYER_MSG, "§9{0} §7{1} §6{2}§7: §b{3}");
        values.put(LangType.GAME_MSG_MENU_BODY_LOGCHAT_PLAYER_MSG_DISCORD, "§9{0} §7{1} §6{2}§7: §9{3}");
        values.put(LangType.GAME_MSG_MENU_BODY_LOGCHAT_PLAYER_MSGCMD, "§9{0} §7{1} §6{2}§7: {3}");
        values.put(LangType.GAME_MSG_MENU_BODY_LOGCHAT_PLAYER_MSGCENS, "§9{0} §7{1} §6{2}§7: §c{3}");
        values.put(LangType.GAME_MSG_MENU_BODY_LOGCHAT_PLAYER_MSG_CMD, "({0})[(cmd:report log {1})(hvr:§fНажми для просмотра истории сообщений §6{1})]");
        values.put(LangType.GAME_MSG_MENU_BODY_LOGCHAT_PLAYER_MSG_CMD_DISCORD, "({0})[(cmd:report log {1})(hvr:§fЭто сообщение было отправлено с дискорда!\nНажми для просмотра истории сообщений §6{1})]");
        values.put(LangType.GAME_MSG_MENU_BODY_LOGCHAT_PLAYER_MSG_CENS_CMD, "({0})[(cmd:report log {1})(hvr:§eЭто сообщение содержит нецензурную лексику!\n Степень §c{2})]");
        //2018-03-16 23:08:47 x1 текст сообщения
        values.put(LangType.GAME_MSG_MENU_BODY_LOGCHAT_MSG, "§9{0} §7{1} §b{2}");
        values.put(LangType.GAME_MSG_MENU_BODY_LOGCHAT_MSG_DISCORD, "§9{0} §7{1} §9{2}");
        values.put(LangType.GAME_MSG_MENU_BODY_LOGCHAT_MSGCMD, "§9{0} §7{1} {2}");
        values.put(LangType.GAME_MSG_MENU_BODY_LOGCHAT_MSGCENS, "§9{0} §7{1} §c{2}");
        values.put(LangType.GAME_MSG_MENU_BODY_LOGCHAT_MSG_CENS_CMD, "({0})[(hvr:§fЭто сообщение содержит нецензурную лексику!\n Степень §c{1})]");

        values.put(LangType.GAME_MSG_MENU_HEAD_LOGCHAT, "§6-- §f{0} §6 -- §fЛог чата §6--------------");
        values.put(LangType.GAME_MSG_MENU_HEAD_LOGCHAT_PLAYER, "§6-- §f{0} §6-- §fЛог чата §b{1} §6------------");
        values.put(LangType.GAME_MSG_MENU_FOOT_LOGCHAT, "§6------- §fлистать §b/report log {1}{0} §6-------");
        values.put(LangType.GAME_MSG_MENU_FOOT_LOGCHAT_BACK_CMD, "§6------------------- (§fназад>>>)[(cmd:report log {1}{0})] §6--------");
        values.put(LangType.GAME_MSG_MENU_FOOT_LOGCHAT_CMD, "§6-------- (§f<<<вперед)[(cmd:report log {2}{0})] §e| (§fназад>>>)[(cmd:report log {2}{1})] §6--------");
        values.put(LangType.GAME_MSG_MENU_FOOT_LOGCHAT_FORWARD_CMD, "§6-------- (§f<<<вперед)[(cmd:report log {1}{0})] §6--------------------");

        values.put(LangType.GAME_MSG_MENU_HEAD_PLAYERSTAT, "§6-------- §fСтатистика игрока §b{0} §6--------");
        values.put(LangType.GAME_MSG_MENU_BODY_PLAYERSTAT_KARMA, "§7Карма §4{0} §7репорты §a{1}§7/§e{2}/§4{3}");
        values.put(LangType.GAME_MSG_MENU_BODY_PLAYERSTAT_KARMA_CMD, "§7Карма §4{0} §7репорты (§a{1})[(hvr:§aСоздал репортов на игроков)]§7/(§e{2})[(hvr:§eРассмотрел репортов)]§7/(§4{3})[(hvr:§4Получил репортов от игроков!)]");
        values.put(LangType.GAME_MSG_MENU_BODY_PLAYERSTAT_OFFLINE, "§6{0} §f{1} §7{2} §f{3} §7{4} §f{5} §7{6} §9был {7}");
        values.put(LangType.GAME_MSG_MENU_BODY_PLAYERSTAT_ONLINE, "§a{0} §f{1} §7{2} §f{3} §7{4} §f{5} §7{6} §aиграет");

        values.put(LangType.GAME_MSG_MENU_SPLITTER, "§6---------------------------------------");
        values.put(LangType.GAME_MSG_EMPTY, "§aНет ни одной записи!");

        values.put(LangType.GAME_MSG_ERROR_UNKNOWN, "§4Неизвестная ошибка! Что-то пошло не так.");

        values.put(LangType.GAME_MSG_MENU_HEAD_REPORTS, "§6------------ §fРепорты на §b{0} §6------------");
        values.put(LangType.GAME_MSG_MENU_FOOT_REPORTS, "§6------- §fлистать §b/reports {1}{0} §6-------");
        values.put(LangType.GAME_MSG_MENU_FOOT_REPORTS_BACK_CMD, "§6------------------- (§fназад>>>)[(cmd:reports {1}{0})] §6--------");
        values.put(LangType.GAME_MSG_MENU_FOOT_REPORTS_CMD, "§6-------- (§f<<<вперед)[(cmd:reports {2}{0})] §e| (§fназад>>>)[(cmd:reports {2}{1})] §6--------");
        values.put(LangType.GAME_MSG_MENU_FOOT_REPORTS_FORWARD_CMD, "§6-------- (§f<<<вперед)[(cmd:reports {1}{0})] §6--------------------");
        values.put(LangType.GAME_MSG_MENU_BODY_REPORTS_OPEN, "§7#{0}, §aне решен§7, {1}, §c{2}§7.");
        values.put(LangType.GAME_MSG_MENU_BODY_REPORTS_NEG, "§7#{0}, §cоштрафован§7, {1}, §c{2}§7.");
        values.put(LangType.GAME_MSG_MENU_BODY_REPORTS_POS, "§7#{0}, §8отклонен§7, {1}, §a{2}§7.");

        values.put(LangType.GAME_MSG_MENU_BODY_REPORTS_HVR, "§7Сервер: §b{0}§7, подал: §a{1}§7, карма: §b{2}");
        values.put(LangType.GAME_MSG_MENU_BODY_REPORTS_HVR_RESULT, "§9{0}§7, рассмотрел: §b{1}§7, уровень §c{2}§7, описание: §e{3}");

        values.put(LangType.DESCRIPT_WORD_CENS, "Нецензурная лексика: {0}.");
        values.put(LangType.DESCRIPT_FLOOD, "Флуд: {0}.");
        values.put(LangType.DISCORD_MSG_SYSTEM_SERVER_ON, "Сервер включен!");
        values.put(LangType.DISCORD_MSG_SYSTEM_SERVER_OFF, "Сервер выключен!");
        values.put(LangType.DISCORD_SYSTEM_ERROR_UNKNOWN, "Неизвестная ошибка при построении JDA...");
        values.put(LangType.DISCORD_SYSTEM_ERROR_EMPTY_CHANEL, "Нет не одного текстового канала на сервере Discord");
        values.put(LangType.DISCORD_MSG_ERROR_RESPONSE, "Я не могу отправить тебе личные сообщения! Разреши отправку, настроив в дискорде **личные сообщения от участников сервера**");

        values.put(LangType.GAME_MSG_BONUS_LINKED, "§a{0} §6получил §a{1} §6бонусной кармы за то что связал дискорд со своим аккаунтом! §fНапиши: §b/link");

        values.put(LangType.GAME_MSG_LINKED, "§aТвоя учетная запись дискорда успешно связана с твоим ником!");
        values.put(LangType.GAME_MSG_UNLINKED, "§aТвоя учетная запись дискорда успешно отвязана от твоего ника!");
        values.put(LangType.GAME_MSG_LINKED_ERROR, "§cКод устарел или не был создан! Тебе нужно получить новый код! §aПросто напиши в чат любого сервера в нашем дискорде.");
        values.put(LangType.DISCORD_MSG_LINKED, "Твоя учетная запись дискорда успешно связана с **{0}**!");
        values.put(LangType.DISCORD_MSG_LINKED_LOG, "**{0}** связал учетную запись!");
        values.put(LangType.DISCORD_MSG_UNLINKED, "**{0}** отвязал учетную запись!");
        values.put(LangType.DISCORD_MSG_UNLINK_HELP, "Для того чтоб отвязать этот аккаунт, в игре просто набери команду: **/UNLINK**");
        values.put(LangType.DISCORD_MSG_NEED_LINKED, "Твоя учетная запись дискорда еще не связана с игровым аккаунтом!");
        values.put(LangType.DISCORD_MSG_NEED_LINKED_CODE, "Зайди на сервер и напиши эту команду **/LINK {0}**");
        values.put(LangType.DISCORD_MSG_NEED_LINKED_HELP, "Если команда не сработает, получи новый код написав в чат любого сервера в дискорде еще раз.");
        values.put(LangType.DISCORD_MSG_LOW_KARMA, "У тебя друг слишком мало кармы!");
        values.put(LangType.DISCORD_MSG_VERYLONG, "Слишком длинное сообщение!");
        values.put(LangType.DISCORD_MSG_GAME, "§9{0}§8: §a{1}");
        values.put(LangType.DISCORD_MSG_UNCORRECT, "В твоём сообщении есть некорректные символы!");
        values.put(LangType.DISCORD_MSG_SEND_FLOOD, "Данное сообщение считается флудом!");
        values.put(LangType.DISCORD_MSG_SEND_CENS, "Данное сообщение содержит ненормативную лексику!");
        values.put(LangType.DISCORD_MSG_FLOOD_WARN, "Ты флудишь! Прекрати или тебя оштрафуют!!!");
        values.put(LangType.DISCORD_MSG_ERROR_SERVER, "Невозможно отправить сообщение на этот сервер!");
        values.put(LangType.DISCORD_MSG_REPORT_NEWCONS, "**{0}** создал репорт: __#{1}__, на: **{2}**, тип: _{3}_, степень: **{4}**, описание: __{5}__, {6}.");
        values.put(LangType.DISCORD_MSG_REPORT_NEW, "**{0}** подал репорт: __#{1}__, на: **{2}**, тип: _{3}_, описание: {4}.");
        values.put(LangType.DISCORD_MSG_REPORT_CONS, "**{0}** рассмотрел репорт __{1}__ на: **{2}**, тип: _{3}_, степень: **{4}**, описание: __{5}__.");
        values.put(LangType.DISCORD_MSG_REPORT_CONSS, "**{0}** ПЕРЕСМОТРЕЛ репорт __{1}__ на: **{2}**, тип: _{3}_, степень: **{4}**, описание: __{5}__.");
        values.put(LangType.DISCORD_MSG_REPORT_ADD, "**{0}** дополнил репорт __{1}__ , описание: {2}.");
        values.put(LangType.DISCORD_MSG_REPORT_APPEAL, "**{0}** апеллирует репорт __{1}__ , описание: {2}.");
        values.put(LangType.DISCORD_MSG_REPORT_NEWCONSAUTO, "**{0}** создал репорт: __#{1}__, на: **{2}**, сервер: _{3}_, тип: _{4}_, степень: **{5}**, описание: {6}.");
        values.put(LangType.DISCORD_MSG_COMMAND_NOTFOUND, "Такой команды нет! **.help** - справка.");
        values.put(LangType.DISCORD_MSG_COMMAND_LIST, "{0} **игроков онлайн**: {1}");
        values.put(LangType.DISCORD_MSG_COMMAND_ACCESS_DENIED, "В доступе отказано!");
        values.put(LangType.DISCORD_MSG_COMMAND_OK, "Команда принята и выполняется.");
        
        values.put(LangType.DISCORD_MSG_ERROR_UNKNOWN, "Неизвестная ошибка!");
        
        values.put(LangType.DISCORD_MSG_COMMAND_SUBSCRIBE, "Ты успешно подписался на репорты которые тебе доступны для рассмотра или пересмотра.");
        values.put(LangType.DISCORD_MSG_COMMAND_UNSUBSCRIBE, "Ты успешно отписался.");
        values.put(LangType.DISCORD_MSG_COMMAND_UNSUBSCRIBE_ALREADY, "Ты еще не подписан, чтобы отписаться.");
        values.put(LangType.DISCORD_MSG_COMMAND_SUBSCRIBE_ALREADY, "Ты уже подписан.");

        values.put(LangType.DISCORD_MSG_COMMAND_HELP_LIST, "**.ls** - список игроков на сервере.");
        values.put(LangType.DISCORD_MSG_COMMAND_HELP_SHUTDOWN, "**.shutdown** - остановить сервер.");
        values.put(LangType.DISCORD_MSG_COMMAND_HELP_SUBSCRIBE, "**.subscribe** - подписаться на репорты.");
        values.put(LangType.DISCORD_MSG_COMMAND_HELP_UNSUBSCRIBE, "**.unsubscribe** - отписаться.");

        values.put(LangType.WORD_TIME_NOW, "сейчас");

        values.put(LangType.WORD_TIME_SEC, "секунда");
        values.put(LangType.WORD_TIME_SEC_1, "секунду");
        values.put(LangType.WORD_TIME_SEC_2, "секунды");
        values.put(LangType.WORD_TIME_SEC_3, "секунд");

        values.put(LangType.WORD_TIME_MIN, "минута");
        values.put(LangType.WORD_TIME_MIN_1, "минуту");
        values.put(LangType.WORD_TIME_MIN_2, "минуты");
        values.put(LangType.WORD_TIME_MIN_3, "минут");

        values.put(LangType.WORD_TIME_HOUR, "час");
        values.put(LangType.WORD_TIME_HOUR_1, "час");
        values.put(LangType.WORD_TIME_HOUR_2, "часа");
        values.put(LangType.WORD_TIME_HOUR_3, "часов");

        values.put(LangType.WORD_TIME_DAY, "день");
        values.put(LangType.WORD_TIME_DAY_1, "день");
        values.put(LangType.WORD_TIME_DAY_2, "дня");
        values.put(LangType.WORD_TIME_DAY_3, "дней");
    }

    public static HashMap<LangType, String> getValues() {
        return values;
    }

}
