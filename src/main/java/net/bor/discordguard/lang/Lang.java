package net.bor.discordguard.lang;

// @author ArtBorax
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.bor.discordguard.Settings;
import net.bor.discordguard.config.IConfig;

public class Lang implements ILangs {

    private static HashMap<String, HashMap<LangType, String>> messages = new HashMap<>();

    private final IConfig config;
    private static final Pattern pFileName = Pattern.compile("^([a-z]{2}_[A-Z]{2}).json$");
    private static final Pattern pParams = Pattern.compile("\\{(\\d+)\\}");
    private static String defaultLang = "ru_RU";

    public Lang(IConfig config) {
        this.config = config;
        defaultLang = config.getLangDefault();
    }

    private HashMap<LangType, String> load(String lang) {
        try {
            HashMap<LangType, String> msgs = config.load(Settings.LANG_FOLDER, lang, HashMap.class);
            //на время разработки отключена загрузка
            //if (msgs == null) {
            if (true) {
                msgs = Messages.getValues();
                config.save(msgs, Settings.LANG_FOLDER, lang);
                return msgs;
            } else {
                HashMap<LangType, String> result = new HashMap<>();
                for (Object obj : msgs.keySet()) {
                    if (obj instanceof String) {
                        String key = (String) obj;
                        result.put(LangType.valueOf(key), msgs.get(key));
                    }
                }
                return result;
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public void reload() {
        messages.put(config.getLangDefault(), load(defaultLang + ".json"));
        for (File file : new File(Settings.CONFIG_DIR + File.separator + Settings.LANG_FOLDER).listFiles()) {
            if (file.isFile()) {
                Matcher match = pFileName.matcher(file.getName());
                if (match.find()) {
                    messages.put(match.group(1), load(file.getName()));
                }
            }
        }
    }

    public static String getDefLangMsg(LangType type) {
        return getLangMsg(defaultLang, type);
    }

    public static String getLangMsg(String lang, LangType type) {
        if (messages.containsKey(lang)) {
            HashMap<LangType, String> msgs = messages.get(lang);
            if (msgs.containsKey(type)) {
                return msgs.get(type);
            }
        }
        return type.name();
    }

    public static String getDefLangMsg(LangType type, String... params) {
        return getLangMsg(defaultLang, type, params);
    }

    public static String getLangMsg(String lang, LangType type, String... params) {
        if (messages.containsKey(lang)) {
            HashMap<LangType, String> msgs = messages.get(lang);
            if (msgs.containsKey(type)) {
                Matcher m = pParams.matcher(msgs.get(type));
                StringBuffer buffer = new StringBuffer();
                while (m.find()) {
                    int index = Integer.parseInt(m.group(1));
                    if (index < params.length) {
                        if (params[index] == null) {
                            m.appendReplacement(buffer, "");
                        } else {
                            m.appendReplacement(buffer, Matcher.quoteReplacement(params[index]));
                        }
                    } else {
                        m.appendReplacement(buffer, "");
                    }
                }
                m.appendTail(buffer);
                return buffer.toString();
            }
        }
        return type.name();
    }

}
